<?php

namespace Tests\Feature;

use App\Jobs\SendCode;
use App\Like;
use App\Mail\verify;
use App\Report;
use App\UserCode;
use App\v2\Contracts\LikeContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use App\v2\Models\User;
use App\v2\Models\Work;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class MobileUserActionsTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    private array $headers = [
        'Accept' => 'application/json',
    ];

    /**
     * Tests authorization by email and password with correct data.
     *
     * @test
     *
     * @return void
     */
    public function auth_user_by_email_with_correct_data()
    {
        $data = [
            "user" => [
                "type"    => "default",
                "default" => [
                    "email"    => "y.pashov@gmail.com",
                    "password" => "password",
                ],
            ],
        ];

        $response = $this->post('/auth/login', $data, $this->headers);

        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'access_token',
                'id',
            ]
        );
    }

    /**
     * Tests authorization by email and password with incorrect password.
     *
     * @test
     */
    public function auth_user_by_email_with_incorrect_data()
    {
        $data = [
            "user" => [
                "type"    => "default",
                "default" => [
                    "email"    => "y.pashov@gmail.com",
                    "password" => "password1",
                ],
            ],
        ];

        $response = $this->post('/auth/login', $data, $this->headers);

        $response->assertStatus(401);
        $response->assertSeeText('Incorrect password');
    }

    // TODO Add tests for social auth

    //TODO Add tests for every field validation for every action

    /**
     * Tests registration with email and password.
     *
     * @test
     */
    public function registration_with_email_and_password()
    {
        $str        = rand();
        $result     = md5($str) . '@test.com';
        $countUsers = User
            ::query()
            ->count();
        $countCodes = UserCode
            ::query()
            ->count();

        $data               = [
            'email' => $result,
        ];
        $response           = $this->post('/auth/register', $data, $this->headers);
        $expectedCountUsers = $countUsers + 1;
        $expectedCountCodes = $countCodes + 1;

        $response->assertStatus(200);
        $this->assertCount($expectedCountUsers, User::all());
        $this->assertCount($expectedCountCodes, UserCode::all());
        $this->assertNull(
            User
                ::query()
                ->latest()
                ->first()->{UserContract::EMAIL_VERIFIED_AT}
        );
    }

    /**
     * Verify registration with email and password.
     *
     * @test
     */
    public function verify_registration_with_email_and_password()
    {
        $code = UserCode
            ::query()
            ->latest()
            ->first();

        $response = $this->get('/auth/verify?code=' . $code->code, $this->headers);

        $response->assertStatus(200);
    }

    /**
     * Testing like with all conditions
     *
     * @test
     */
    public function set_like()
    {
        $response = $this->post('/users/works/1/like', $this->headers);
        $response->assertStatus(401);

        $this->authorize();

        $countLikes = Like
            ::query()
            ->count();

        $work = Work
            ::query()
            ->whereNotIn(
                WorkContract::ID, Like
                ::query()
                ->where(LikeContract::USER_ID, 1002)
                ->select(LikeContract::WORK_ID)
                ->get()
                ->pluck(LikeContract::WORK_ID)
                ->values()
                ->toArray()
            )
            ->inRandomOrder()
            ->first();

        $response           = $this->post('/users/works/' . $work->{WorkContract::ID} . '/like', $this->headers);
        $expectedCountLikes = $countLikes + 1;
        $response->assertStatus(200);

        $oneUserLikes = Like
            ::query()
            ->where(LikeContract::WORK_ID, $work->{WorkContract::ID})
            ->where(LikeContract::USER_ID, 1002)
            ->get();

        $this->assertEquals(1, $oneUserLikes->count());

        $this->assertCount($expectedCountLikes, Like::all());

        $ownWork = Work
            ::query()
            ->create(
                [
                    WorkContract::USER_ID        => 1002,
                    WorkContract::COMPETITION_ID => $work->{WorkContract::COMPETITION_ID},
                    WorkContract::DESCRIPTION    => '',
                    WorkContract::DATA           => '',
                ]
            );

        $response = $this->post('/users/works/' . $ownWork->{WorkContract::ID} . '/like', $this->headers);
        $response->assertStatus(412);

        // TODO Delete created work and sanitize test affection on database

        $anotherWorkInSameCompetition = Work
            ::query()
            ->where(WorkContract::COMPETITION_ID, $work->{WorkContract::COMPETITION_ID})
            ->where(WorkContract::ID, $work->{WorkContract::ID})
            ->inRandomOrder()
            ->first();

        $response = $this->post(
            '/users/works/' . $anotherWorkInSameCompetition->{WorkContract::ID} . '/like', $this->headers
        );
        $response->assertStatus(403);

    }

    public function report()
    {
        $data = [
            'type' => 1,
            'message' => 'Test'
        ];

        $countReports = Report
            ::query()
            ->count();

        $response = $this->post('/works/{work}/report', $data, $this->headers);
        $expectedCountReports = $countReports + 1;

        $this->assertCount($expectedCountReports, Report::all());
    }

    private function authorize()
    {
        $this->actingAs(
            User
                ::query()
                ->where(UserContract::ID, 1002)
                ->first(),
            'api'
        );
    }
}
