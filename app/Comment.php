<?php

namespace App;

use App\Traits\checkUnread;
use App\v2\Contracts\CommentContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use checkUnread, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = CommentContract::FILLABLE;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
    ];

    /**
     * Get the User for the Comment.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }


    /**
     * Get the Work for the Comment.
     */
    public function work()
    {
        return $this->belongsTo(\App\Work::class);
    }

    public function getReadReadAttribute()
    {
        return $this->read;
    }

    public function checkRead()
    {
        return true;
    }

    public static function countRead()
    {
        return 0;
    }
}
