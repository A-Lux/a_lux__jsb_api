<?php

namespace App;

use App\Type;
use App\Work;
use App\Like;
use App\Statistic;
use App\Traits\checkUnread;
use App\Traits\ImageResise;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;
use Storage;

class Competition extends Model
{
    use SoftDeletes, ImageResise, checkUnread;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'company_id',
        'category_id',
        'name',
        'image',
        'type_id',
        'description',
        'sum',
        'share',
        'user_id',
        'ad',
        'url',
        'min_works',
        'start_at',
        'finish_at',
        'email',
        'created_at',
        'confirmed',
        'read',
        'views',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['
        deleted_at',
        'start_at',
        'finish_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_at'   => 'timestamp',
        'finish_at'  => 'timestamp',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * Get the Works for the Competition.
     */
    public function works()
    {
        return $this->hasMany(\App\Work::class);
    }


    /**
     * Get the Populars for the Competition.
     */
    public function populars()
    {
        return $this->hasMany(\App\Populars::class);
    }


    /**
     * Get the Favorites for the Competition.
     */
    public function favorites()
    {
        return $this->hasMany(\App\Favorite::class);
    }


    /**
     * Get the Statistics for the Competition.
     */
    public function statistics()
    {
        return $this->hasMany(\App\Statistic::class);
    }


    /**
     * Get the Company for the Competition.
     */
    public function company()
    {
        return $this->belongsTo(\App\Company::class);
    }


    /**
     * Get the Category for the Competition.
     */
    public function category()
    {
        return $this->belongsTo(\App\Category::class);
    }

    public function countWorks()
    {
        return Work::where('competition_id', $this->id)
                   ->count()
            ;
    }

    public function type()
    {
        return $this->belongsTo(\App\Type::class);
    }

    public function getType()
    {
        return (Type::find($this->type_id))->value;
    }

    public function isCompleted(Request $request)
    {
        if (Auth::guard('api')
                ->user()) {
            return Statistic::where(
                'user_id', Auth::guard('api')
                               ->user()->id
            )
                            ->where('competition_id', $this->id)
                            ->exists()
                ;
        } else {
            return false;
        }
    }

    public function isLiked(Request $request)
    {
        if (Auth::guard('api')
                ->check()) {
            $works = Work::where('competition_id', $this->id)
                         ->select('id')
                         ->get()
                         ->toArray()
            ;

            return Like::where(
                'user_id', Auth::guard('api')
                               ->user()->id
            )
                       ->whereIn('work_id', $works)
                       ->exists()
                ;
        } else {
            return false;
        }
    }

    public function isWinner($id)
    {
        return Statistic::where('user_id', $id)
                        ->where('is_winner', true)
                        ->where('competition_id', $this->id)
                        ->exists()
            ;
    }

    public function getReadReadAttribute()
    {
        return $this->read;
    }

    public function checkRead()
    {
            return true;
    }

    public static function countRead()
    {
        return 0;
    }
}
