<?php

namespace App\Listeners;

use App\Traits\Push;
use App\Transaction;
use App\v2\Contracts\CommentContract;
use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\LikeContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use App\v2\Models\Competition;
use App\v2\Models\User;
use App\v2\Models\Work;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\LikeCreated as Event;
use Illuminate\Support\Facades\Auth;

class CommentCreated
{
    use Push;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     *
     * @return void
     */
    public function handle($event)
    {
        // TODO Add repository
        $work = Work::query()->where(WorkContract::ID, $event->comment->{CommentContract::WORK_ID})->first();

        $user = User::query()->where(UserContract::ID, $work->{WorkContract::USER_ID})->first();
        $user->settings = collect(json_decode($user->settings, true));
        if ($user->settings->isEmpty()) {
            $locale = 'en';
        } else {
            $locale = $user->settings->toArray();
            if (isset($locale['lang'])) {
                $locale = $locale['lang'];
            } else {
                $locale = 'en';
            }
        }

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://api.soviet.iprize.co/users/keys/' . $locale);
        $keys = json_decode($res->getBody()->getContents(), true);

        $title = $keys['push_comment_title'];

        $body = $keys['push_comment_body'];

        $body = str_replace(
            '%USERNAME%', Auth::guard('api')
                              ->user()->name, $body
        );

        $this->sendPush(
            $title,
            $body,
            $user->fcm_token,
            [
                "competition_id" => $work->{WorkContract::COMPETITION_ID},
                "work_id"        => $work->id,
                "type"           => 'work',
            ]
        );

    }
}
