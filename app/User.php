<?php

namespace App;

use App\Traits\ImageResise;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\TokenRepository;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\Guards\TokenGuard;
use Illuminate\Notifications\Notifiable;
use League\OAuth2\Server\ResourceServer;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Redis;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail
{
    use HasApiTokens,
        Notifiable,
        SoftDeletes,
        ImageResise;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deleted_at',
        'name',
        'email',
        'avatar',
        'password',
        'email_verified_at',
        'fcm_token',
        'city_id',
        'city_name',
        'country_id',
        'country_name',
        'about',
        'difficulty',
        'settings',
        'tiktok',
        'youtube',
        'facebook',
        'instagram',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at',
        'email_verified_at',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'timestamp',
        'created_at'        => 'timestamp',
        'updated_at'        => 'timestamp',
    ];

    /**
     * Get the Works for the User.
     */
    public function works()
    {
        return $this->hasMany(\App\Work::class);
    }

    //bppG6*54

    /**
     * Get the Likes for the User.
     */
    public function likes()
    {
        return $this->hasMany(\App\Like::class);
    }


    /**
     * Get the Comments for the User.
     */
    public function comments()
    {
        return $this->hasMany(\App\Comment::class);
    }


    /**
     * Get the Favorites for the User.
     */
    public function favorites()
    {
        return $this->hasMany(\App\Favorite::class);
    }


    /**
     * Get the Blacklists for the User.
     */
    public function blacklists()
    {
        return $this->belongsToMany(
            \App\Blacklist::class,
            'user_blacklists'
        );
    }


    /**
     * Get the Reports for the User.
     */
    public function reports()
    {
        return $this->hasMany(\App\Report::class);
    }


    /**
     * Get the Statistics for the User.
     */
    public function statistics()
    {
        return $this->hasMany(\App\Statistic::class);
    }


    /**
     * Get the Companies for the User.
     */
    public function companies()
    {
        return $this->belongsToMany(\App\Company::class);
    }

    /**
     * Get the SocialNetworks for the User.
     */
    public function socialNetworks()
    {
        return $this->belongsToMany(\App\SocialNetwork::class);
    }

    public static function getByToken(
        Request $request,
                $token
    ) {
        $tokenguard = new TokenGuard(
            App::make(ResourceServer::class),
            Auth::createUserProvider('users'),
            App::make(TokenRepository::class),
            App::make(ClientRepository::class),
            App::make('encrypter')
        );
        $request->headers->set(
            'Authorization',
            'Bearer ' . $token
        );

        return $tokenguard->user($request);
    }

    public function getAvatarAttribute($value)
    {
        if (Str::contains(
            $value,
            'http'
        )) {
            return $value;
        }

        return env('APP_URL') . '/' . $value;
    }

    private function loadRolesRelations()
    {
        if (!$this->relationLoaded('role')) {
            $this->load('role');
        }

        if (!$this->relationLoaded('roles')) {
            $this->load('roles');
        }
    }

    private function loadPermissionsRelations()
    {
        $this->loadRolesRelations();

        if ($this->role && !$this->role->relationLoaded('permissions')) {
            $this->role->load('permissions');
            $this->load('roles.permissions');
        }
    }

    public function hasPermission($name)
    {
        $this->loadPermissionsRelations();

        $_permissions = $this
            ->roles_all()
            ->pluck('permissions')
            ->flatten()
            ->pluck('key')
            ->unique()
            ->toArray();

        return in_array(
            $name,
            $_permissions
        );
    }
}
