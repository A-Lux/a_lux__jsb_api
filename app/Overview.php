<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Overview extends Model
{
    protected $fillable = [
        'users',
        'new_users',
        'total_question_complete',
        'total_question_complete',
        'total_question_winner',
        'total_works',
        'total_likes',
        'total_share',
        'total_comments',
        'today_works',
        'total_competition'
    ];
}
