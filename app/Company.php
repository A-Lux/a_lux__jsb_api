<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * Get the QuestionSets for the Company.
     */
    public function questionSets()
    {
        return $this->hasMany(\App\QuestionSet::class);
    }


    /**
     * Get the Competitions for the Company.
     */
    public function competitions()
    {
        return $this->hasMany(\App\Competition::class);
    }


    /**
     * Get the Users for the Company.
     */
    public function users()
    {
        return $this->belongsToMany(\App\User::class);
    }

}
