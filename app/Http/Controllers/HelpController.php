<?php

namespace App\Http\Controllers;

use App\Help;
use Illuminate\Http\Request;

class HelpController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function index(Request $request)
    {
        return parent::show($request, Help::first()->id);
    }

    public function create(Request $request)
    {
        return parent::create($request);
    }


    public function show(Request $request, $id)
    {
        return parent::show($request, Help::first()->id);
    }

    public function edit(Request $request, $id)
    {
        return parent::edit($request, Help::first()->id);
    }

    public function update(Request $request, $id)
    {
        return parent::update($request, Help::first()->id);
    }

    public function site()
    {
        return response(['text' => '<html><head><meta name="viewport" content="initial-scale=1.0, user-scalable=no" /></head><body>' . Help::first()->text . '</body></html>'], 200);
    }
}
