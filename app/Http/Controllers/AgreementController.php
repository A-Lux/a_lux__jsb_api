<?php

namespace App\Http\Controllers;

use App\Agreement;
use Illuminate\Http\Request;

class AgreementController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function index(Request $request)
    {
        return parent::show($request, Agreement::first()->id);
    }

    public function create(Request $request)
    {
        return parent::create($request);
    }


    public function show(Request $request, $id)
    {
        return parent::show($request, Agreement::first()->id);
    }

    public function site()
    {
        return response(['text' => '<html><head><meta name="viewport" content="initial-scale=1.0, user-scalable=no" /></head><body>' . Agreement::first()->text . '</body></html>'], 200);
    }

    public function edit(Request $request, $id)
    {
        return parent::edit($request, Agreement::first()->id);
    }

    public function update(Request $request, $id)
    {
        return parent::update($request, Agreement::first()->id);
    }
}
