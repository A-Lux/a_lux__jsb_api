<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Read;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $feedback = Feedback::create([
            'text' => $request->text,
            'user_id' => Auth::guard('api')->check() ? Auth::guard('api')->user()->id : null,
            'email' => $request->email
        ]);
        return response([], 200);
    }
}
