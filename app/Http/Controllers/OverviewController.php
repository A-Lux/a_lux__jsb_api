<?php

namespace App\Http\Controllers;

use App\Overview;
use Carbon\Carbon;
use App\User;
use App\Statistic;
use App\Work;
use App\Like;
use App\Comment;
use App\Competition;
use App\QuestionSet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;

class OverviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $value = DB::table('settings')
                   ->where('key', 'admin.premoderation')
                   ->first()->value;

        if ($value == 0) {
            DB::table('settings')
              ->where('key', 'admin.premoderation')
              ->first()
              ->update(['value' => 1])
            ;
        } else {
            DB::table('settings')
              ->where('key', 'admin.premoderation')
              ->first()
              ->update(['value' => 0])
            ;
        }


        return response([], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Overview::create(
            [
                'users'                   => User::count(),
                'new_users'               => User::where('created_at', Carbon::today())
                                                 ->count(),
                'total_question_complete' => Statistic::whereNotNull('question_set_id')
                                                      ->count(),
                'total_question_complete' => Statistic::whereNotNull('question_set_id')
                                                      ->where('created_at', Carbon::today())
                                                      ->count(),
                'total_question_winner'   => Statistic::whereNotNull('question_set_id')
                                                      ->where('created_at', Carbon::today())
                                                      ->where('is_winner', 1)
                                                      ->count(),
                'total_works'             => Work::count(),
                'total_likes'             => Like::count(),
                'total_share'             => Work::sum('share_amount') + QuestionSet::sum('share_amount'),
                'total_comments'          => Comment::count(),
                'today_works'             => Work::where('created_at', Carbon::today())
                                                 ->count(),
                'total_competition'       => Competition::count(),
            ]
        );
    }
}
