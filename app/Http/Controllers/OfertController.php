<?php

namespace App\Http\Controllers;

use App\Ofert;
use Illuminate\Http\Request;

class OfertController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function index(Request $request)
    {
        return parent::show($request, Ofert::first()->id);
    }

    public function create(Request $request)
    {
        return parent::create($request);
    }


    public function show(Request $request, $id)
    {
        return parent::show($request, Ofert::first()->id);
    }

    public function edit(Request $request, $id)
    {
        return parent::edit($request, Ofert::first()->id);
    }

    public function update(Request $request, $id)
    {
        return parent::update($request, Ofert::first()->id);
    }

    public function site()
    {
        return response(['text' => '<html><head><meta name="viewport" content="initial-scale=1.0, user-scalable=no" /></head><body>' . Ofert::first()->text. '</body></html>'], 200);
    }
}
