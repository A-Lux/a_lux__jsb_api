<?php

namespace App\Http\Controllers\Api;

use App\Blacklist;
use App\Category;
use App\Competition;
use App\Read;
use App\User;
use App\UserBlacklist;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserCollection;
use App\Statistic;
use App\Transaction;
use App\Work;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class UserAPIController extends Controller
{
    public function index()
    {
        return new UserCollection(User::paginate());
    }

    public function show(Request $request, $user = null)
    {
        if (is_null($user)) {
            $user = Auth::guard('api')
                        ->user();
        } else {
            $user = User::find($user);
        }
        $response['common']                    = [];
        $response['common']['id']              = $user->id;
        $response['common']['name']            = $user->name;
        $response['common']['avatar']          = $user->avatar;
        $response['common']['email']           = $user->email;
        $response['common']['city']['id']      = $user->city_id;
        $response['common']['city']['name']    = $user->city_name;
        $response['common']['country']['id']   = $user->country_id;
        $response['common']['country']['name'] = $user->country_name;
        $response['common']['about']           = $user->about;
        $response['common']['tiktok']          = $user->tiktok;
        $response['common']['youtube']         = $user->youtube;
        $response['common']['facebook']        = $user->facebook;
        $response['common']['instagram']       = $user->instagram;

        $response['common']['total'] = Transaction
            ::where('user_id', $user->id)
            ->where('approved', 1)
            ->select('created_at as date', 'sum', 'type as operation')
            ->orderBy('created_at', 'desc')
            ->sum('sum');
        $competitions                = Work
            ::where('user_id', $user->id)
            ->select('competition_id')
            ->distinct('competition_id')
            ->get()
            ->pluck('competition_id');
        $response['competition']     = Competition
            ::whereIn('id', $competitions)
            ->where('finish_at', '<', Carbon::now())
            ->select(
                'competitions.id', 'competitions.name',
                'competitions.image as img', 'competitions.sum as price'
            )
            ->get();
        foreach ($response['competition'] as $competition) {
            $competition->count = $competition->countWorks();
            $competition->type  = "Media";
        }
        $response['favourites'] = Category::select('id', 'name', 'image')
                                          ->find(1);

        return response($response, 200);
    }

    public function history(Request $request, $id = null)
    {
        if (is_null($id)) {
            $user = Auth::guard('api')
                        ->user();
        } else {
            $user = User::find($id);
        }

        $competitions = Work::where('user_id', $user->id)
                            ->select('competition_id')
                            ->distinct('competition_id')
                            ->get()
                            ->pluck('competition_id');

        $response['list'] =
            Competition::whereIn('id', $competitions)
                       ->select(
                           'competitions.id',
                           'competitions.name',
                           'competitions.image as img',
                           'competitions.image',
                           'competitions.sum',
                           'competitions.sum as price',
                           'finish_at'
                       )
                       ->orderBy('finish_at', 'desc')
                       ->get();
        foreach ($response['list'] as $competition) {
            $competition->count  = $competition->countWorks();
            $competition->finish = $competition->finish_at < Carbon::now()->timestamp;
            $competition->win    = $competition->finish ? $competition->isWinner($id) : '';
        }

        //        $response['list'] = $response['list']->sortBy('finish');
        $response['list'] = $response['list']->sortByDesc('created_at');

        return response($response, 200);
    }

    public function store(Request $request)
    {
        return new UserResource(User::create($request->all()));
    }

    public function update(Request $request)
    {
        $user = Auth::guard('api')->user();

        $this->validate(
            $request, [
                        'name'     => 'required|string|max:255',
                        //            'email' => 'string|email|max:255',
                        'password' => 'string|min:6',
                        'avatar'   => 'file',
                    ]
        );

        if (!empty($request->avatar)) {
            $avatar         = Storage::put('public/users/' . $user->id . '/avatars', $request->avatar);
            $info['avatar'] = $avatar;
        } else {
            if (!$user->avatar) {
                $avatar         = 'users/default.jpeg';
                $info['avatar'] = $avatar;
            }
        }

        if (!empty($request->name)) {
            $info['name'] = $request->name;
        }

        //        if (!empty($request->email)) {
        //            if($request->email != $user->email){
        //                if(User::
        //                    where('email', $request->email)
        //                        ->where('id', '!=', $user->id)
        //                        ->count() == 0
        //                ){
        //                    $info['email'] = $request->email;
        //                }
        //                else{
        //                    return response('incorrect email', 422);
        //                }
        //            }
        //        }

        if (!empty($request->city_name)) {
            $info['city_name'] = $request->city_name;
        }

        if (!empty($request->country_name)) {
            $info['country_name'] = $request->country_name;
        }


        if (!empty($request->city_id)) {
            $info['city_id'] = $request->city_id;
        }

        if (!empty($request->country_id)) {
            $info['country_id'] = $request->country_id;
        }

        if (!empty($request->password)) {
            if (!empty($request->password_confirm)) {
                if ($request->password == $request->password_confirm) {
                    $info['password'] = Hash::make($request->password);
                }
            }
        }


        if (!empty($request->about)) {
            $info['about'] = $request->about;
        }

        if (!empty($request->tiktok)) {
            $info['tiktok'] = $request->tiktok;
        }
        if (!empty($request->youtube)) {
            $info['youtube'] = $request->youtube;
        }
        if (!empty($request->facebook)) {
            $info['facebook'] = $request->facebook;
        }
        if (!empty($request->instagram)) {
            $info['instagram'] = $request->instagram;
        }

        User::query()->where('id', $user->id)->update($info);

        $user = User::query()->where('id', $user->id)->first();

        return response($user, 200);
    }

    public function destroy(Request $request, User $user)
    {
        $user->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }

    public function addToBlackList(Request $request)
    {
        $blacklist = Blacklist::create(
            [
                'user_id' => $request->user_id,
            ]
        );
        UserBlacklist::create(
            [
                'user_id'      => $request->user()->id,
                'blacklist_id' => $blacklist->id,
            ]
        );

        return response([], 200);
    }

    public function deleteFromBlackList(Request $request, User $user)
    {
        $blacklist     = Blacklist::where('user_id', $user->id)
                                  ->first();
        $userBlacklist = UserBlacklist::where('blacklist_id', $blacklist->id)
                                      ->where(
                                          'user_id', Auth::guard('api')
                                                         ->user()->id
                                      )
                                      ->first();
        $blacklist->delete();
        $userBlacklist->delete();

        return response([], 200);
    }

    public function random()
    {
        $user = User::all()
                    ->random();

        return response($user->createToken('Laravel Password Grant Client')->accessToken, 200);
    }

    public function guest()
    {
        $user = User::find(23);

        return response($user->createToken('Laravel Password Grant Client')->accessToken, 200);
    }

    public function statistics(Request $request)
    {
        return [
            'list' => Statistic::orderBy('created_at', 'desc')
                               ->where('is_winner', true)
                               ->select('sum as score', 'user_id')
                               ->with(
                                   ['user' => function ($query) {
                                       $query->select('id', 'name', 'avatar');
                                   }]
                               )
                               ->get(),
        ];
    }

    public function updateFirebaseToken(Request $request)
    {
        $request->user()
                ->update(
                    [
                        'fcm_token' => $request->token,
                    ]
                )
        ;

        return response([], 200);
    }

    public function blacklist(Request $request)
    {
        return response(
            Auth::guard('api')
                ->user()
                ->blacklists()
                ->with('user')
                ->get(), 200
        );
    }

    public function ban(Request $request)
    {
        if ($request->user) {
            $user = User::find($request->user);
            if ($user->is_blocked) {
                $user->update(
                    [
                        'is_blocked' => 0,
                    ]
                );
            } else {
                $user->update(
                    [
                        'is_blocked' => 1,
                    ]
                );
            }

            return back();
        }
    }

    public function pay(Request $request)
    {
        if (Auth::guard('api')
                ->check()) {

            $total = Transaction
                ::where(
                    'user_id',
                    Auth
                        ::guard('api')
                        ->user()->id
                )
                // ->where('approved', 1)
                ->select('created_at as date', 'sum', 'type as operation')
                ->orderBy('created_at', 'desc')
                ->sum('sum');

            if ($total >= 1000 && $total > $request->sum) {
                $transaction = Transaction::create(
                    [
                        'user_id'      => Auth::user()->id,
                        'sum'          => -$request->sum,
                        'type'         => 'out',
                        'approved'     => 0,
                        'payment_type' => $request->type,
                        'number'       => $request->number,
                        'email'        => $request->email,
                    ]
                );
//                Read::query()
//                    ->create(
//                        [
//                            'model_name' => Transaction::class,
//                            'model_id'   => $transaction->id,
//                            'status'     => 0,
//                        ]
//                    )
//                ;

                return response([], 200);
            } else {
                if ($total < 1000) {
                    return response(['total' => 'Not enough rating'], 422);
                }
                if ($total < $request->sum) {
                    return response(['total' => 'Too big rating request'], 423);
                }
            }
        }
    }

    public function details(Request $request)
    {
        $original = Transaction::where('user_id', Auth::user()->id)
            // ->where('approved', 1)
                               ->select('created_at as date', 'sum', 'type as operation', 'approved')
                               ->where('type', '<>', 'like_transaction')
                               ->where('type', '<>', 'Question')
                               ->orderBy('created_at', 'desc')
                               ->get();
        // question

        $latest = Transaction::where('user_id', Auth::user()->id)
            // ->where('approved', 1)
                             ->select('created_at as date', 'sum', 'type as operation', 'approved')
                             ->where('type', 'like_transaction')
                             ->orderBy('created_at', 'desc')
                             ->first();

        $questions = Transaction::where('user_id', Auth::user()->id)
            // ->where('approved', 1)
                                ->select('created_at as date', 'sum', 'type as operation', 'approved')
                                ->where('type', 'Question')
                                ->orderBy('created_at', 'desc')
                                ->first();

        if ($questions) {
            $questions->sum = Transaction::where('user_id', Auth::user()->id)
                                         ->where('type', 'Question')
                                         ->sum('sum');

            $original->prepend($questions);
        }

        if ($latest) {
            $latest->sum = Transaction::where('user_id', Auth::user()->id)
                                      ->where('type', 'like_transaction')
                                      ->sum('sum');

            $original->prepend($latest);
        }

        $data['list'] = $original;

        $data['total'] = Transaction::where('user_id', Auth::user()->id)
                                    ->where('approved', 1)
                                    ->select('created_at as date', 'sum', 'type as operation', 'approved')
                                    ->orderBy('created_at', 'desc')
                                    ->get()
                                    ->sum('sum');

        return response($data, 200);
    }

    public function currency()
    {
        return response(
            [
                'rate'     => DB::table(
                    'settings'
                )
                                ->where(
                                    'key',
                                    'admin.rate'
                                )
                                ->first()->value,
                'currency' => DB::table(
                    'settings'
                )
                                ->where(
                                    'key',
                                    'admin.currency'
                                )
                                ->first()->value,
            ],
            200
        );
    }

    public function getRating()
    {
        $user = Auth::guard('api')
                    ->user();

        return Transaction::where('user_id', $user->id)
                          ->where('approved', 1)
                          ->select('created_at as date', 'sum', 'type as operation')
                          ->orderBy('created_at', 'desc')
                          ->sum('sum')
        ;
    }

    public function getCountries()
    {
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.vk.com/method/']);

        $response = $client->request(
            'GET', 'database.getCountries', [
                     'query' => [
                         'access_token' => env('VK_ACCESS_TOKEN'),
                         'v'            => '5.5',
                         'need_all'     => 1,
                         'count'        => 1000,
                     ],
                 ]
        );

        return response(
            json_decode(
                $response->getBody()
                         ->getContents()
            )->response->items, 200
        );
    }

    public function getCities(Request $request, $id)
    {
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.vk.com/method/']);

        $response = $client->request(
            'GET', 'database.getCities', [
                     'query' => [
                         'country_id'   => $id,
                         'q'            => $request->q,
                         'access_token' => env('VK_ACCESS_TOKEN'),
                         'v'            => '5.5',
                         'need_all'     => 1,
                         'count'        => 10,
                     ],
                 ]
        );

        return response(
            json_decode(
                $response->getBody()
                         ->getContents()
            )->response->items, 200
        );
    }
}
