<?php

namespace App\Http\Controllers\Api;

use App\Populars;
use App\Http\Resources\PopularsCollection;
use App\Http\Resources\PopularsResource;
use App\Http\Controllers\Controller;

class PopularsAPIController extends Controller
{
    public function index()
    {
        return new PopularsCollection(Populars::paginate());
    }

    public function show(Populars $populars)
    {
        return new PopularsResource($populars->load(['competition']));
    }

    public function store(Request $request)
    {
        return new PopularsResource(Populars::create($request->all()));
    }

    public function update(Request $request, Populars $populars)
    {
        $populars->update($request->all());

        return new PopularsResource($populars);
    }

    public function destroy(Request $request, Populars $populars)
    {
        $populars->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
