<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Ad;
use Carbon\Carbon;
use App\Http\Resources\AdResource;
use App\Http\Resources\AdCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AdAPIController extends Controller
{
    public function index()
    {
        $ads = Ad
            ::query()
            ->select(
                'url',
                'image as img'
            )
            ->whereDate('expire_at', '>=', Carbon::now('Asia/Almaty'))
            ->get();

        foreach ($ads as $ad) {
            $ad->img = Storage
                ::disk('s3')
                ->getAdapter()
                ->getClient()
                ->getObjectUrl(
                    env('AWS_BUCKET'), $ad->img
                );
        }

        return response($ads, 200);
    }

    public function show(Ad $ad)
    {
        return new AdResource($ad->load([]));
    }

    public function store(Request $request)
    {
        return new AdResource(Ad::create($request->all()));
    }

    public function update(Request $request, Ad $ad)
    {
        $ad->update($request->all());

        return new AdResource($ad);
    }

    public function destroy(Request $request, Ad $ad)
    {
        $ad->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
