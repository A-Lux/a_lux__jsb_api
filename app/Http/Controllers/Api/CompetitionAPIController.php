<?php

namespace App\Http\Controllers\Api;

use App\Competition;
use App\Jobs\FinishCompetition;
use App\Read;
use App\Type;
use App\User;
use App\Work;
use Illuminate\Http\Request;
use App\Http\Resources\CompetitionResource;
use App\Http\Resources\CompetitionCollection;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Language;
use App\Traits\Push;
use App\Transaction;
use App\VisitCompetition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CompetitionAPIController extends Controller
{
    use Push;

    public function index()
    {
        return new CompetitionCollection(Competition::paginate());
    }

    public function show(Request $request, $id)
    {
        $competition = Competition
            ::where('id', $id)
            ->select(
                'id',
                'sum as price',
                'ad as img',
                'name',
                'start_at',
                'finish_at',
                'type_id',
                'description',
                'sum',
                'url',
                'min_works',
                'user_id',
                'views'
            )
            ->first()
        ;

        $competition->update(
            [
                'views' => $competition->views + 1,
            ]
        );

        $competition->type      = $competition->getType();
        $competition->count     = $competition->countWorks();
        $competition->finish    = $competition->finish_at < Carbon::now()->timestamp;
        $competition->completed = $competition->isCompleted($request);
        $competition->unlucky   =
            $competition->finish_at < Carbon::now()->timestamp
            &&
            Work::where('competition_id', $competition->id)
                ->count() < $competition->min_works;
        $competition->user      = User::find($competition->user_id);

        return response($competition, 200);
    }

    public function create(Request $request)
    {
        // dd($request->all());

        $request->validate(
            [
                'name'        => ['required', 'max:255'],
                'category_id' => ['required', 'exists:categories,id'],
                'ad'          => ['required', 'file'],
                'image'       => ['required', 'file'],
                'min_works'   => ['required', 'numeric', 'min:30'],
                'type_id'     => ['required', 'exists:types,id'],
                'description' => ['required'],
                'start_at'    => ['required'],
                'finish_at'   => ['required'],
                'email'       => ['required', 'email'],
            ]
        );


        $competition = Competition::create(
            [
                'name'        => $request->name,
                'category_id' => $request->category_id,
                'ad'          => '',
                'url'         => $request->url ? $request->url : '',
                'image'       => '',
                'min_works'   => $request->min_works,
                'type_id'     => $request->type_id,
                'description' => $request->description,
                'user_id'     => $request->user()->id,
                'start_at'    => Carbon::createFromTimestamp($request->start_at / 1000),
                'finish_at'   => Carbon::createFromTimestamp($request->finish_at / 1000),
                'email'       => $request->email,
            ]
        );

        FinishCompetition
            ::dispatch($competition->id)
            ->delay(Carbon::createFromTimestamp($request->finish_at / 1000))
        ;

        $ad    = Storage::put('public/users/' . $request->user()->id . '/competitions', $request->ad);
        $image = Storage::put('public/users/' . $request->user()->id . '/competitions', $request->image);

        $competition->update(
            [
                'ad'    => $ad,
                'image' => $image,
            ]
        );
//
//        Read::query()
//            ->create(
//                [
//                    'model_name' => Competition::class,
//                    'model_id' => $competition->id,
//                    'status' => 0
//                ]
//            );

        return response([], 200);
    }

    public function update(Request $request, Competition $competition)
    {
        $competition->update($request->all());

        return new CompetitionResource($competition);
    }

    public function destroy(Request $request, Competition $competition)
    {
        $competition->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }

    public function share(Request $request, Competition $competition)
    {
        // if(Auth::guard('api')->check()){
        //     Transaction::create([
        //         'approved' => 1,
        //         'sum' => setting('bonuses.share'),
        //         'type' => 'share_bonus',
        //         'user_id' => Auth::guard('api')->user()->id
        //     ]);
        // }
        $competition->update(['share' => $competition->share + 1]);

        return response([], 200);
    }

    public function types()
    {
        return response(
            Type::where('value', '<>', 'audio')
                ->get(), 200
        );
    }

    public function approve($id)
    {

        $competition = Competition::find($id);
        $user        = User::find($competition->user_id);

        if ($user->settings->isEmpty()) {
            $locale = 'en';
        } else {
            $locale = $user->settings->toArray();
            if (isset($locale['lang'])) {
                $locale = $locale['lang'];
            } else {
                $locale = 'en';
            }
        }

        $title = Language
            ::where('key_lang', 'push_competition_approve_title')
            ->where('lang', $locale)
            ->first()
        ;

        $body = Language
            ::where('key_lang', 'push_competition_approve_body')
            ->where('lang', $locale)
            ->first()
        ;

        if ($competition->user_id) {
            \App\Transaction::create(
                [
                    'approved' => 1,
                    'sum'      => setting('bonuses.competition'),
                    'type'     => 'create_competition',
                    'user_id'  => $user->id,
                ]
            );
        }

        $competition->confirmed  = 1;
        $competition->created_at = \Carbon\Carbon::now();
        $competition->save();

        $this->sendPush(
            $title->value,
            $body->value,
            $user->fcm_token,
            [
                "competition" => ['id' => $competition->id],
                "type"        => 'competition',
            ]
        );

        return back();
    }

    public function setFinishDate(Request $request, $competition)
    {
        $competition = Competition
            ::query()
            ->find($competition)
        ;
        $check       = \DB
            ::table(config('queue.connections.database.table'))
            ->where('payload', 'LIKE', '%' . $competition->id . '%')
            ->first()
        ;
        if ($check) {
            \DB::table(config('queue.connections.database.table'))
               ->where('payload', 'LIKE', '%' . $competition->id . '%')
               ->delete()
            ;
        }

        FinishCompetition
            ::dispatch($competition->id)
            ->delay(Carbon::createFromTimestamp($competition->finish_at / 1000))
        ;

        return back();
    }
}
