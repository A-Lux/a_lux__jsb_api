<?php

namespace App\Http\Controllers\Api;

use App\Like;
use App\Http\Resources\LikeResource;
use App\Http\Controllers\Controller;
use App\v2\Contracts\LikeContract;
use App\v2\Contracts\UserContract;
use App\v2\Repositories\UserRepository;
use App\v2\Responses\ApiResponse;
use App\v2\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class LikeAPIController extends Controller
{
    private UserRepository $userRepository;
    private \Aws\S3\S3Client $storage;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->storage        = Storage::disk('s3')
                                       ->getAdapter()
                                       ->getClient();
    }

    public function index(Request $request)
    {
        $likes = Like
            ::query()
            ->where(LikeContract::WORK_ID, $request->{LikeContract::WORK_ID})
            ->paginate();

        $user_ids = $likes
            ->getCollection()
            ->pluck('user_id')
            ->toArray();

        $users = $this->userRepository->get(
            [[UserContract::ID, 'IN', $user_ids]],
            ['ID', 'desc'],
            [UserContract::ID, UserContract::NAME, UserContract::AVATAR, UserContract::COUNTRY_NAME, UserContract::CITY_NAME]
        );

        $likes->getCollection()
              ->transform(function ($like) use ($users) {
                  $like->user = $users
                      ->where(UserContract::ID, $like->{LikeContract::USER_ID})
                      ->first();
                  $like->user->{UserContract::AVATAR} = $this->storage->getObjectUrl(
                      env('AWS_BUCKET'),
                      $like->user->{UserContract::AVATAR}
                  );
                  return $like;
              })
        ;

        $response = ApiResponse::success(
            $likes, Response::HTTP_OK
        );

        return response(
            $response['message'],
            $response['code']
        );
    }

    public function log(Request $request)
    {
        Log::notice('Revenuecat data: ' . print_r($request->all(), true));
    }
}
