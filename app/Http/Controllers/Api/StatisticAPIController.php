<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Competition;
use App\Statistic;
use App\Http\Resources\StatisticCollection;
use App\Http\Resources\StatisticResource;
use App\Like;
use App\QuestionSet;
use App\Transaction;
use App\User;
use App\Work;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StatisticAPIController extends Controller
{
    public function index()
    {
        return new StatisticCollection(Statistic::paginate());
    }

    public function show(Statistic $statistic)
    {
        return new StatisticResource($statistic->load(['user', 'questionSet', 'competition']));
    }

    public function store(Request $request)
    {
        return new StatisticResource(Statistic::create($request->all()));
    }

    public function update(Request $request, Statistic $statistic)
    {
        $statistic->update($request->all());

        return new StatisticResource($statistic);
    }

    public function destroy(Request $request, Statistic $statistic)
    {
        $statistic->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }

    public function rating(Request $request)
    {
        if ($request->range == 'all') {
            $result = Transaction
                ::query()
                ->where('sum', '>', 0)
                ->select('user_id', DB::raw('SUM(sum) as rating'));
        }
        else {
            $result = Transaction::query()->select('user_id', DB::raw('SUM(sum) as rating'));
        }
        // ->where('is_winner', 1);
        if ($request->range) {
            if ($request->range == 'today') {
                $result = $result->where('created_at', '>=', Carbon::
                today());
            }
            if ($request->range == 'week') {
                $result = $result->where('created_at', '>=', Carbon::today()->subWeek());
            }
            if ($request->range == 'month') {
                $result = $result->where('created_at', '>=', Carbon::today()->subMonth());
            }
            if ($request->range == 'all') {
                // $result = $result->where('created_at', '>=', Carbon::today()->subYear());
            }
        }
        // ->whereBetween('is_winner', 1)
        $result = $result->groupBy('user_id')
            ->orderBy('rating', 'desc')
            ->orderBy('id', 'desc')
            ->whereHas('user')
            ->with(['user' => function ($q) {
                $q->select('id', 'name', 'avatar');
            }])
            ->get()
            ->filter(function ($value, $key) {
                return !is_null($value->user->name);
            })
            ->forPage(1, 100)
            ->values();

        return response($result, 200);
    }

    public function today()
    {
        $result = [];
        $result['userList'] = Statistic::select(
                'users.name',
                // \Illuminate\Support\Facades\DB::raw('CONCAT("https://ewr1.vultrobjects.com/general/", users.avatar) as avatar'),
                'users.avatar',
                'statistics.user_id',
                DB::raw('SUM(sum) as rating')
            )
            ->join('users', 'users.id', 'statistics.user_id')
            ->where('is_winner', 1)
            ->whereNotNull('users.name')
            ->groupBy('user_id', 'name', 'avatar')
            ->inRandomOrder()
            ->get();
        $result['activity'] = User::query()->count() * 10; //Like::count() + User::count() + Work::count() + Comment::count() + Work::sum('share_amount') + Competition::sum('share') + QuestionSet::sum('share');

        return response($result, 200);
    }
}
