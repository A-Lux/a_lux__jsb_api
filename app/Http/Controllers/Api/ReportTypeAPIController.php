<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\ReportType;
use App\Http\Controllers\Controller;

class ReportTypeAPIController extends Controller
{
    public function index()
    {
        return ReportType::orderBy('position')->get();
    }
}
