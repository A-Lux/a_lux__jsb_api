<?php

namespace App\Http\Controllers\Api;

use App\Answer;
use App\Difficulty;
use App\Http\Controllers\Controller;
use App\Imports\QuestionSetImport;
use App\Question;
use App\QuestionSet;
use App\Statistic;
use App\Traits\Push;
use App\Transaction;
use App\User;
use Aws\S3\S3Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class QuestionSetAPIController extends Controller
{
    use Push;

    private S3Client $storage;

    public function __construct()
    {
        $this->storage = Storage::disk('s3')
                                ->getAdapter()
                                ->getClient();
    }


    public function index(Request $request)
    {
        $page = 1;
        if (isset($request->page)) {
            $page = $request->page;
        }
        $questions = QuestionSet
            ::query()
            ->select('id', 'name', 'created_at', 'sum as price', 'image as img')
            ->withCount('statistics as peopleCompleted');
        if (Auth::guard('api')
                ->check()) {
            // $questions = $questions->whereDoesntHave('statistics', function ($query) use ($request) {
            //     $query->where('user_id', Auth::guard('api')->user()->id);
            // });
            $questions = $questions->where(
                'difficulty_id',
                Auth::guard('api')
                    ->user()->difficulty ?
                    Auth::guard('api')
                        ->user()->difficulty :
                    Difficulty
                        ::query()
                        ->max('id')
            );
        } else {
            $questions = $questions->where('difficulty_id', Difficulty::max('id'));
        }
        $questions = $questions
            ->inRandomOrder()
            ->get()
            ->forPage($page, setting('admin.question_page'))
            ->values();

        $response = [
            'page' => $page,
            'list' => $questions,
        ];

        return response($response, 200);
    }

    public function show(Request $request, QuestionSet $questionSet)
    {
        $questionSet->peopleCompleted = Statistic::where('question_set_id', $questionSet->id)
                                                 ->count();
        if (Auth::guard('api')
                ->check()) {
            $questionSet->completed = Statistic
                ::query()
                ->where('question_set_id', $questionSet->id)
                ->where(
                    'user_id', Auth::guard('api')
                                   ->user()->id
                )
                ->exists();
        } else {
            $questionSet->completed = false;
        }
        $questionSet->price          = $questionSet->sum;
        $questionSet->img            = $this->storage->getObjectUrl(
            env('AWS_BUCKET'), $questionSet->ad
        );
        $questionSet->amountQuestion = $questionSet->questions()
                                                   ->count();
        $questionSet->makeHidden(['deleted_at', 'updated_at', 'difficulty_id']);

        return response($questionSet, 200);
    }

    public function start(Request $request, $id)
    {
        $response       = [];
        $set            = QuestionSet::query()
                                     ->find($id);
        $questionSetIDs = QuestionSet
            ::query()
            ->select('id')
            ->where('difficulty_id', $set->difficulty_id)
            ->get();

        $response['list'] = Question
            ::query()
            ->whereIn(
                'question_set_id', $questionSetIDs->pluck('id')
                                                  ->toArray()
            )
            ->with(['answers' => function ($query) use ($request) {
                if (!isset($request->site)) {
                    $query
                        ->inRandomOrder()
                        ->select('id', 'question_id', 'text', 'is_right as right')
                    ;
                } else {
                    $query
                        ->select('id', 'question_id', 'text')
                        ->inRandomOrder()
                    ;
                }
            }])
            ->limit(5)
            ->inRandomOrder()
            ->get();
        $response['time'] = QuestionSet::query()
                                       ->find($id)->time_amount;

        return response($response, 200);
    }

    public function finish(Request $request, QuestionSet $questionSet)
    {
        if (Auth::guard('api')
                ->check()) {
            if ($request->win) {
                Statistic
                    ::query()
                    ->create(
                        [
                            'is_winner'       => $request->win,
                            'user_id'         => Auth::guard('api')
                                                     ->user()->id,
                            'question_set_id' => $questionSet->id,
                            'sum'             => $questionSet->sum,
                            'time_amount'     => $request->time,
                            'amount_right'    => $request->amount_right,
                        ]
                    )
                ;
                Transaction
                    ::query()
                    ->create(
                        [
                            'user_id'  => Auth::guard('api')
                                              ->user()->id,
                            'sum'      => $questionSet->sum,
                            'type'     => 'Question',
                            'approved' => 1,
                        ]
                    )
                ;
            } else {
                Statistic
                    ::query()
                    ->create(
                        [
                            'is_winner'       => $request->win,
                            'user_id'         => Auth('api')->user()->id,
                            'question_set_id' => $questionSet->id,
                            'sum'             => 0,
                            'time_amount'     => $request->time_amount,
                            'amount_right'    => $request->amount_right,
                        ]
                    )
                ;
            }
        }

        return response([], 200);
    }

    public function finishSite(Request $request, QuestionSet $questionSet)
    {
        if (Auth::guard('api')
                ->check()) {
            $win = false;

            $rightAnswers = Answer
                ::query()
                ->whereIn(
                    'question_id',
                    Question
                        ::query()
                        ->where('question_set_id', $questionSet->id)
                        ->select('id')
                        ->get()
                )
                ->select('id')
                ->where('is_right', 1)
                ->get()
                ->pluck('id')
                ->toArray();

            $sent = [];

            foreach ($request->answers as $question_id => $answer_id) {
                $sent[] = $answer_id;
            }

            if (empty(array_diff($rightAnswers, $sent))) {
                $win = true;
            }
            if ($win) {
                Statistic
                    ::query()
                    ->create(
                        [
                            'is_winner'       => $win,
                            'user_id'         => Auth::guard('api')
                                                     ->user()->id,
                            'question_set_id' => $questionSet->id,
                            'sum'             => $questionSet->sum,
                            'time_amount'     => $request->time_amount,
                            'amount_right'    => Question::where('question_set_id', $questionSet->id)
                                                         ->count('id'),
                        ]
                    )
                ;
                Transaction
                    ::query()
                    ->create(
                        [
                            'user_id'  => Auth::guard('api')
                                              ->user()->id,
                            'sum'      => $questionSet->sum,
                            'type'     => 'Question',
                            'approved' => 1,
                        ]
                    )
                ;
                $this->sendPush(
                    'You Win!',
                    'You win ' . $questionSet->sum . ' points on answering ' . $questionSet->name,
                    Auth::guard('api')
                        ->user()->fcm_token
                );

                return response(['win' => true], 200);
            } else {
                Statistic
                    ::query()
                    ->create(
                        [
                            'is_winner'       => $win,
                            'user_id'         => Auth::guard('api')
                                                     ->user()->id,
                            'question_set_id' => $questionSet->id,
                            'sum'             => 0,
                            'time_amount'     => $request->time_amount,
                            'amount_right'    => count($rightAnswers) - count(
                                    array_diff($rightAnswers, $sent)
                                ),
                        ]
                    )
                ;
                $this->sendPush(
                    'You Lose!',
                    'You tried to answer ' . $questionSet->name . '. Better luck next time!',
                    Auth::guard('api')
                        ->user()->fcm_token
                );

                return response(['win' => false], 200);
            }
        }

        return response([], 200);
    }

    public function store(Request $request)
    {
        return new QuestionSetResource(QuestionSet::create($request->all()));
    }

    public function update(Request $request, QuestionSet $questionSet)
    {
        $questionSet->update($request->all());

        return new QuestionSetResource($questionSet);
    }

    public function destroy(Request $request, QuestionSet $questionSet)
    {
        $questionSet->delete();

        return response()->json([], ResponseAlias::HTTP_NO_CONTENT);
    }

    public function import(Request $request)
    {
        $validatedData = $request->validate(
            [
                'company'      => ['required', 'numeric'],
                'questionTime' => ['required', 'numeric'],
                'name'         => ['required', 'string'],
                'prize'        => ['required', 'numeric'],
                'level'        => ['required', 'numeric'],
                'url'          => ['required', 'url'],
                'image'        => ['required', 'file'],
                'template'     => ['required', 'file'],
            ]
        );

        Excel::import(new QuestionSetImport($request), $request->template);

        return response(200);
    }

    public function shareYes(Request $request, QuestionSet $questionSet)
    {
        $questionSet->update(['share' => $questionSet->share + 1]);

        if (Auth::guard('api')
                ->check()) {
            if (is_null(
                Auth::guard('api')
                    ->user()->difficulty
            )) {
                User
                    ::query()
                    ->where(
                        'id', Auth::guard('api')
                                  ->user()->id
                    )
                    ->update(
                        [
                            'difficulty' => Difficulty::min('id'),
                        ]
                    )
                ;
            }
            if (Auth::guard('api')
                    ->user()->difficulty < Difficulty::query()
                                                     ->max('id')) {
                Auth::guard('api')
                    ->user()
                    ->update(
                        [
                            'difficulty' => Auth::guard('api')
                                                ->user()->difficulty + 1,
                        ]
                    )
                ;
            }
        }

        return response(
            [
                'user' => Auth::guard('api')
                              ->user(),
            ], 200
        );
    }

    public function shareNo(Request $request, QuestionSet $questionSet)
    {
        $questionSet->update(['share' => $questionSet->share + 1]);
        if (Auth::guard('api')
                ->check()) {
            if (is_null(
                Auth::guard('api')
                    ->user()->difficulty
            )) {
                User
                    ::query()
                    ->where(
                        'id', Auth::guard('api')
                                  ->user()->id
                    )
                    ->update(
                        [
                            'difficulty' => Difficulty::query()
                                                      ->min('id'),
                        ]
                    )
                ;
            }
            if (Auth::guard('api')
                    ->user()->difficulty > Difficulty::query()
                                                     ->min('id')) {
                if ($request->win) {
                    Auth::guard('api')
                        ->user()
                        ->update(
                            [
                                'difficulty' => Auth::guard('api')
                                                    ->user()->difficulty - 1,
                            ]
                        )
                    ;
                }
            }
        }

        return response(
            [
                'user' => Auth::guard('api')
                              ->user(),
            ], 200
        );
    }

    public function massDelete(Request $request)
    {
        if (!empty($request->company_id)) {
            $set_ids      = QuestionSet
                ::query()
                ->where('company_id', $request->company_id)
                ->select('id')
                ->get();
            $questions_id = Question
                ::query()
                ->whereIn('question_set_id', $set_ids)
                ->select('id')
                ->get();
            $answers_id   = Answer
                ::query()
                ->whereIn('question_id', $questions_id)
                ->select('id')
                ->get();
            Answer
                ::query()
                ->whereIn('id', $answers_id)
                ->delete()
            ;
            Question
                ::query()
                ->whereIn('id', $questions_id)
                ->delete()
            ;
            QuestionSet
                ::query()
                ->whereIn('id', $set_ids)
                ->delete()
            ;

            return response([], 200);
        }
        if (!empty($request->difficulty_id)) {
            $set_ids      = QuestionSet
                ::query()
                ->where('company_id', $request->difficulty_id)
                ->select('id')
                ->get();
            $questions_id = Question
                ::query()
                ->whereIn('question_set_id', $set_ids)
                ->select('id')
                ->get();
            $answers_id   = Answer
                ::query()
                ->whereIn('question_id', $questions_id)
                ->select('id')
                ->get();
            Answer
                ::query()
                ->whereIn('id', $answers_id)
                ->delete()
            ;
            Question
                ::query()
                ->whereIn('id', $questions_id)
                ->delete()
            ;
            QuestionSet
                ::query()
                ->whereIn('id', $set_ids)
                ->delete()
            ;

            return response([], 200);
        }
    }
}
