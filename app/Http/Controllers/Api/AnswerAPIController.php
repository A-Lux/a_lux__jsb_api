<?php

namespace App\Http\Controllers\Api;

use App\Answer;
use App\Http\Resources\AnswerCollection;
use App\Http\Resources\AnswerResource;
use App\Http\Controllers\Controller;

class AnswerAPIController extends Controller
{
    public function index()
    {
        return new AnswerCollection(Answer::paginate());
    }

    public function show(Answer $answer)
    {
        return new AnswerResource($answer->load(['question']));
    }

    public function store(Request $request)
    {
        return new AnswerResource(Answer::create($request->all()));
    }

    public function update(Request $request, Answer $answer)
    {
        $answer->update($request->all());

        return new AnswerResource($answer);
    }

    public function destroy(Request $request, Answer $answer)
    {
        $answer->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
