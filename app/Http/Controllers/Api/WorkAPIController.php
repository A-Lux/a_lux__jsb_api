<?php

namespace App\Http\Controllers\Api;

use App;
use App\Comment;
use App\Competition;
use App\Http\Requests\LikeRequest;
use App\Transaction;
use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\LikeContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use App\v2\Repositories\CompetitionRepository;
use App\v2\Repositories\LikeRepository;
use App\v2\Repositories\WorkRepository;
use App\Work;
use App\Like;
use App\Report;
use App\Statistic;
use App\Type;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Language;
use App\Rules\WorkFileValidate;
use App\Traits\Push;
use App\User;
use App\VisitCompetition;
use Illuminate\Support\Facades\DB;

class WorkAPIController extends Controller
{
    use Push;

    protected CompetitionRepository $competitionRepository;
    protected WorkRepository $workRepository;
    protected LikeRepository $likeRepository;

    /**
     * @param \App\v2\Repositories\CompetitionRepository $competitionRepository
     * @param \App\v2\Repositories\WorkRepository        $workRepository
     * @param \App\v2\Repositories\LikeRepository        $likeRepository
     */
    public function __construct(CompetitionRepository $competitionRepository, WorkRepository $workRepository, LikeRepository $likeRepository)
    {
        $this->competitionRepository = $competitionRepository;
        $this->workRepository        = $workRepository;
        $this->likeRepository        = $likeRepository;
    }

    public function index(Request $request, $competition_id)
    {
        $user = null;
        if (Auth::guard('api')
                ->check()) {
            $user = Auth::guard('api')
                        ->user();
            if (VisitCompetition
                ::query()
                ->where(
                    'user_id', Auth::guard('api')
                                   ->user()->id
                )
                ->where('competition_id', $competition_id)
                ->exists()) {
                VisitCompetition
                    ::query()
                    ->where(
                        'user_id', Auth::guard('api')
                                       ->user()->id
                    )
                    ->where('competition_id', $competition_id)
                    ->update(
                        [
                            'visited_at' => Carbon::now(),
                        ]
                    )
                ;
            } else {
                VisitCompetition
                    ::query()
                    ->create(
                        [
                            'user_id'        => Auth::guard('api')
                                                    ->user()->id,
                            'competition_id' => $competition_id,
                            'visited_at'     => Carbon::now(),
                        ]
                    )
                ;
            }
        }
        $response = [];
        $page     = 1;
        if ($request->page) {
            $page = $request->page;
        }
        $response['page'] = $page;
        //        $response['unlucky'] = ;
        $works =
            Work
                ::query()
                ->select(
                    'id',
                    'competition_id',
                    'share_amount',
                    'created_at',
                    'data',
                    'width',
                    'height',
                    'thumbnail as preview',
                    'description',
                    'user_id'
                )
                ->where('competition_id', $competition_id)
                ->withCount('likes as amountLikes')
                ->orderBy('amountLikes', 'desc')
                ->orderBy('created_at', 'desc')
                ->with(['user' => function ($q) {
                    $q->select(
                        'id',
                        'name',
                        'avatar',
                        'country_name',
                        'city_name',
                        'is_blocked'
                    );
                }])
                ->orderBy('created_at', 'desc');

        if ($user) {
            $works = $works->whereNotIn('user_id', $user->blacklists->pluck('user_id'));
        }

        if (DB::table('settings')
              ->where('key', 'admin.work_premoderation')
              ->first()->value == 1) {
            $works = $works->where('verified', 1);
        }

        $works = $works
            ->get()
            ->filter(function ($value, $key) {
                if (!is_null($value->user)) {
                    return !is_null($value->user->name) && $value->user->is_blocked == 0;
                }

                return false;
            })
            ->forPage($page, 10)
            ->values();

        if ($request->work_id) {
            if ($page == 1) {
                $highlight            = Work
                    ::query()
                    ->select
                    (
                        'id',
                        'competition_id',
                        'share_amount',
                        'created_at',
                        'data',
                        'width',
                        'height',
                        'thumbnail as preview',
                        'description',
                        'user_id'
                    )
                    ->withCount('likes as amountLikes')
                    ->with(['user' => function ($q) {
                        $q->select(
                            'id',
                            'name',
                            'avatar',
                            'country_name',
                            'city_name',
                            'is_blocked'
                        );
                    }])
                    ->where('id', $request->work_id)
                    ->first();
                $highlight->highlight = true;
                $works->prepend($highlight);
            }
        }

        foreach ($works as $work) {
            $work->statics       = new \stdClass;
            $work->statics->like = new \stdClass;
            $work->statics->win  =
                Statistic
                    ::query()
                    ->where('competition_id', $work->competition_id)
                    ->where('user_id', $work->user_id)
                    ->where('is_winner', 1)
                    ->exists();
            if (Auth::guard('api')
                    ->check()) {
                $work->statics->like->is_liked = $work->isLiked($request, $work->id);
            }
            $work->statics->like->amount  = $work->amountLikes;
            $work->statics->share         = new \stdClass;
            $work->statics->share->amount = $work->share_amount;
            $work->makeHidden('amount');
            $work->makeHidden('user_id');
            $work->statics->comments         = new \stdClass;
            $work->statics->comments->amount = Comment
                ::query()
                ->where('work_id', $work->id)
                ->where('verified', 1)
                ->count();
            $work->type                      = (Type
                ::query()
                ->find(
                    (Competition
                        ::query()
                        ->find($work->competition_id))->type_id
                ))->value;
        }
        $response['list']     = $works;
        $response['liked']    = Competition
            ::query()
            ->find($competition_id)
            ->isLiked($request);
        $response['finished'] = Competition
                ::query()
                ->find($competition_id)->finish_at < Carbon::now()->timestamp;;

        return response($response, 200);
    }

    public function show(Request $request, $id)
    {
        $work = Work
            ::where('id', $id)
            ->select(
                'id',
                'user_id',
                'competition_id',
                'description',
                'data',
                'share_amount',
                'created_at',
                'thumbnail as preview',
                'width',
                'height'
            )
            ->with(['user' => function ($q) {
                $q->select('id', 'name', 'avatar', 'country_name', 'city_name');
            }])
            ->first();

        $work->statics       = new \stdClass;
        $work->statics->like = new \stdClass;
        if (Auth::guard('api')
                ->check()) {
            $work->statics->like->is_liked = $work->isLiked($request, $id);
        }
        $work->statics->like->amount  = $work->amountLikes;
        $work->statics->share         = new \stdClass;
        $work->statics->share->amount = $work->share_amount;
        $work->makeHidden('amount');
        $work->makeHidden('user_id');
        $work->statics->comments         = new \stdClass;
        $work->statics->comments->amount = Comment
            ::query()
            ->where('work_id', $work->id)
            ->where('verified', 1)
            ->count();
        $work->type                      = (Type
            ::query()
            ->find(
                (Competition
                    ::query()
                    ->find($work->competition_id))->type_id
            ))->value;
        $work->liked                     = Competition
            ::query()
            ->find($work->competition_id)
            ->isLiked($request);
        $work->competitionName           = Competition
            ::query()
            ->find($work->competition_id)->name;

        return response($work, 200);
    }

    public function store(Request $request, $competition_id)
    {
        $this->validate($request, [
            'file'        => new WorkFileValidate,
            'description' => 'max:500',
        ]);
        $file = $request->file;

        if ($request->file) {
            $realname  = Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
            $extension = $file->getClientOriginalExtension();
            $new_name  = Str::slug($realname) . "-" . time() . "." . $extension;
            $filename  = env('APP_URL') . '/' . Storage::put(
                    '/competitions/' . $competition_id . '/' . Auth::guard('api')
                                                                   ->user()->id, $file
                );
        }

        //$file = env('APP_URL') . 'storage/' . str_replace("public/", "", $address);
        $work = [
            'competition_id' => $competition_id,
            'user_id'        => Auth::guard('api')
                                    ->user()->id,
            'description'    => $request->description,
            'data'           => $file ? $filename : '',
            'verified'       => DB::table('settings')
                                  ->where('key', 'admin.work_premoderation')
                                  ->first()->value == 1 ? 0 : 1,
        ];

        if (Competition
                ::query()
                ->find($competition_id)->type_id == 3) {
            $work['thumbnail'] = '';
        }

        $width  = 0;
        $height = 0;
        if (Competition
                ::query()
                ->find($competition_id)->type_id == 1) {
            $address = $file->move(public_path('/storage/competitions/' . $competition_id), $new_name);
            [$width, $height] = getimagesize($address);
            $work['width']  = $width;
            $work['height'] = $height;
            unlink($address);
        }
        $newWork = Work
            ::query()
            ->create($work);

        Statistic
            ::query()
            ->create(
                [
                    'competition_id' => $competition_id,
                    'user_id'        => Auth::guard('api')
                                            ->user()->id,
                    'sum'            => 0,
                    'is_winner'      => false,
                ]
            )
        ;

        if (DB::table('settings')
              ->where('key', 'admin.premoderation')
              ->first()->value == 1) {
            return response([], 200);
        } else {
            return response([], 201);
        }
    }

    public function update(Request $request, Work $work)
    {
        $work->update($request->all());

        return new WorkResource($work);
    }

    public function share(Request $request, Work $work)
    {
        $work->update(['share_amount' => $work->share_amount + 1]);

        return response([], 200);
    }

    public function destroy(Request $request, Work $work)
    {
        if ($work->user_id == $request->user()->id) {
            $work->delete();

            Statistic
                ::query()
                ->where('user_id', $request->user()->id)
                ->where('competition_id', $work->competition_id)
                ->delete()
            ;

            return response()->json([], Response::HTTP_NO_CONTENT);
        } else {
            return response([], 403);
        }
    }

    public function restore(Request $request, $work)
    {
        Work::withTrashed()
            ->find($work)
            ->restore()
        ;

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    public function like(LikeRequest $request, App\v2\Models\Work $work)
    {

        if (Auth::guard('api')
                ->user()->{UserContract::ID} === $work->{WorkContract::USER_ID}) {
            return response('You cannot like your own work.', 412);
        }

        $competition = $this->competitionRepository->show($work->{WorkContract::COMPETITION_ID},);
        if (empty($competition) || $competition->{CompetitionContract::FINISH_AT} < Carbon::now()) {
            return response('Competition ended', 423);
        }

        $workIDs = $this->workRepository->get(
            [
                [
                    WorkContract::COMPETITION_ID,
                    '=',
                    $work->{WorkContract::COMPETITION_ID},
                ],
            ],
            [WorkContract::ID, 'DESC'],
            [WorkContract::ID]
        );
        $likes   = $this->likeRepository->get(
            [
                [LikeContract::WORK_ID, 'IN', $workIDs->flatten()
                                                      ->pluck('id')
                                                      ->toArray()],
                [LikeContract::USER_ID, '=', Auth::guard('api')
                                                 ->user()->{UserContract::ID}],
            ],
            [LikeContract::ID, 'DESC']
        );
        if ($likes->isNotEmpty()) {
            return response('You already liked this competition.', 403);
        }

//        if ($check->allowed()) {
        Like
            ::query()
            ->create(
                [
                    'user_id' => Auth
                        ::guard('api')
                        ->user()->id,
                    'work_id' => $work->id,
                ]
            )
        ;
//        } else {
//            return response($check->message(), $check->code());
//        }

        return response([], 200);
    }

    public function report(Request $request, Work $work)
    {
        $request->validate(
            [
                'type'    => ['required'],
                'message' => ['required'],
            ]
        );

        $report = Report
            ::query()
            ->create(
                [
                    'user_id'        => $request->user()->id,
                    'work_id'        => $work->id,
                    'report_type_id' => $request->type,
                    'text'           => $request->message,
                ]
            );

        return response([], 200);
    }

    public function likeBulk(Request $request, Work $work)
    {
        for ($i = 0; $i < $request->amount; $i++) {
            Like
                ::query()
                ->create(
                    [
                        'user_id' => 11,
                        'work_id' => $work->id,
                    ]
                )
            ;
        }

        return response([], 200);
    }

    public function win(Request $request, Work $work)
    {
        Statistic
            ::query()
            ->where('user_id', $work->user_id)
            ->where('competition_id', $work->competition_id)
            ->update(
                [
                    'is_winner' => 1,
                    'sum'       => Competition
                        ::query()
                        ->find($work->competition_id)->sum,
                ]
            )
        ;
        Transaction
            ::query()
            ->create(
                [
                    'user_id'  => $work->user_id,
                    'sum'      => $work->sum,
                    'type'     => 'Work',
                    'approved' => 1,
                ]
            )
        ;

        return response([], 200);
    }

    public function approve(Work $work)
    {

        $user = User
            ::query()
            ->find($work->user_id);

        if ($user->settings->isEmpty()) {
            $locale = 'en';
        } else {
            $locale = $user->settings->toArray();
            if (isset($locale['lang'])) {
                $locale = $locale['lang'];
            } else {
                $locale = 'en';
            }
        }

        $title = Language
            ::query()
            ->where('key_lang', 'push_competition_approve_title')
            ->where('lang', $locale)
            ->first();

        $body = Language
            ::query()
            ->where('key_lang', 'push_competition_approve_body')
            ->where('lang', $locale)
            ->first();
        if ($work->user_id) {
            Transaction
                ::query()
                ->create(
                    [
                        'approved' => 1,
                        'sum'      => setting('bonuses.competition'),
                        'type'     => 'create_competition',
                        'user_id'  => $work->user_id,
                    ]
                )
            ;
        }
        $work
            ->update(
                [
                    'verified' => 1,
                ]
            )
        ;
        $this->sendPush(
            $title->value,
            $body->value,
            $user->fcm_token,
            [
                "competition_id" => Competition
                    ::query()
                    ->find($work->competition_id)->id,
                "work_id"        => $work->id,
                "type"           => 'work',
            ]
        );

        return back()->withErrors(['success' => true]);
    }
}
