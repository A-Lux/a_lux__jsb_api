<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Competition;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Popular;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VisitCategory;
use App\VisitCompetition;
use App\Work;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Database\Eloquent\Builder;

class CategoryAPIController extends Controller
{
    public function index()
    {
        $cats  = Category
            ::select(
                'id',
                'name',
                DB
                    ::raw(
                        'CONCAT("https://ewr1.vultrobjects.com/general/", image) as image'
                    )
            )
            ->get()
        ;
        $count = null;
        foreach ($cats as $cat) {
            $cat->new_competitions = null;
        }
        if (Auth::guard('api')
                ->check()) {
            foreach ($cats as $cat) {
                $temp = VisitCategory
                    ::where(
                        'user_id',
                        Auth
                            ::guard('api')
                            ->user()->id
                    )
                    ->where('category_id', $cat->id)
                    ->select('visited_at')
                    ->first()
                ;
                if ($temp) {
                    $cat->new_competitions = Competition
                        ::whereDate('created_at', '>=', $temp->visited_at)
                        ->where('confirmed', 1)
                        ->where('category_id', $cat->id)
                        ->count()
                    ;
                    if ($cat->new_competitions == 0) {
                        $cat->new_competitions = null;
                    }
                } else {
                    $cat->new_competitions = Competition
                        ::where('confirmed', 1)
                        ->where('category_id', $cat->id)
                        ->count()
                    ;
                    if ($cat->new_competitions == 0) {
                        $cat->new_competitions = null;
                    }
                }
            }
        } else {
            foreach ($cats as $cat) {
                $cat->new_competitions = Competition
                    ::where('confirmed', 1)
                    ->where('category_id', $cat->id)
                    ->count()
                ;
                // if($cat->new_competitions == 0){
                //     $cat->new_competitions = null;
                // }
            }
        }

        return response($cats, 200);
    }

    public function main()
    {
        $final = new \stdClass;

        $popularsRaw = Popular
            ::with(
                [
                    'competition' => function ($q) {
                        $q->where('start_at', '<', Carbon::now());
                        $q->where('finish_at', '>', Carbon::now());
                        $q->select('id', 'name', 'image', 'sum', 'views');
                    },
                ]
            )
            ->orderBy('position')
            ->get()
        ;


        $populars        = new \stdClass;
        $populars->name  = setting('site.popular');
        $populars->image = 'https://api.br.iprize.co/icFire.png';

        foreach ($popularsRaw as $popular) {
            if ($popular->competition) {
                $popular->competition->count     = $popular->competition->countWorks();
                $popular->competition->new_works = null;
                if (Auth::guard('api')
                        ->check()) {
                    $temp = VisitCompetition
                        ::where(
                            'user_id',
                            Auth
                                ::guard('api')
                                ->user()
                                ->id
                        )
                        ->where('competition_id', $popular->competition->id)
                        ->select('visited_at')
                        ->first()
                    ;
                    if ($temp) {
                        $popular->competition->new_works = Work
                            ::where(
                                'user_id',
                                Auth
                                    ::guard('api')
                                    ->user()
                                    ->id
                            )
                            ->where('created_at', '>=', $temp->visited_at)
                            ->where('competition_id', $popular->competition->id)
                            ->where('verified', 1)
                            ->count()
                        ;
                    } else {
                        $popular->competition->new_works = Work
                            ::where('competition_id', $popular->competition->id)
                            ->where('verified', 1)
                            ->count()
                        ;
                    }
                } else {
                    $popular->competition->new_works = Work
                        ::where('competition_id', $popular->competition->id)
                        ->where('verified', 1)
                        ->count()
                    ;
                }
                $populars->list[] = $popular->competition;
            }
        }

        $h         = "0";
        $final->$h = $populars;
        // $final->newCompetitions = $count;
        $categories = Category
            ::select(
                'id', 'name', \Illuminate\Support\Facades\DB::raw(
                'CONCAT("https://ewr1.vultrobjects.com/general/", image) as image'
            )
            )
            ->get()
        ;
        foreach ($categories as $category) {
            // $category->count = null;
            // if(!is_null($count)){
            //     if(isset($count[$category->id])){
            //         $category->new_competitions = $count[$category->id];
            //     }
            // }
            $category->list = Competition
                ::select(
                    'competitions.id',
                    'competitions.name',
                    'competitions.views',
                    \Illuminate\Support\Facades\DB::raw(
                        'CONCAT("https://ewr1.vultrobjects.com/general/", competitions.image) as image'
                    ),
                    'competitions.sum',
                    'competitions.min_works',
                    'competitions.finish_at',
                    DB::raw('COUNT(works.competition_id) as count')
                )
                ->leftJoin('works', 'competitions.id', '=', 'works.competition_id')
                ->where('start_at', '<', Carbon::now())
                // ->whereNull('works.deleted_at')
                ->where('finish_at', '>', Carbon::now())
                // ->whereNull('works.deleted_at')
                ->where('category_id', $category->id)
                ->groupBy(
                    'competitions.id',
                    'competitions.name',
                    'competitions.image',
                    'competitions.sum',
                    'competitions.min_works',
                    'competitions.views',
                    'competitions.finish_at',
                )
                ->where('confirmed', 1)
                // ->orderBy('count', 'desc')
                ->orderBy('competitions.created_at', 'desc')
                ->withCount(
                    [
                        'works' => function (Builder $query) {
                            $query->where('works.verified', 1);
                        }]
                )
                ->limit(4)
                ->get()
            ;
            foreach ($category->list as $competition) {
                $competition->new_works = null;
                // $competition->count = $competition->countWorks();
                $competition->unlucky =
                    $competition->finish_at < Carbon::now()->timestamp
                    &&
                    $competition->count < $competition->min_works;
                if ($competition->finish_at > Carbon::now()->timestamp) {
                    if (Auth::guard('api')
                            ->check()) {
                        $temp = VisitCompetition
                            ::where(
                                'user_id', Auth::guard('api')
                                               ->user()->id
                            )
                            ->where('competition_id', $competition->id)
                            ->select('visited_at')
                            ->first()
                        ;
                        if ($temp) {
                            $competition->new_works = Work
                                ::where('created_at', '>=', $temp->visited_at)
                                ->where('competition_id', $competition->id)
                                ->where('verified', 1)
                                ->count()
                            ;
                        } else {
                            $competition->new_works = Work
                                ::where('competition_id', $competition->id)
                                ->where('verified', 1)
                                ->count()
                            ;
                        }
                    } else {
                        $competition->new_works = Work
                            ::where('competition_id', $competition->id)
                            ->where('verified', 1)
                            ->count()
                        ;
                    }
                }
            }
            $id         = $category->id;
            $final->$id = $category;
        }

        return response([$final], 200);
    }

    public function show(Request $request, Category $category)
    {
        $validatedData = $request->validate(
            [
                'page' => 'required',
            ]
        );
        if (Auth::guard('api')
                ->check()) {
            if (VisitCategory::where(
                'user_id', Auth::guard('api')
                               ->user()->id
            )
                             ->where('category_id', $category->id)
                             ->exists()) {
                VisitCategory::where(
                    'user_id', Auth::guard('api')
                                   ->user()->id
                )
                             ->where('category_id', $category->id)
                             ->update(
                                 [
                                     'visited_at' => Carbon::now(),
                                 ]
                             )
                ;
                // $category->temp = VisitCategory::where('user_id', Auth::guard('api')->user()->id)
                // ->where('category_id', $category->id)->first();
            } else {
                VisitCategory::create(
                    [
                        'user_id'     => Auth::guard('api')
                                             ->user()->id,
                        'category_id' => $category->id,
                        'visited_at'  => Carbon::now(),
                    ]
                );
            }
        } else {
            // die();
        }
        $category->makeHidden('deleted_at');
        $category->makeHidden('position');
        $category->makeHidden('updated_at');
        $category->page = intval($request->page);
        $category->list = Competition::select(
            'id', 'name', \Illuminate\Support\Facades\DB::raw(
            'CONCAT("https://ewr1.vultrobjects.com/general/", image) as image'
        ), 'sum', 'finish_at'
        )
                                     ->withCount(
                                         [
                                             'works as count' => function (Builder $query) {
                                                 $query->where('works.verified', 1);
                                             }]
                                     )
                                     ->where('category_id', $category->id)
                                     ->where('start_at', '<', Carbon::now())
                                     ->where('confirmed', 1)
            // ->orderBy('count', 'desc')
                                     ->orderBy('competitions.created_at', 'desc')
            // ->where('finish_at', '>', Carbon::now())
                                     ->get()
                                     ->forPage($request->page, 10)
                                     ->values()
        ;

        foreach ($category->list as $competition) {
            $competition->finish_at < Carbon::now()->timestamp ? $competition->finish = true : false;
            // $competition->count = $competition->countWorks();
            $competition->unlucky =
                $competition->finish_at < Carbon::now()->timestamp
                &&
                $competition->count < $competition->min_works;
            if ($competition->finish_at > Carbon::now()->timestamp) {
                if (Auth::guard('api')
                        ->check()) {
                    $temp = VisitCompetition::where(
                        'user_id', Auth::guard('api')
                                       ->user()->id
                    )
                                            ->where('competition_id', $competition->id)
                                            ->select('visited_at')
                                            ->first()
                    ;
                    if ($temp) {
                        $competition->new_works = Work::where('created_at', '>=', $temp->visited_at)
                                                      ->where('competition_id', $competition->id)
                                                      ->where('verified', 1)
                                                      ->count()
                        ;
                    } else {
                        $competition->new_works = Work::where('competition_id', $competition->id)
                                                      ->where('verified', 1)
                                                      ->count()
                        ;
                    }
                } else {
                    $competition->new_works = Work::where('competition_id', $competition->id)
                                                  ->where('verified', 1)
                                                  ->count()
                    ;
                }
            }
        }

        return response($category, 200);
    }

    public function showRandom(Request $request, Category $category)
    {
        // $validatedData = $request->validate([
        //     'page' => 'required',
        // ]);
        $category->makeHidden('deleted_at');
        $category->makeHidden('position');
        $category->makeHidden('updated_at');
        $category->page = intval($request->page);
        $category->list = Competition::select(
            'id', 'name', \Illuminate\Support\Facades\DB::raw(
            'CONCAT("https://ewr1.vultrobjects.com/general/", image) as image'
        ), 'sum', 'finish_at'
        )
                                     ->withCount('works as count')
                                     ->where('category_id', $category->id)
                                     ->where('start_at', '<', Carbon::now())
                                     ->where('confirmed', 1)
                                     ->inRandomOrder()
                                     ->limit(5)
                                     ->where('finish_at', '>', Carbon::now())
                                     ->get()
        ;
        // ->forPage($request->page, 10)
        // ->values();

        foreach ($category->list as $competition) {
            $competition->finish_at < Carbon::now()->timestamp ? $competition->finish = true : false;
            // $competition->count = $competition->countWorks();
            $competition->unlucky =
                $competition->finish_at < Carbon::now()->timestamp
                &&
                $competition->count < $competition->min_works;
            if ($competition->finish_at > Carbon::now()->timestamp) {
                if (Auth::guard('api')
                        ->check()) {
                    $temp = VisitCompetition::where(
                        'user_id', Auth::guard('api')
                                       ->user()->id
                    )
                                            ->where('competition_id', $competition->id)
                                            ->select('visited_at')
                                            ->first()
                    ;
                    if ($temp) {
                        $competition->new_works = Work::where('created_at', '>=', $temp->visited_at)
                                                      ->where('competition_id', $competition->id)
                                                      ->count()
                        ;
                    } else {
                        $competition->new_works = Work::where('competition_id', $competition->id)
                                                      ->count()
                        ;
                    }
                } else {
                    $competition->new_works = Work::where('competition_id', $competition->id)
                                                  ->count()
                    ;
                }
            }
        }

        return response($category, 200);
    }

    public function store(Request $request)
    {
        return new CategoryResource(Category::create($request->all()));
    }

    public function update(Request $request, Category $category)
    {
        $category->update($request->all());

        return new CategoryResource($category);
    }

    public function destroy(Request $request, Category $category)
    {
        $category->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
