<?php

namespace App\Http\Controllers\Api;

use App\Difficulty;
use App\Http\Resources\DifficultyCollection;
use App\Http\Resources\DifficultyResource;
use App\Http\Controllers\Controller;

class DifficultyAPIController extends Controller
{
    public function index()
    {
        return new DifficultyCollection(Difficulty::paginate());
    }

    public function show(Difficulty $difficulty)
    {
        return new DifficultyResource($difficulty->load(['questionSets']));
    }

    public function store(Request $request)
    {
        return new DifficultyResource(Difficulty::create($request->all()));
    }

    public function update(Request $request, Difficulty $difficulty)
    {
        $difficulty->update($request->all());

        return new DifficultyResource($difficulty);
    }

    public function destroy(Request $request, Difficulty $difficulty)
    {
        $difficulty->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
