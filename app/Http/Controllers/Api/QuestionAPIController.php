<?php

namespace App\Http\Controllers\Api;

use App\Question;
use App\Http\Resources\QuestionCollection;
use App\Http\Resources\QuestionResource;
use App\Http\Controllers\Controller;

class QuestionAPIController extends Controller
{
    public function index()
    {
        return new QuestionCollection(Question::paginate());
    }

    public function show(Question $question)
    {
        return new QuestionResource($question->load(['answers', 'questionSet']));
    }

    public function store(Request $request)
    {
        return new QuestionResource(Question::create($request->all()));
    }

    public function update(Request $request, Question $question)
    {
        $question->update($request->all());

        return new QuestionResource($question);
    }

    public function destroy(Request $request, Question $question)
    {
        $question->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
