<?php

namespace App\Http\Controllers\Api;

use App\Operations;
use App\Http\Resources\OperationsCollection;
use App\Http\Resources\OperationsResource;
use App\Http\Controllers\Controller;

class OperationsAPIController extends Controller
{
    public function index()
    {
        return new OperationsCollection(Operations::paginate());
    }

    public function show(Operations $operations)
    {
        return new OperationsResource($operations->load([]));
    }

    public function store(Request $request)
    {
        return new OperationsResource(Operations::create($request->all()));
    }

    public function update(Request $request, Operations $operations)
    {
        $operations->update($request->all());

        return new OperationsResource($operations);
    }

    public function destroy(Request $request, Operations $operations)
    {
        $operations->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
