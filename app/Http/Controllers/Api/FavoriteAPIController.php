<?php

namespace App\Http\Controllers\Api;

use App\Favorite;
use App\Http\Resources\FavoriteCollection;
use App\Http\Resources\FavoriteResource;
use App\Http\Controllers\Controller;

class FavoriteAPIController extends Controller
{
    public function index()
    {
        return new FavoriteCollection(Favorite::paginate());
    }

    public function show(Favorite $favorite)
    {
        return new FavoriteResource($favorite->load(['user', 'competition']));
    }

    public function store(Request $request)
    {
        return new FavoriteResource(Favorite::create($request->all()));
    }

    public function update(Request $request, Favorite $favorite)
    {
        $favorite->update($request->all());

        return new FavoriteResource($favorite);
    }

    public function destroy(Request $request, Favorite $favorite)
    {
        $favorite->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
