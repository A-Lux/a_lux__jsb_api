<?php

namespace App\Http\Controllers\Api;

use App\SocialNetwork;
use App\Http\Resources\SocialNetworkCollection;
use App\Http\Resources\SocialNetworkResource;
use App\Http\Controllers\Controller;

class SocialNetworkAPIController extends Controller
{
    public function index()
    {
        return new SocialNetworkCollection(SocialNetwork::paginate());
    }

    public function show(SocialNetwork $socialNetwork)
    {
        return new SocialNetworkResource($socialNetwork->load(['users']));
    }

    public function store(Request $request)
    {
        return new SocialNetworkResource(SocialNetwork::create($request->all()));
    }

    public function update(Request $request, SocialNetwork $socialNetwork)
    {
        $socialNetwork->update($request->all());

        return new SocialNetworkResource($socialNetwork);
    }

    public function destroy(Request $request, SocialNetwork $socialNetwork)
    {
        $socialNetwork->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
