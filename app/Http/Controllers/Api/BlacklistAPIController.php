<?php

namespace App\Http\Controllers\Api;

use App\Blacklist;
use App\Http\Resources\BlacklistCollection;
use App\Http\Resources\BlacklistResource;
use App\Http\Controllers\Controller;

class BlacklistAPIController extends Controller
{
    public function index()
    {
        return new BlacklistCollection(Blacklist::paginate());
    }

    public function show(Blacklist $blacklist)
    {
        return new BlacklistResource($blacklist->load(['user', 'users']));
    }

    public function store(Request $request)
    {
        return new BlacklistResource(Blacklist::create($request->all()));
    }

    public function update(Request $request, Blacklist $blacklist)
    {
        $blacklist->update($request->all());

        return new BlacklistResource($blacklist);
    }

    public function destroy(Request $request, Blacklist $blacklist)
    {
        $blacklist->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
