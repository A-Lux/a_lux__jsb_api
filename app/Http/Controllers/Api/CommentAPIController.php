<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\CommentResource;
use App\Read;
use App\v2\Contracts\UserContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Language;
use App\Traits\Push;
use App\User;
use App\Work;
use Illuminate\Support\Str;

class CommentAPIController extends Controller
{
    use Push;

    public function index()
    {
        return new CommentCollection(Comment::paginate());
    }

    public function show($id)
    {
        $comments = [
            'list' =>
                Comment
                    ::query()
                    ->select('id', 'text as message', 'user_id', 'created_at')
                    ->where('work_id', $id)
                    ->with(['user' => function ($q) {
                        $q->select('id', 'name', 'avatar', UserContract::COUNTRY_NAME, UserContract::CITY_NAME);
                    }])
                    ->where('verified', 1)
                    ->orderBy('created_at', 'desc')
                    ->get(),
        ];

        return response($comments, 200);
    }

    public function store(Request $request, $work_id)
    {
        $request->validate(
            [
                'message' => 'required|max:255',
            ]
        );
        $comment = Comment
            ::query()
            ->create(
                [
                    'user_id' => Auth::guard('api')
                                     ->user()->id,
                    'text' => $request->message,
                    'work_id' => $work_id,
                    'verified' => 1 //DB::table('settings')->where('key', 'admin.premoderation')->first()->value == 1 ? 0 : 1
                ]
            );

        return response([], 200);
    }

    public function update(Request $request, Comment $comment)
    {
        $comment->update($request->all());

        return new CommentResource($comment);
    }

    public function destroy(Request $request, Comment $comment)
    {
        $comment->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
