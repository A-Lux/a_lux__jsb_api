<?php

namespace App\Http\Controllers;

use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class LanguageController extends Controller
{
    public function get($lang)
    {
        $langs    = Language
            ::query()
            ->where('lang', $lang)
            ->select('key_lang', 'value')
            ->get();
        $response = [];
        foreach ($langs as $key) {
            $response[$key->key_lang] = empty($key->value) ? '' : $key->value;
        }

        return response($response, 200);
    }

    public function getLangs()
    {
//        $keys = Language::select('value', 'lang')
//            ->orWhere('value', '')
//            ->whereNull('value')
//            ->get()
//            ->pluck('lang');
//
//        $langs = Language::select('value', 'lang')
//            ->where('key_lang', 'language')
//            ->whereNotIn('lang', $keys)
//            ->distinct()
//            ->get();

        $langs = json_decode(
            '
            [
            {
              "value": "English",
              "lang": "en"
            },
            {
              "value": "Français",
              "lang": "fr"
            },
            {
              "value": "Español",
              "lang": "es"
            },
            {
              "value": "Português",
              "lang": "pt"
            },
            {
              "value": "Türkçe",
              "lang": "tr"
            },
            {
              "value": "Русский",
              "lang": "ru"
            },
            {
              "value": "Український",
              "lang": "ua"
            },
            {
              "value": "Қазақ",
              "lang": "kz"
            },
            {
              "value": "Tiếng Việt",
              "lang": "vi"
            },
            {
              "value": "ไทย",
              "lang": "th"
            },
            {
              "value": "Filipino",
              "lang": "fil"
            },
            {
              "value": "Indonesia",
              "lang": "id"
            },
            {
              "value": "Melayu",
              "lang": "my"
            },
            {
              "value": "हिंदी",
              "lang": "hi"
            },
            {
              "value": "中文（简体",
              "lang": "zh"
            },
            {
              "value": "語言",
              "lang": "ZH-TW"
            }
           ]'
        );

        return response($langs, 200);
    }

    public function editLang($lang)
    {
        print_r(
            Language::where('lang', $lang)
                    ->get()
        );
    }

    public function add(Request $request)
    {
        $request->validate([
                               'code' => 'required',
                           ]);

        $keys = Arr::flatten(
            Language::select('key_lang')
                    ->distinct()
                    ->get()
                    ->toArray()
        );
        foreach ($keys as $key) {
            Language::create([
                                 'key_lang' => $key,
                                 'value'    => $key == 'language' ? $request->language : '',
                                 'lang'     => $request->code,
                             ]);
        }

        return back();
    }
}
