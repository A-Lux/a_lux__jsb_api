<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Competition;
use App\Like;
use App\Read;
use App\Work;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class CompetitionController extends VoyagerBaseController
{
    public function show(Request $request, $id)
    {
        $data = parent::show($request, $id);
        $view = $data[0];

        $ids = Work::where('competition_id', $data[1]['dataTypeContent']->id)->get();

        $data[1]['dataTypeContent']->totalWorks = $ids->count();
        // $data[1]['dataTypeContent']->totalShares = $data[1]['dataTypeContent']->works->reduce(function ($carry, $item) {
        //     return $carry + $item;
        // });
        $data[1]['dataTypeContent']->totalLikes = Like::whereIn('work_id', $ids->pluck('id'))->count();
        $data[1]['dataTypeContent']->totalComments = Comment::whereIn('work_id', $ids->pluck('id'))->count();

        // Read::query()
        //     ->where('model_name', Competition::class)
        //     ->where('model_id', $id)
        //     ->update(
        //         [
        //             'status' => 1
        //         ]
        //     );

        return Voyager::view($view, $data[1]);
    }

    public function index(Request $request)
    {
        $data = parent::index($request);
        $view = $data[0];
        return Voyager::view($view, $data[1]);
    }

    // public function store(Request $request)
    // {
    //     dd($request);
    // }
}
