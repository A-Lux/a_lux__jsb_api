<?php

namespace App\Http\Controllers\Admin;

use App\Read;
use App\Work;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class WorkController extends VoyagerBaseController
{
    public function index(Request $request)
    {
        $data = parent::index($request);
        $view = $data[0];
        //
        for ($i = 0; $i < count($data[1]['dataTypeContent']); $i++) {
            $data[1]['dataTypeContent'][$i] = $data[1]['dataTypeContent'][$i]->addWinnerAttribute();
        }
        return Voyager::view($view, $data[1]);
    }

    public function show(Request $request, $id)
    {
        $data = parent::show($request, $id);
        $view = $data[0];
        // //
        // for ($i = 0; $i < count($data[1]['dataTypeContent']); $i++) {
        //     $data[1]['dataTypeContent'][$i] = $data[1]['dataTypeContent'][$i]->addWinnerAttribute();
        // }
        // Read::query()
        //     ->where('model_name', Work::class)
        //     ->where('model_id', $id)
        //     ->update(
        //         [
        //             'status' => 1
        //         ]
        //     );
        return Voyager::view($view, $data[1]);
    }
}
