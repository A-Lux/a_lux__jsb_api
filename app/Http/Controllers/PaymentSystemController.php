<?php

namespace App\Http\Controllers;

use App\PaymentSystem;
use Illuminate\Http\Request;

class PaymentSystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(PaymentSystem::get(), 200);
    }
}
