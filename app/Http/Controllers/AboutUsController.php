<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\Faq;
use Illuminate\Http\Request;

class AboutUsController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function index(Request $request)
    {
        return parent::show($request, AboutUs::first()->id);
    }

    public function create(Request $request)
    {
        return parent::create($request);
    }


    public function show(Request $request, $id)
    {
        return parent::show($request, AboutUs::first()->id);
    }

    public function edit(Request $request, $id)
    {
        return parent::edit($request, AboutUs::first()->id);
    }

    public function update(Request $request, $id)
    {
        return parent::update($request, AboutUs::first()->id);
    }

    public function site()
    {
        return response(['text' => '<html><head><meta name="viewport" content="initial-scale=1.0, user-scalable=no" /></head><body>' . AboutUs::first()->text . '</body></html>'], 200);
    }

    public function faqs(Request $request)
    {
        return response(['faqs' => Faq::get()], 200);
    }
}
