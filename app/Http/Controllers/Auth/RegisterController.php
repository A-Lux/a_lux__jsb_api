<?php

namespace App\Http\Controllers\Auth;

use App\Difficulty;
use App\Http\Requests\MailVerificationRequest;
use App\Http\Requests\RegisterRequest;
use App\Jobs\SendCode;
use App\User;
use App\v2\Contracts\DifficultyContract;
use App\v2\Contracts\UserContract;
use Carbon\Carbon;
use App\Mail\verify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Traits\Code;
use App\Transaction;
use App\UserCode;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\Auth;
use \Laravel\Passport\TokenRepository;
use \Laravel\Passport\ClientRepository;
use \Laravel\Passport\Guards\TokenGuard;
use \League\OAuth2\Server\ResourceServer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use Code;

    public function register(RegisterRequest $request)
    {
        $data = $request->validated();
        $user = User
            ::query()
            ->create(
                [
                    'avatar'     => 'guest-user.jpg',
                    'email'      => $data['email'],
                    'role_id'    => UserContract::ROLES['User'],
                    'difficulty' => DifficultyContract::EASY,
                ]
            );
        if (!empty($data['modal'])) {
            Transaction
                ::query()
                ->create(
                    [
                        'approved' => 1,
                        'sum'      => setting('bonuses.register'),
                        'type'     => 'register_modal',
                        'user_id'  => $user->id,
                    ]
                )
            ;
        }
        if (!empty($data['promo_code_input'])) {
            $user_id = preg_replace("/[^0-9\s]/", "", $data['promo_code_input']);
            if (User::query()
                    ->exists($user_id)) {
                Transaction::create(
                    [
                        'approved' => 1,
                        'sum'      => setting('bonuses.share'),
                        'type'     => 'share_bonus',
                        'user_id'  => $user_id,
                    ]
                );
                Transaction::create(
                    [
                        'approved' => 1,
                        'sum'      => setting('bonuses.share'),
                        'type'     => 'share_bonus',
                        'user_id'  => $user->id,
                    ]
                );
            }
        }

        SendCode::dispatch($user);

        return response([], 200);
    }

    public function verify(MailVerificationRequest $request)
    {
        $user = $this->findUserByCode($request->code);

        if ($user) {
            $user->update(['email_verified_at' => Carbon::now()]);
            UserCode
                ::query()
                ->where('user_id', $user->id)
                ->where('code', $request->code)
                ->delete()
            ;

            $token = $user->createToken('Laravel Password Grant Client')->accessToken;

            return response(['access_token' => $token, 'email' => $user->email], 200);
        } else {
            return response('Invalid code', 422);
        }
    }
}
