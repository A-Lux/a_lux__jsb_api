<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\SendCode;
use App\Traits\Code;
use App\User;
use App\UserCode;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    use Code;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails
    */

    public function sendCode(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'exists:users'],
        ]);

        $user = User::where('email', $request->email)->first();

        if ($user) {
            $this->addCodeToQueue($user);
        } else {
            return response('User not found', 422);
        }

        return response([], 200);
    }

    public function checkCode(Request $request)
    {
        // $request->validate([
        //     'code' => ['digits_between:1000,9999']
        // ]);

        $user = $this->findUserByCode($request->code);

        if ($user) {
            UserCode::where('user_id', $user->id)->where('code', $request->code)->delete();
            return response(['access_token' => $user->createToken('Laravel Password Grant Client')->accessToken, 'id' => $user->id]);
        } else {
            return response('User not found', 422);
        }
    }
}
