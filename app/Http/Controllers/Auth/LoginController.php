<?php

namespace App\Http\Controllers\Auth;

use App\Difficulty;
use App\Http\Requests\LoginRequest;
use App\User;
use App\v2\Contracts\DifficultyContract;
use App\v2\Contracts\UserContract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SocialNetwork;
use \Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Hash;
use App\UserSocialNetwork;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $data = $request->validated()['user'];

        if ($data['type'] === 'default') {
            return $this->authByEmail($data['default']['email'], $data['default']['password']);
        } else {
            return $this->authBySocialNetwork(
                $data['social']['type'],
                $data['social']['id'],
                $data['social']['name'],
                $data['social']['email'] ?? ''
            );
        }
    }

    public function authByEmail($email, $password)
    {
        $user = User
            ::query()
            ->where(UserContract::EMAIL, '=', $email)
            ->first();

        if ($user->{UserContract::IS_BLOCKED}) {
            return response([], 403);
        }

        if (Hash::check($password, $user->password)) {
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;

            return response(['access_token' => $token, 'id' => $user->id], Response::HTTP_OK);
        }

        $response = __('auth.password');

        return response($response, 401);
    }

    public function authBySocialNetwork($network, $id, $name, $email)
    {
        $network = SocialNetwork
            ::query()
            ->where('name', $network)
            ->first();

        $check = UserSocialNetwork
            ::query()
            ->where('social_network_id', $network->id)
            ->where('social_id', $id)
            ->first();

        if ($check) {
            $user = User
                ::query()
                ->find($check->user_id);
            if ($user->is_blocked) {
                return response(__('auth.user'), 403);
            }

            $token = $user->createToken('Laravel Password Grant Client')->accessToken;

            return response(['access_token' => $token, 'newUser' => false, 'id' => $user->id], 200);
        } else {
            $userData = [
                'avatar'     => 'guest-user.jpg',
                'name'       => $name,
                'role_id'    => UserContract::ROLES['User'],
                'difficulty' => DifficultyContract::EASY,
            ];
            if ($email != '') {
                $userData['email'] = $email;
            }
            $user = User::query()
                        ->create($userData);
            UserSocialNetwork
                ::query()
                ->create(
                    [
                        'user_id'           => $user->id,
                        'social_network_id' => $network->id,
                        'social_id'         => $id,
                    ]
                )
            ;
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;

            return response(['access_token' => $token, 'newUser' => true, 'id' => $user->id], 200);
        }
    }

    public function logout(Request $request): \Illuminate\Http\Response
    {
        $user = Auth::guard('api')
                    ->user();
        try {
            $this->revokeTokens($user->tokens);

            return response([], Response::HTTP_OK);
        } catch (\Exception $exception) {
            //TODO Make exception
            return response([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function revokeTokens($userTokens)
    {
        foreach ($userTokens as $token) {
            $token->revoke();
        }
    }

    public function check()
    {
        if (Auth::guard('api')
                ->check()) {
            return response([], 200);
        } else {
            return response([], 401);
        }
    }
}
