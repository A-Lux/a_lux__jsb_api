<?php

namespace App\Http\Middleware;

use App;
use App\User;
use Auth;
use Closure;

class langCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('api')->check()) {
            Auth::guard('api')->user()->update(['settings' => collect([
                'lang' => App::getLocale()
            ])]);
        }
        return $next($request);
    }
}
