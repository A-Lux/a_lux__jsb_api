<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user'                  => ['required', 'array'],
            'user.type'             => ['required'],
            'user.default.email'    => [
                Rule::requiredIf(
                    request()->user['type'] === 'default'
                ),
                'email',
                'nullable',
                Rule::exists('users','email')->whereNull('deleted_at')
            ],
            'user.default.password' => [
                'nullable',
                Rule::requiredIf(
                    request()->user['type'] === 'default'
                ),
            ],
            'user.social.type'      => [
                'nullable',
                Rule::requiredIf(
                    request()->user['type'] === 'social'
                ),
                Rule::in(
                    [
                        'google',
                        'vk',
                        'facebook',
                        'instagram',
                        'apple',
                    ]
                ),
            ],
            'user.social.id'        => [
                'nullable',
                Rule::requiredIf(
                    request()->user['type'] === 'social'
                ),
            ],
            'user.social.name'      => [
                'nullable',
                Rule::requiredIf(
                    request()->user['type'] === 'social'
                ),
            ],
            'user.social.email'     => [
                'nullable',
                'email',
            ],
        ];
    }
}
