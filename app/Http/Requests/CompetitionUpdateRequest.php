<?php

namespace App\Http\Requests;

use App\v2\Contracts\CompetitionContract;
use App\v2\Models\Competition;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class CompetitionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Competition $competition): bool
    {
        return Gate::inspect('update', [Competition::class, $competition])->allowed();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            CompetitionContract::DESCRIPTION  => 'string',
            CompetitionContract::IS_DENIED    => 'boolean',
            CompetitionContract::IS_CONFIRMED => 'boolean',
            CompetitionContract::IS_READ      => 'boolean',
        ];
    }
}
