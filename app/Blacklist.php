<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Blacklist
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User|null $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blacklist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blacklist newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Blacklist onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blacklist query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blacklist whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blacklist whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blacklist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blacklist whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blacklist whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Blacklist withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Blacklist withoutTrashed()
 * @mixin \Eloquent
 */
class Blacklist extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'user_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * Get the User for the Blacklist.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }


    /**
     * Get the Users for the Blacklist.
     */
    public function users()
    {
        return $this->belongsToMany(\App\User::class, 'user_blacklists');
    }
}
