<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSocialNetwork extends Model
{
    protected $fillable = ['user_id', 'social_network_id', 'social_id'];
}
