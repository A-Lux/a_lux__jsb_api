<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Ofert
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ofert newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ofert newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ofert query()
 * @mixin \Eloquent
 */
class Ofert extends Model
{
    //
}
