<?php

namespace App\Console\Commands;

use App\Like;
use App\Statistic;
use App\Work;
use Illuminate\Console\Command;

class randomLikes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:likes {competition}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $temp = Work::where('competition_id', '>=', $this->argument('competition'))->get();
        foreach ($temp as $work) {
            Statistic::create([
                'competition_id' => $work->competition_id,
                'user_id' => $work->user_id,
                'sum' => 0,
                'is_winner' => false
            ]);
        }
        // $work->update([
        //     'share_amount' => rand(10, 100)
        // ]);
        // $d = rand(10, 100);
        // }
        // foreach ($temp as $item) {
        //     $d = rand(10, 100);
        //     for ($i = 0; $i < $d; $i++) {
        //         Like::create([
        //             'user_id' => 11,
        //             'work_id' => $item->id
        //         ]);
        //     }
        // };
    }
}
