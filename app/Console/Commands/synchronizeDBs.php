<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class synchronizeDBs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'synchro:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $about_us = DB::connection('old')->table('about_us')->get()->toArray();
        foreach ($about_us as $key => $el) {
             $about_us[$key] = (array) $el;
        }
        foreach(array_chunk($about_us, 1000) as $t) {
         DB::connection('mysql')->table('about_us')->insert($t);
        }
        $this->progress_bar(1, 59);
        $ads = DB::connection('old')->table('ads')->get()->toArray();
        foreach ($ads as $key => $el) {
             $ads[$key] = (array) $el;
        }
        foreach(array_chunk($ads, 1000) as $t) {
         DB::connection('mysql')->table('ads')->insert($t);
        }
        $this->progress_bar(2, 59);
        $agreements = DB::connection('old')->table('agreements')->get()->toArray();
        foreach ($agreements as $key => $el) {
             $agreements[$key] = (array) $el;
        }
        foreach(array_chunk($agreements, 1000) as $t) {
         DB::connection('mysql')->table('agreements')->insert($t);
        }
        $this->progress_bar(3, 59);
        $answers = DB::connection('old')->table('answers')->get()->toArray();
        foreach ($answers as $key => $el) {
             $answers[$key] = (array) $el;
        }
        foreach(array_chunk($answers, 1000) as $t) {
         DB::connection('mysql')->table('answers')->insert($t);
        }
        $this->progress_bar(4, 59);
        $banned_users = DB::connection('old')->table('banned_users')->get()->toArray();
        foreach ($banned_users as $key => $el) {
             $banned_users[$key] = (array) $el;
        }
        foreach(array_chunk($banned_users, 1000) as $t) {
         DB::connection('mysql')->table('banned_users')->insert($t);
        }
        $this->progress_bar(5, 59);
        $blacklists = DB::connection('old')->table('blacklists')->get()->toArray();
        foreach ($blacklists as $key => $el) {
             $blacklists[$key] = (array) $el;
        }
        foreach(array_chunk($blacklists, 1000) as $t) {
         DB::connection('mysql')->table('blacklists')->insert($t);
        }
        $this->progress_bar(6, 59);
        $categories = DB::connection('old')->table('categories')->get()->toArray();
        foreach ($categories as $key => $el) {
             $categories[$key] = (array) $el;
        }
        foreach(array_chunk($categories, 1000) as $t) {
         DB::connection('mysql')->table('categories')->insert($t);
        }
        $this->progress_bar(7, 59);
        $comments = DB::connection('old')->table('comments')->get()->toArray();
        foreach ($comments as $key => $el) {
             $comments[$key] = (array) $el;
            unset($comments[$key]['read']);
        }
        foreach(array_chunk($comments, 1000) as $t) {
         DB::connection('mysql')->table('comments')->insert($t);
        }
        $this->progress_bar(8, 59);
        $companies = DB::connection('old')->table('companies')->get()->toArray();
        foreach ($companies as $key => $el) {
             $companies[$key] = (array) $el;
        }
        foreach(array_chunk($companies, 1000) as $t) {
         DB::connection('mysql')->table('companies')->insert($t);
        }
        $this->progress_bar(9, 59);
        $company_user = DB::connection('old')->table('company_user')->get()->toArray();
        foreach ($company_user as $key => $el) {
             $company_user[$key] = (array) $el;
        }
        foreach(array_chunk($company_user, 1000) as $t) {
         DB::connection('mysql')->table('company_user')->insert($t);
        }
        $this->progress_bar(10, 59);
        $competitions = DB::connection('old')->table('competitions')->get()->toArray();
        foreach ($competitions as $key => $el) {
             $competitions[$key] = (array) $el;
             unset($competitions[$key]['read']);
        }
        foreach(array_chunk($competitions, 1000) as $t) {
         DB::connection('mysql')->table('competitions')->insert($t);
        }
        $this->progress_bar(11, 59);
        $data_rows = DB::connection('old')->table('data_rows')->get()->toArray();
        foreach ($data_rows as $key => $el) {
             $data_rows[$key] = (array) $el;
        }
        foreach(array_chunk($data_rows, 1000) as $t) {
         DB::connection('mysql')->table('data_rows')->insert($t);
        }
        $this->progress_bar(12, 59);
        $data_types = DB::connection('old')->table('data_types')->get()->toArray();
        foreach ($data_types as $key => $el) {
             $data_types[$key] = (array) $el;
        }
        foreach(array_chunk($data_types, 1000) as $t) {
         DB::connection('mysql')->table('data_types')->insert($t);
        }
        $this->progress_bar(13, 59);
        $difficulties = DB::connection('old')->table('difficulties')->get()->toArray();
        foreach ($difficulties as $key => $el) {
             $difficulties[$key] = (array) $el;
        }
        foreach(array_chunk($difficulties, 1000) as $t) {
         DB::connection('mysql')->table('difficulties')->insert($t);
        }
        $this->progress_bar(14, 59);
        $failed_jobs = DB::connection('old')->table('failed_jobs')->get()->toArray();
        foreach ($failed_jobs as $key => $el) {
             $failed_jobs[$key] = (array) $el;
        }
        foreach(array_chunk($failed_jobs, 1000) as $t) {
         DB::connection('mysql')->table('failed_jobs')->insert($t);
        }
        $this->progress_bar(15, 59);
        $favorites = DB::connection('old')->table('favorites')->get()->toArray();
        foreach ($favorites as $key => $el) {
             $favorites[$key] = (array) $el;
        }
        foreach(array_chunk($favorites, 1000) as $t) {
         DB::connection('mysql')->table('favorites')->insert($t);
        }
        $this->progress_bar(16, 59);
        $helps = DB::connection('old')->table('helps')->get()->toArray();
        foreach ($helps as $key => $el) {
             $helps[$key] = (array) $el;
        }
        foreach(array_chunk($helps, 1000) as $t) {
         DB::connection('mysql')->table('helps')->insert($t);
        }
        $this->progress_bar(17, 59);
        $jobs = DB::connection('old')->table('jobs')->get()->toArray();
        foreach ($jobs as $key => $el) {
             $jobs[$key] = (array) $el;
        }
        foreach(array_chunk($jobs, 1000) as $t) {
         DB::connection('mysql')->table('jobs')->insert($t);
        }
        $this->progress_bar(18, 59);
        $languages = DB::connection('old')->table('languages')->get()->toArray();
        foreach ($languages as $key => $el) {
             $languages[$key] = (array) $el;
        }
        foreach(array_chunk($languages, 1000) as $t) {
         DB::connection('mysql')->table('languages')->insert($t);
        }
        $this->progress_bar(19, 59);
        $likes = DB::connection('old')->table('likes')->get()->toArray();
        foreach ($likes as $key => $el) {
             $likes[$key] = (array) $el;
        }
        foreach(array_chunk($likes, 1000) as $t) {
         DB::connection('mysql')->table('likes')->insert($t);
        }
        $this->progress_bar(20, 59);
        $menu_items = DB::connection('old')->table('menu_items')->get()->toArray();
        foreach ($menu_items as $key => $el) {
             $menu_items[$key] = (array) $el;
        }
        foreach(array_chunk($menu_items, 1000) as $t) {
         DB::connection('mysql')->table('menu_items')->insert($t);
        }
        $this->progress_bar(21, 59);
        $menus = DB::connection('old')->table('menus')->get()->toArray();
        foreach ($menus as $key => $el) {
             $menus[$key] = (array) $el;
        }
        foreach(array_chunk($menus, 1000) as $t) {
         DB::connection('mysql')->table('menus')->insert($t);
        }
        $this->progress_bar(22, 59);
        $this->progress_bar(23, 59);
        $oauth_access_tokens = DB::connection('old')->table('oauth_access_tokens')->get()->toArray();
        foreach ($oauth_access_tokens as $key => $el) {
             $oauth_access_tokens[$key] = (array) $el;
        }
        foreach(array_chunk($oauth_access_tokens, 1000) as $t) {
         DB::connection('mysql')->table('oauth_access_tokens')->insert($t);
        }
        $this->progress_bar(24, 59);
        $oauth_auth_codes = DB::connection('old')->table('oauth_auth_codes')->get()->toArray();
        foreach ($oauth_auth_codes as $key => $el) {
             $oauth_auth_codes[$key] = (array) $el;
        }
        foreach(array_chunk($oauth_auth_codes, 1000) as $t) {
         DB::connection('mysql')->table('oauth_auth_codes')->insert($t);
        }
        $this->progress_bar(25, 59);
        $oauth_clients = DB::connection('old')->table('oauth_clients')->get()->toArray();
        foreach ($oauth_clients as $key => $el) {
             $oauth_clients[$key] = (array) $el;
        }
        foreach(array_chunk($oauth_clients, 1000) as $t) {
         DB::connection('mysql')->table('oauth_clients')->insert($t);
        }
        $this->progress_bar(26, 59);
        $oauth_personal_access_clients = DB::connection('old')->table('oauth_personal_access_clients')->get()->toArray();
        foreach ($oauth_personal_access_clients as $key => $el) {
             $oauth_personal_access_clients[$key] = (array) $el;
        }
        foreach(array_chunk($oauth_personal_access_clients, 1000) as $t) {
         DB::connection('mysql')->table('oauth_personal_access_clients')->insert($t);
        }
        $this->progress_bar(27, 59);
        $oauth_refresh_tokens = DB::connection('old')->table('oauth_refresh_tokens')->get()->toArray();
        foreach ($oauth_refresh_tokens as $key => $el) {
             $oauth_refresh_tokens[$key] = (array) $el;
        }
        foreach(array_chunk($oauth_refresh_tokens, 1000) as $t) {
         DB::connection('mysql')->table('oauth_refresh_tokens')->insert($t);
        }
        $this->progress_bar(28, 59);
        $oferts = DB::connection('old')->table('oferts')->get()->toArray();
        foreach ($oferts as $key => $el) {
             $oferts[$key] = (array) $el;
        }
        foreach(array_chunk($oferts, 1000) as $t) {
         DB::connection('mysql')->table('oferts')->insert($t);
        }
        $this->progress_bar(29, 59);
        $operations = DB::connection('old')->table('operations')->get()->toArray();
        foreach ($operations as $key => $el) {
             $operations[$key] = (array) $el;
        }
        foreach(array_chunk($operations, 1000) as $t) {
         DB::connection('mysql')->table('operations')->insert($t);
        }
        $this->progress_bar(30, 59);
        $overviews = DB::connection('old')->table('overviews')->get()->toArray();
        foreach ($overviews as $key => $el) {
             $overviews[$key] = (array) $el;
        }
        foreach(array_chunk($overviews, 1000) as $t) {
         DB::connection('mysql')->table('overviews')->insert($t);
        }
        $this->progress_bar(31, 59);
        $password_resets = DB::connection('old')->table('password_resets')->get()->toArray();
        foreach ($password_resets as $key => $el) {
             $password_resets[$key] = (array) $el;
        }
        foreach(array_chunk($password_resets, 1000) as $t) {
         DB::connection('mysql')->table('password_resets')($password_resets);
        }
        $this->progress_bar(32, 59);
        $payment_systems = DB::connection('old')->table('payment_systems')->get()->toArray();
        foreach ($payment_systems as $key => $el) {
             $payment_systems[$key] = (array) $el;
        }
        foreach(array_chunk($payment_systems, 1000) as $t) {
         DB::connection('mysql')->table('payment_systems')->insert($t);
        }
        $this->progress_bar(33, 59);
        $permission_role = DB::connection('old')->table('permission_role')->get()->toArray();
        foreach ($permission_role as $key => $el) {
             $permission_role[$key] = (array) $el;
        }
        foreach(array_chunk($permission_role, 1000) as $t) {
         DB::connection('mysql')->table('permission_role')->insert($t);
        }
        $this->progress_bar(34, 59);
        $permissions = DB::connection('old')->table('permissions')->get()->toArray();
        foreach ($permissions as $key => $el) {
             $permissions[$key] = (array) $el;
        }
        foreach(array_chunk($permissions, 1000) as $t) {
         DB::connection('mysql')->table('permissions')->insert($t);
        }
        $this->progress_bar(35, 59);
        $populars = DB::connection('old')->table('populars')->get()->toArray();
        foreach ($populars as $key => $el) {
             $populars[$key] = (array) $el;
        }
        foreach(array_chunk($populars, 1000) as $t) {
         DB::connection('mysql')->table('populars')->insert($t);
        }
        $this->progress_bar(36, 59);
        $question_sets = DB::connection('old')->table('question_sets')->get()->toArray();
        foreach ($question_sets as $key => $el) {
             $question_sets[$key] = (array) $el;
        }
        foreach(array_chunk($question_sets, 1000) as $t) {
         DB::connection('mysql')->table('question_sets')->insert($t);
        }
        $this->progress_bar(37, 59);
        $questions = DB::connection('old')->table('questions')->get()->toArray();
        foreach ($questions as $key => $el) {
             $questions[$key] = (array) $el;
        }
        foreach(array_chunk($questions, 1000) as $t) {
         DB::connection('mysql')->table('questions')->insert($t);
        }
        $this->progress_bar(38, 59);
        $report_types = DB::connection('old')->table('report_types')->get()->toArray();
        foreach ($report_types as $key => $el) {
             $report_types[$key] = (array) $el;
        }
        foreach(array_chunk($report_types, 1000) as $t) {
         DB::connection('mysql')->table('report_types')->insert($t);
        }
        $this->progress_bar(39, 59);
        $reports = DB::connection('old')->table('reports')->get()->toArray();
        foreach ($reports as $key => $el) {
             $reports[$key] = (array) $el;
            unset($reports[$key]['read']);
        }
        foreach(array_chunk($reports, 1000) as $t) {
         DB::connection('mysql')->table('reports')->insert($t);
        }
        $this->progress_bar(40, 59);
        $requests = DB::connection('old')->table('requests')->get()->toArray();
        foreach ($requests as $key => $el) {
             $requests[$key] = (array) $el;
            unset($requests[$key]['read']);
        }
        foreach(array_chunk($requests, 1000) as $t) {
         DB::connection('mysql')->table('requests')->insert($t);
        }
        $this->progress_bar(41, 59);
        $roles = DB::connection('old')->table('roles')->get()->toArray();
        foreach ($roles as $key => $el) {
             $roles[$key] = (array) $el;
        }
        foreach(array_chunk($roles, 1000) as $t) {
         DB::connection('mysql')->table('roles')->insert($t);
        }
        $this->progress_bar(42, 59);
        $settings = DB::connection('old')->table('settings')->get()->toArray();
        foreach ($settings as $key => $el) {
             $settings[$key] = (array) $el;
        }
        foreach(array_chunk($settings, 1000) as $t) {
         DB::connection('mysql')->table('settings')->insert($t);
        }
        $this->progress_bar(43, 59);
        $social_networks = DB::connection('old')->table('social_networks')->get()->toArray();
        foreach ($social_networks as $key => $el) {
             $social_networks[$key] = (array) $el;
        }
        foreach(array_chunk($social_networks, 1000) as $t) {
         DB::connection('mysql')->table('social_networks')->insert($t);
        }
        $this->progress_bar(44, 59);
        $statistics = DB::connection('old')->table('statistics')->get()->toArray();
        foreach ($statistics as $key => $el) {
             $statistics[$key] = (array) $el;
        }
        foreach(array_chunk($statistics, 1000) as $t) {
         DB::connection('mysql')->table('statistics')->insert($t);
        }
        $this->progress_bar(45, 59);
        $transactions = DB::connection('old')->table('transactions')->get()->toArray();
        foreach ($transactions as $key => $el) {
             $transactions[$key] = (array) $el;
            unset($transactions[$key]['read']);
        }
        foreach(array_chunk($transactions, 1000) as $t) {
         DB::connection('mysql')->table('transactions')->insert($t);
        }
        $this->progress_bar(46, 59);
        $translations = DB::connection('old')->table('translations')->get()->toArray();
        foreach ($translations as $key => $el) {
             $translations[$key] = (array) $el;
        }
        foreach(array_chunk($translations, 1000) as $t) {
         DB::connection('mysql')->table('translations')->insert($t);
        }
        $this->progress_bar(47, 59);
        $types = DB::connection('old')->table('types')->get()->toArray();
        foreach ($types as $key => $el) {
             $types[$key] = (array) $el;
        }
        foreach(array_chunk($types, 1000) as $t) {
         DB::connection('mysql')->table('types')->insert($t);
        }
        $this->progress_bar(48, 59);
        $user_blacklist = DB::connection('old')->table('user_blacklist')->get()->toArray();
        foreach ($user_blacklist as $key => $el) {
             $user_blacklist[$key] = (array) $el;
        }
        foreach(array_chunk($user_blacklist, 1000) as $t) {
         DB::connection('mysql')->table('user_blacklist')->insert($t);
        }
        $this->progress_bar(49, 59);
        $user_blacklists = DB::connection('old')->table('user_blacklists')->get()->toArray();
        foreach ($user_blacklists as $key => $el) {
             $user_blacklists[$key] = (array) $el;
        }
        foreach(array_chunk($user_blacklists, 1000) as $t) {
         DB::connection('mysql')->table('user_blacklists')->insert($t);
        }
        $this->progress_bar(50, 59);
        $user_codes = DB::connection('old')->table('user_codes')->get()->toArray();
        foreach ($user_codes as $key => $el) {
             $user_codes[$key] = (array) $el;
        }
        foreach(array_chunk($user_codes, 1000) as $t) {
         DB::connection('mysql')->table('user_codes')->insert($t);
        }
        $this->progress_bar(51, 59);
        $user_operation = DB::connection('old')->table('user_operation')->get()->toArray();
        foreach ($user_operation as $key => $el) {
             $user_operation[$key] = (array) $el;
        }
        foreach(array_chunk($user_operation, 1000) as $t) {
         DB::connection('mysql')->table('user_operation')->insert($t);
        }
        $this->progress_bar(52, 59);
        $user_roles = DB::connection('old')->table('user_roles')->get()->toArray();
        foreach ($user_roles as $key => $el) {
             $user_roles[$key] = (array) $el;
        }
        foreach(array_chunk($user_roles, 1000) as $t) {
         DB::connection('mysql')->table('user_roles')->insert($t);
        }
        $this->progress_bar(53, 59);
        $user_social_network = DB::connection('old')->table('user_social_network')->get()->toArray();
        foreach ($user_social_network as $key => $el) {
             $user_social_network[$key] = (array) $el;
        }
        foreach(array_chunk($user_social_network, 1000) as $t) {
         DB::connection('mysql')->table('user_social_network')->insert($t);
        }
        $this->progress_bar(54, 59);
        $user_social_networks = DB::connection('old')->table('user_social_networks')->get()->toArray();
        foreach ($user_social_networks as $key => $el) {
             $user_social_networks[$key] = (array) $el;
        }
        foreach(array_chunk($user_social_networks, 1000) as $t) {
         DB::connection('mysql')->table('user_social_networks')->insert($t);
        }
        $this->progress_bar(55, 59);
        $users = DB::connection('old')->table('users')->get()->toArray();
        foreach ($users as $key => $el) {
             $users[$key] = (array) $el;
        }
        foreach(array_chunk($users, 1000) as $t) {
         DB::connection('mysql')->table('users')->insert($t);
        }
        $this->progress_bar(56, 59);
        $visit_categories = DB::connection('old')->table('visit_categories')->get()->toArray();
        foreach ($visit_categories as $key => $el) {
             $visit_categories[$key] = (array) $el;
        }
        foreach(array_chunk($visit_categories, 1000) as $t) {
         DB::connection('mysql')->table('visit_categories')->insert($t);
        }
        $this->progress_bar(57, 59);
        $visit_competitions = DB::connection('old')->table('visit_competitions')->get()->toArray();
        foreach ($visit_competitions as $key => $el) {
             $visit_competitions[$key] = (array) $el;
        }
        foreach(array_chunk($visit_competitions, 1000) as $t) {
         DB::connection('mysql')->table('visit_competitions')->insert($t);
        }
        DB::statement('UPDATE competitions SET image = Replace(image, \'https://ewr1.vultrobjects.com/general/\', \'\'), ad = Replace(ad, \'https://ewr1.vultrobjects.com/general/\', \'\');');
        $this->progress_bar(58, 59);
        $works = DB::connection('old')->table('works')->get()->toArray();
        foreach ($works as $key => $el) {
             $works[$key] = (array) $el;
            unset($works[$key]['read']);
        }
        foreach(array_chunk($works, 1000) as $t) {
         DB::connection('mysql')->table('works')->insert($t);
        }
        DB::statement('UPDATE works SET data = Replace(data, \'https://ewr1.vultrobjects.com/general/\', \'\')');
        $this->progress_bar(59, 59);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    function progress_bar($done, $total, $info="", $width=50) {
        static $start_time;

        // if we go over our bound, just ignore it
        if($done > $total) return;

        if(empty($start_time)) $start_time=time();
        $now = time();

        $perc=(double)($done/$total);

        $bar=floor($perc*$width);

        $status_bar="\r[";
        $status_bar.=str_repeat("=", $bar);
        if($bar<$width){
            $status_bar.=">";
            $status_bar.=str_repeat(" ", $width-$bar);
        } else {
            $status_bar.="=";
        }

        $disp=number_format($perc*100, 0);

        $status_bar.="] $disp%  $done/$total";

        $rate = ($now-$start_time)/$done;
        $left = $total - $done;
        $eta = round($rate * $left, 2);

        $elapsed = $now - $start_time;

        $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

        echo "$status_bar  ";

        flush();

        // when done, send a newline
        if($done == $total) {
            echo "\n";
        }
    }
}
