<?php

namespace App\Console\Commands;

use App\Language;
use Illuminate\Console\Command;
use Stevebauman\Location\Facades\Location;

class keysAdd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:keys {keys} {lang}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach(json_decode($this->argument('keys')) as $key=>$value){
            Language::insert([
                'key_lang' => $key,
                'value' => $value,
                'lang' => $this->argument('lang')
            ]);
        }
    }
}
