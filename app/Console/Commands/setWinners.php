<?php

namespace App\Console\Commands;

use App\Competition;
use App\Like;
use App\Statistic;
use App\Traits\Push;
use App\Transaction;
use App\Work;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class setWinners extends Command
{
    use Push;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:winner';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set winner for all competitions that apply';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $competition_ids = Competition
//            ::whereDate('finish_at', '<', Carbon::now())
//            ->whereNull('checked')
//            ->select('id')
//            ->get()
//            ->pluck('id')
//        ;
//
//        $filtered = [];
//
//        for ($i = 0, $iMax = count($competition_ids); $i < $iMax; $i++) {
//            if (!Statistic
//                ::where('competition_id', $competition_ids[$i])
//                ->where('is_winner', 1)
//                ->exists()) {
//                $filtered[] = $competition_ids[$i];
//            }
//        }
//
//        $works = Work
//            ::whereIn('competition_id', $filtered)
//            ->get()
//            ->groupBy('competition_id')
//        ;
//
//        $winners = [];
//
//        foreach ($works as $competition => $posts) {
//            if ($posts->count() < Competition::find($competition)->min_works) {
//                Competition
//                    ::find($competition)
//                    ->update(
//                        [
//                            'checked' => 1,
//                        ]
//                    )
//                ;
//                continue;
//            }
//            $likes        = 0;
//            $share_amount = 0;
//            foreach ($posts as $work) {
//                if ($likes < $work->likes()
//                                  ->count()) {
//                    $likes                 = $work->likes()
//                                                  ->count()
//                    ;
//                    $share_amount          = $work->share_amount;
//                    $winners[$competition] = $work->id;
//                } else {
//                    if ($likes == $work->likes()
//                                       ->count()) {
//                        if ($work->share_amount > $share_amount) {
//                            $share_amount          = $work->share_amount;
//                            $winners[$competition] = $work->id;
//                        }
//                    }
//                }
//            }
//        }
//
//        foreach ($winners as $key => $value) {
//            $competition = Competition::find($key);
//            $competition->update(
//                [
//                    'checked' => 1,
//                ]
//            );
//            $work = Work::find($value);
//            Statistic
//                ::where('user_id', $work->user_id)
//                ->where('competition_id', $work->competition_id)
//                ->update(
//                    [
//                        'is_winner' => 1,
//                        'sum'       => $competition->sum,
//                    ]
//                )
//            ;
//            Transaction::create(
//                [
//                    'approved' => 1,
//                    'sum'      => $competition->sum,
//                    'type'     => 'win',
//                    'user_id'  => $work->user_id,
//                ]
//            );
//            $this->sendPush(
//                User::find($work->user_id)->fcm_token,
//                'You Win!',
//                'Your work in competition ' . $competition->name . ' has won!. ' . $competition->sum . ' points of rating were added to your account'
//            );
//        }
    }
}
