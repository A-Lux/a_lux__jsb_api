<?php

namespace App\Console\Commands;

use App\Comment;
use App\Competition;
use App\Like;
use App\Overview;
use App\QuestionSet;
use App\Statistic;
use App\User;
use App\Work;
use Carbon\Carbon;
use Illuminate\Console\Command;

class setStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:overview';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new overview. Must be in cron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Overview::create([
            'users' => User::count(),
            'new_users' => User::whereDate(
                'created_at',
                Carbon::today()
            )
                ->count(),
            'total_question_complete' => Statistic::whereNull('competition_id')
                ->count(),
            'today_question_complete' => Statistic::whereNull('competition_id')
                ->whereDate(
                    'created_at',
                    Carbon::today()
                )
                ->count(),
            'total_question_winner' => Statistic::whereNull('competition_id')
                ->where('is_winner', 1)
                ->count(),
            'total_works' => Work::count(),
            'total_likes' => Like::count(),
            'total_share' =>
            Competition::sum('share') +
                Work::sum('share_amount') +
                QuestionSet::sum('share'),
            'total_comments' => Comment::count(),
            'today_works' => Work::whereDate(
                'created_at',
                Carbon::today()
            )
                ->count(),
            'total_competition' => Competition::count(),
        ]);
    }
}
