<?php

namespace App;

use App\Traits\checkUnread;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Report
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $user_id
 * @property int|null $work_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User|null $user
 * @property-read \App\Work|null $work
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereWorkId($value)
 * @mixin \Eloquent
 */
class Report extends Model
{
    use checkUnread;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'user_id', 'work_id', 'text', 'read'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * Get the User for the Report.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }


    /**
     * Get the Work for the Report.
     */
    public function work()
    {
        return $this->belongsTo(\App\Work::class);
    }

    public function getReadReadAttribute()
    {
//        $this->read();
        return $this->read;
    }

    public function checkRead()
    {
        // $check = Read
        //     ::query()
        //     ->where('model_name', '=', 'App\Report')
        //     ->where('model_id', $this->id)
        //     ->first();
        // if ($check) {
        //     return $check->status;
        // }
        // else {
            return true;
        // }
    }

    public static function countRead()
    {
        return 0;/* Read
            ::query()
            ->where('model_name', 'like', '%Report')
            ->where('status', '=', 0)
            ->count();
            */
    }
}
