<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\UserBlacklist
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $user_id
 * @property int|null $blacklist_id
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBlacklist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBlacklist newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\UserBlacklist onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBlacklist query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBlacklist whereBlacklistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBlacklist whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBlacklist whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBlacklist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBlacklist whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBlacklist whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserBlacklist withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\UserBlacklist withoutTrashed()
 * @mixin \Eloquent
 */
class UserBlacklist extends Model
{
    use SoftDeletes;
    protected $fillable = ['user_id', 'blacklist_id'];
}
