<?php

namespace App\Repositories;

use App\Transaction;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

/**
 * Class TransactionRepository.
 */
class TransactionRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Transaction::class;
    }
}
