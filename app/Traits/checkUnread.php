<?php

namespace App\Traits;

trait checkUnread
{
    public function read()
    {
        $this->update([
            'read' => true
        ]);
    }
}
