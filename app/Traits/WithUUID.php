<?php

namespace App\Traits;


use Illuminate\Support\Str;

trait WithUUID
{
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->{$model->getKeyName()} = Str::uuid()->toString();
        });
    }

    /*
     * This function is used internally by Eloquent models to test if the model has auto increment value
     * @returns bool Always false
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * This function is used internally by Eloquent models to set the data type for the primary key.
     *
     * @return string Always string
     */
    public function getkeyType()
    {
        return 'string';
    }
}
