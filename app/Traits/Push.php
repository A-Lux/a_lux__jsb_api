<?php

namespace App\Traits;

trait Push
{
    public function sendPush($title, $text, $token, $data = null)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $YOUR_API_KEY = 'AAAAA-ZNKno:APA91bHIIV8m3cKXZUjdJMuQ6Ucc3weLaz84Wh9Z4A56AwUBrWLRXChSS3S5cjp-z4C7XZ6drZ_20DA0oKOoOVREiKywsEiZyIYFmA9qRP_F6_OVR4Kbmh_5NfrE64J0xyOhjNswxz0_'; // Server key
        $YOUR_TOKEN_ID = $token; // Client token id
        $request_body = array(
            'to' => $YOUR_TOKEN_ID,
            'notification' => array(
                'title' => $title,
                'body' => $text,
                // 'icon' => 'http://storage.en.iprize.co/icFire.png',
                // 'click_action' => asset('/'),
            ),
            "priority" => "high",
            "foreground" =>  false,
            "userInteraction" =>  false,
            "content_available" =>  true,
        );
        if (!is_null($data)) {
            $request_body["data"] = $data;
        }
        $fields = json_encode($request_body);
        $request_headers = array(
            'Content-Type: application/json',
            'Authorization: key=' . $YOUR_API_KEY,
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);
    }
}
