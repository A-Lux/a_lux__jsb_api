<?php

namespace App\Traits;

use App\Jobs\SendCode;
use App\User;
use App\UserCode;

trait Code
{
    public function addCodeToQueue($user)
    {
        SendCode::dispatch($user);
    }

    public function findUserByCode($code)
    {
        $check = UserCode
            ::query()
            ->where('code', $code)
            ->whereRaw('DATEDIFF(created_at, now()) < 3')
            ->orderBy('created_at', 'desc')
            ->first();

        if ($check) {
            $user = User
                ::query()
                ->find($check->user_id);

            return $user;
        } else {
            return false;
        }
    }
}
