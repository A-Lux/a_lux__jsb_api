<?php

namespace App\Traits;
use Illuminate\Support\Facades\Storage;

trait ImageResise
{
    public static function resize($value)
    {
        // $fileInPath, $fileOutPath,
        $w_o = 1024;
        $h_o = null;
        $percent = false;
        $image = explode('/', $value);
        Storage::disk('local')->put('public/' . end($image), Storage::get($value));
        $fileInPath = Storage::disk('local')->path('public/' . end($image));
        $fileOutPath = $fileInPath;
        // dd($value);
        if (@is_array(getimagesize($fileInPath))) {
            // $directory = "";
            // $directoryDir = explode("/", $fileOutPath);
            // foreach ($directoryDir as $index => $dirNew) {
            //     if ((count($directoryDir) - 1) > $index) {
            //         $directory .= (($index == 0 && $fileOutPath[0] != "/") ? "" : "/") . $dirNew;
            //         if (!is_dir($directory)) {
            //             mkdir($directory);
            //         }
            //     }
            // }
            list($w_i, $h_i, $type) = getimagesize($fileInPath);

            if (!$w_i || !$h_i) {
                echo 'Невозможно получить длину и ширину изображения при уменьшении';
                return;
            }
            $types = array('', 'gif', 'jpeg', 'png', 'webp');
            if($type == 18){
                $ext = $types[4];
            }
            else{
                $ext = $types[$type];
            }

            if ($ext) {
                $func = 'imagecreatefrom' . $ext;
                $img = $func($fileInPath);
            } else {
                echo 'Некорректный формат файла';
                return;
            }
            if ($percent) {
                $w_o *= $w_i / 100;
                $h_o *= $h_i / 100;
            }
            if (!$h_o) $h_o = $w_o / ($w_i / $h_i);
            if (!$w_o) $w_o = $h_o / ($h_i / $w_i);
            $img_o = imagecreatetruecolor($w_o, $h_o);
            imagealphablending($img_o, false);
            imagesavealpha($img_o, true);
            imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
            if ($type == 2 || $type = 4) {
                if($type == 4){
                    imagewebp($img_o, $fileOutPath, 75);
                }
                else{
                    imagejpeg($img_o, $fileOutPath, 75);
                }

                Storage::put('w1024' . $value, Storage::disk('local')->get('public/' . end($image)));
                $link = Storage::url('w1024' . $value);
                Storage::disk('local')->delete('app/public/' . end($image));
                return $link;
            } else {
                $func = 'image' . $ext;
                $func($img_o, $fileOutPath);
                Storage::put('w1024' . $value, Storage::disk('local')->get('public/' . end($image)));
                $link = Storage::url('w1024' . $value);
                Storage::disk('local')->delete('app/public/' . end($image));
                return $link;
            }
        }
    }
}
