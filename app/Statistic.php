<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'user_id', 'competition_id', 'question_set_id', 'is_winner', 'sum'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_winner' => 'boolean'
    ];

    /**
     * Get the User for the Statistic.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }


    /**
     * Get the QuestionSet for the Statistic.
     */
    public function questionSet()
    {
        return $this->belongsTo(\App\QuestionSet::class);
    }


    /**
     * Get the Competition for the Statistic.
     */
    public function competition()
    {
        return $this->belongsTo(\App\Competition::class);
    }

    public function getAvatarAttribute($value)
    {
        return env('STORAGE_URL') . '/' . $value;
    }
}
