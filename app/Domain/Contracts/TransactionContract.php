<?php


namespace App\Domain\Contracts;


interface TransactionContract
{
    public const FIELD_ID           = 'id';
    public const FIELD_USER_ID      = 'user_id';
    public const FIELD_SUM          = 'sum';
    public const FIELD_TYPE         = 'type';
    public const FIELD_APPROVED     = 'approved';
    public const FIELD_PAYMENT_TYPE = 'payment_type';
    public const FIELD_NUMBER       = 'number';
    public const FIELD_EMAIL        = 'email';
    public const FIELD_PHONE        = 'phone';
    public const FIELD_CREATED_AT   = 'created_at';
    public const FIELD_UPDATED_AT   = 'updated_at';
}
