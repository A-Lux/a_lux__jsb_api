<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Read extends Model
{
    protected $table = 'polymorph_read';

    protected $fillable = ['*'];

    protected $casts = [
        'status' => 'integer'
    ];
}
