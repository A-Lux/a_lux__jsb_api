<?php

namespace App;

use App\Traits\checkUnread;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use checkUnread;

    protected $fillable = [
        'user_id',
        'sum',
        'type',
        'approved',
        'payment_type',
        'number',
        'email',
        'read'
    ];

    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function getReadReadAttribute()
    {
//        $this->read();

        return $this->read;
    }

    public function checkRead()
    {
    //     $check = Read
    //         ::query()
    //         ->where('model_name', 'like', '%Transaction')
    //         ->where('model_id', $this->id)
    //         ->first();
    //     if ($check) {
    //         return $check->status;
    //     }
    //     else {
            return true;
        // }
    }


    public static function countRead()
    {
        return 0; /* Read
            ::query()
            ->where('model_name', 'like', '%Transaction')
            ->where('status', '=', 0)
            ->count();
            */
    }
}
