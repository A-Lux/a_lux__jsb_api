<?php

namespace App\Exports;

use App\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;

class EmailExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $users = User
            ::select('email', 'created_at')
            ->get()
        ;

        foreach ($users as $user) {
            $user->register = Carbon
                ::createFromTimestamp($user->created_at)
                ->toDateTimeString()
            ;
            $user->makeHidden('created_at');
        }

        return $users;
    }
}
