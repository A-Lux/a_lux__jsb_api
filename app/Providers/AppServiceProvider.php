<?php

namespace App\Providers;

use App;
use App\Company;
use App\Difficulty;
use App\Overview;
use Laravel\Passport\Passport;
use Illuminate\Support\ServiceProvider;
use Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->registerPolicies();

        Passport::routes();

        view()->composer(['voyager::question-sets.browse'], function ($view) {
            return $view->with(['levels' => Difficulty::get(), 'companies' => Company::get()]);
        });
        view()->composer(['voyager::index'], function ($view) {
            return $view->with(['overviews' => Overview::orderBy('created_at', 'desc')->limit(100)->get()]);
        });
        App::setLocale(Request::header('Accept-Language', 'en'));
    }
}
