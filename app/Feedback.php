<?php

namespace App;

use App\Traits\checkUnread;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use checkUnread;

    protected $fillable = [
        'text',
        'user_id',
        'email',
        'read'
    ];

    protected $table = 'requests';

    public function getReadReadAttribute()
    {
//        $this->read();
        return $this->read;
    }

    public function checkRead()
    {
        // $check = Read
        //     ::query()
        //     ->where('model_name',  'like', '%Feedback')
        //     ->where('model_id', $this->id)
        //     ->first();
        // if ($check) {
        //     return $check->status;
        // }
        // else {
            return true;
        // }
    }


    public static function countRead()
    {
        return 0; /* Read
            ::query()
            ->where('model_name', 'like', '%Feedback')
            ->where('status', '=', 0)
            ->count();
            */
    }
}
