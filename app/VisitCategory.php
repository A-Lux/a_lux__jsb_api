<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitCategory extends Model
{
    protected $fillable = ['user_id', 'category_id', 'visited_at'];
}
