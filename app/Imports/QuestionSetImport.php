<?php

namespace App\Imports;

use App\Answer;
use App\Question;
use App\QuestionSet;
use DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class QuestionSetImport implements ToCollection
{
    // /**
    // * @param array $row
    // *
    // * @return \Illuminate\Database\Eloquent\Model|null
    // */

    protected $id;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function collection(Collection $rows)
    {

        $sets = $rows->chunk($this->request->amount);

        $image = Storage::put('/questions/' . time() . '/', $this->request->image);
        $ad = Storage::put('/questions/' . time() . '/', $this->request->ad);

        foreach ($sets as $questions) {
            $set = [
                'company_id' => $this->request->company,
                'time_amount' => $this->request->questionTime,
                'ad' => '',
                'image' => '',
                'name' => $this->request->name,
                'sum' => $this->request->prize,
                'difficulty_id' => $this->request->level,
                'url' => $this->request->url
            ];

            $questionSet = QuestionSet::create($set);

            $image = Storage::put('/questions/' . $questionSet->id, $this->request->image);
            $ad = Storage::put('/questions/' . $questionSet->id, $this->request->ad);

            $questionSet->update([
                'image' => $image,
                'ad' => $ad
            ]);

            $questionSet->save();

            foreach ($questions as $column) {
                $column = $column->toArray();
                if (isset($column[1])) {
                    $question = Question::create([
                        'question_set_id' => $questionSet->id,
                        'text' => $column[0]
                    ]);
                    Answer::create([
                        'is_right' => 1,
                        'text' => $column[1],
                        'question_id' => $question->id
                    ]);
                    Answer::create([
                        'is_right' => 0,
                        'text' => $column[2],
                        'question_id' => $question->id
                    ]);
                    Answer::create([
                        'is_right' => 0,
                        'text' => $column[3],
                        'question_id' => $question->id
                    ]);
                    Answer::create([
                        'is_right' => 0,
                        'text' => $column[4],
                        'question_id' => $question->id
                    ]);
                    Answer::create([
                        'is_right' => 0,
                        'text' => $column[5],
                        'question_id' => $question->id
                    ]);
                }
            }

            QuestionSet::whereNotIn('id', Question::select('question_set_id')
                ->get())
                ->delete();
        }
    }
}
