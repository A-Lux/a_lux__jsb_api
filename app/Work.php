<?php

namespace App;

use App\Like;
use App\Traits\checkUnread;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Storage;
use App\Traits\ImageResise;
use Illuminate\Support\Facades\Redis;

class Work extends Model
{
    use SoftDeletes, ImageResise, checkUnread;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'competition_id',
        'description',
        'data',
        'thumbnail',
        'width',
        'height',
        'share_amount',
        'verified',
        'read',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'deleted_at' => 'timestamp',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * Get the Likes for the Work.
     */
    public function likes()
    {
        return $this->hasMany(\App\Like::class);
    }


    /**
     * Get the Comments for the Work.
     */
    public function comments()
    {
        return $this->hasMany(\App\Comment::class);
    }


    /**
     * Get the Reports for the Work.
     */
    public function reports()
    {
        return $this->hasMany(\App\Report::class);
    }


    /**
     * Get the User for the Work.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }


    /**
     * Get the Competition for the Work.
     */
    public function competition()
    {
        return $this->belongsTo(\App\Competition::class)
                    ->withTrashed()
        ;;
    }

    public function isLiked(Request $request, $work_id)
    {
        if (Auth::guard('api')
                ->check()) {
            return Like::where(
                'user_id', Auth::guard('api')
                               ->user()->id
            )
                       ->where('work_id', $work_id)
                       ->exists()
            ;
        }
    }

    // For admin

    public function addWinnerAttribute()
    {
        $competition = \App\Competition::withTrashed()
                                       ->find($this->competition_id);
        if (($competition)->finish_at > Carbon::now()->timestamp) {
            $this->winner = -1;

            return $this;
        }
        if (($competition)->finish_at < Carbon::now()->timestamp) {
            if (!\App\Statistic::where('competition_id', $this->competition_id)
                               ->where('is_winner', 1)
                               ->exists()) {
                $this->winner = 2;

                return $this;
            }
        }
        if (($competition)->finish_at < Carbon::now()->timestamp) {
            if (\App\Statistic::where('competition_id', $this->competition_id)
                              ->where('user_id', $this->user_id)
                              ->where('is_winner', 1)
                              ->exists()
            ) {
                $this->winner = 1;

                return $this;
            }
        }
        if (($competition)->finish_at < Carbon::now()->timestamp) {
            if (!\App\Statistic::where('competition_id', $this->competition_id)
                               ->where('user_id', $this->user_id)
                               ->where('is_winner', 1)
                               ->exists()) {
                $this->winner = 0;

                return $this;
            }
        }
    }

    public function getReadReadAttribute()
    {
        return $this->read;
    }

    public function checkRead()
    {
        return true;
    }


    public static function countRead()
    {
        return 0;
    }
}
