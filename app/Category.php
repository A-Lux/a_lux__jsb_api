<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'image', 'name', 'position'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'created_at' => 'timestamp',
        // 'updated_at' => 'timestamp',
        // 'deleted_at' => 'timestamp'
    ];

    /**
     * Get the Competitions for the Category.
     */
    public function competitions()
    {
        return $this->hasMany(\App\Competition::class);
    }

    public function getImageAttribute($value)
    {
        if (Str::contains($value, 'http')) {
            return $value;
        }
        return env('APP_URL') . '/' . $value;
    }
}
