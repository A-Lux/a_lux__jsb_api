<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class verify extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('vendor.notifications.email')
            ->with([
                'actionText' => 'Нажмите на эту кнопку чтобы завершить регистрацию',
                'actionUrl' => url('/api/auth/verify') . '?token=' . $this->token . '&first=true',
                'level' => 1,
                'introLines' => [],
                'outroLines' => []
            ]);
    }
}
