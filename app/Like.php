<?php

namespace App;

use App\Events\LikeCreated;
use App\v2\Contracts\LikeContract;
use Illuminate\Database\Eloquent\Model;


class Like extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = LikeContract::FILLABLE;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * Get the User for the Like.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }


    /**
     * Get the Work for the Like.
     */
    public function work()
    {
        return $this->belongsTo(\App\Work::class);
    }

    protected $dispatchesEvents = [
        'created' => LikeCreated::class,
    ];
}
