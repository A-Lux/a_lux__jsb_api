<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Difficulty
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\QuestionSet[] $questionSets
 * @property-read int|null $question_sets_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Difficulty newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Difficulty newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Difficulty query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Difficulty whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Difficulty whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Difficulty whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Difficulty whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Difficulty whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Difficulty extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * Get the QuestionSets for the Difficulty.
     */
    public function questionSets()
    {
        return $this->hasMany(\App\QuestionSet::class);
    }

}
