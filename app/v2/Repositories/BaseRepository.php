<?php


namespace App\v2\Repositories;


use App\v2\Models\CategoryAd;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Collection;

class BaseRepository
{
    public function get(array $where, array $orderBy, array $fields = ['*'], $limit = null) : Collection
    {
        $data = $this->model
            ->query();

        foreach ($where as $condition) {
            if($condition[1] != 'IN') {
                $data->where(...$condition);
            }
            else {
                $data->whereIn($condition[0], $condition[2]);
            }
        }

        $data->select($fields);

        if (!empty($orderBy)) {
            $data->orderBy(...$orderBy);
        }

        if ($limit) {
            $data->limit($limit);
        }

        return $data->get();
    }

    /**
     * @param $filters
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allWithFilters($filters): \Illuminate\Database\Eloquent\Collection
    {
        $result = $this->model->query();

        $result = app(Pipeline::class)
            ->send($result)
            ->through(
                $filters
            )
            ->thenReturn()
        ;

        $result = $result->get();

        return $result;
    }

    /**
     * @param $filters
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateWithFilters($filters): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        $result = $this->model->query();

        $result = app(Pipeline::class)
            ->send($result)
            ->through(
                $filters
            )
            ->thenReturn()
        ;

        $result = $result
            ->paginate();

        return $result;
    }

    public function show($id, $fields=['*'])
    {
        return $this
            ->model
            ->where('id', $id)
            ->select(...$fields)
            ->first()
            ;
    }

    public function update($id, $data)
    {
        return $this
            ->model
            ->where('id', $id)
            ->update($data)
            ;
    }
}
