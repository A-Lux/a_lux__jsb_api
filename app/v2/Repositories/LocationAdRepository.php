<?php


namespace App\v2\Repositories;

use App\v2\Models\LocationAd;

class LocationAdRepository extends BaseRepository
{
    /**
     * @var \App\v2\Models\LocationAd
     */
    protected LocationAd $model;

    public function __construct()
    {
        $this->model = new LocationAd();
    }
}
