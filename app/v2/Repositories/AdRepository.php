<?php


namespace App\v2\Repositories;


use App\v2\Contracts\CategoryAdContract;
use App\v2\Contracts\CategoryContract;
use App\v2\Models\CategoryAd;
use Illuminate\Support\Facades\DB;

class AdRepository extends BaseRepository
{
    /**
     * @var \App\v2\Models\CategoryAd
     */
    protected CategoryAd $model;

    public function __construct()
    {
        $this->model = new CategoryAd();
    }

    public function main()
    {
        //TODO Limit select fields
        $categories = collect(CategoryContract::LIST)->pluck('id');

        $ads = CategoryAd
            ::query()
            ->where(CategoryAdContract::CATEGORY_ID, $categories[0])
            ->where(CategoryAdContract::IS_ON_MAIN_SCREEN, true)
            ->inRandomOrder()
            ->limit(1);
        ;

        foreach ($categories as $category_id) {
            if ($category_id === $categories[0]) {
                continue;
            }
            $query = DB
                ::table(CategoryAdContract::TABLE)
                ->where(CategoryAdContract::CATEGORY_ID, $category_id)
                ->inRandomOrder()
                ->limit(1);

            $ads->union($query);
        }

        $ads = $ads
            ->get()
        ;

        return $ads;
    }
}
