<?php


namespace App\v2\Repositories;

use App\v2\Models\Location;

class LocationRepository extends BaseRepository
{
    /**
     * @var \App\v2\Models\Location
     */
    protected Location $model;

    public function __construct()
    {
        $this->model = new Location();
    }
}
