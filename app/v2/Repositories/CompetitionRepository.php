<?php


namespace App\v2\Repositories;


use App\v2\Contracts\CategoryContract;
use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use App\v2\Models\Competition;
use App\v2\Models\Popular;
use Carbon\Carbon;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CompetitionRepository extends BaseRepository
{
    /**
     * @var \App\v2\Models\Competition
     */
    protected Competition $model;
    protected WorkRepository $workRepository;

    public function __construct()
    {
        $this->model = new Competition();
        $this->workRepository = new WorkRepository();
    }

    public function paginate(array $where, array $orderBy)
    {
        $competitions = Competition
            ::query();

        foreach ($where as $condition) {
            $competitions->where(...$condition);
        }

        if (!empty($orderBy)) {
            $competitions->orderBy(...$orderBy);
        }

        $competitions = $competitions->paginate();

        return $competitions;
    }

    public function getPopulars()
    {
        $competitions = Competition
            ::query()
            ->whereIn(
                'id',
                Popular
                    ::all()
                    ->pluck('competition_id')
                    ->toArray()
            )
            ->select(
                [
                    DB::raw(
                        '(Select count(' . WorkContract::ID . ') from ' . WorkContract::TABLE . ' where ' . WorkContract::COMPETITION_ID . ' = ' . CompetitionContract::TABLE . '.' . CompetitionContract::ID . ' AND ' . WorkContract::VERIFIED . ' = 1) as works_amount'
                    ),
                    ...CompetitionContract::SMALL,
                ]
            )
            ->inRandomOrder()
            ->limit(4)
            ->get()
            ;

        if(Auth::guard('api')->check()){
            foreach ($competitions as $competition) {
                $categoryReadAmount = Cache::get('unread-user:'.Auth::guard('api')->user()->{UserContract::ID}.'competition:'.$competition->id);
                if (empty($categoryReadAmount)) {
                    $competition = $this->workRepository->countForOneCompetition($competition);
                    Cache::put('unread-user:'.Auth::guard('api')->user()->{UserContract::ID}.'competition:'.$competition->{CompetitionContract::ID}, $competition->badge, 3600);
                }
            }
        }
        else{
            $competitions = $this->workRepository->countForCompetitions($competitions);
        }

        return $competitions;
    }

    public function getGroupedByCategoriesForMainPage()
    {
        $categories = collect(CategoryContract::LIST)->pluck('id');

        $competitions = Competition
            ::query()
            ->where('category_id', $categories[0])
            ->where('confirmed', '=', true)
            ->select(
                [
                    DB::raw(
                        '(Select count(' . WorkContract::ID . ') from ' . WorkContract::TABLE . ' where ' . WorkContract::COMPETITION_ID . ' = ' . CompetitionContract::TABLE . '.' . CompetitionContract::ID . ' AND ' . WorkContract::VERIFIED . ' = 1) as works_amount'
                    ),
                    ...CompetitionContract::SMALL,
                ]
            )
            ->orderBy(CompetitionContract::VIEWS, 'DESC')
            ->limit(16)
        ;

        foreach ($categories as $category_id) {
            if ($category_id === $categories[0]) {
                continue;
            }

            $category = DB
                ::table('competitions')
                ->where('category_id', $category_id)
                ->where('confirmed', '=', true)
                ->whereNull('deleted_at')
                ->select(
                    [
                        DB::raw(
                            '(Select count(' . WorkContract::ID . ') from ' . WorkContract::TABLE . ' where ' . WorkContract::COMPETITION_ID . ' = ' . CompetitionContract::TABLE . '.' . CompetitionContract::ID . ' AND ' . WorkContract::VERIFIED . ' = 1) as works_amount'
                        ),
                        ...CompetitionContract::SMALL,
                    ]
                )
                ->orderBy(CompetitionContract::VIEWS, 'DESC')
                ->limit(16)
            ;
            $competitions->union($category);
        }

        $competitions = $competitions
            ->get();

        if(Auth::guard('api')->check()){
            foreach ($competitions as $competition) {
                $competition->badge = Cache::get('unread-user:'.Auth::guard('api')->user()->{UserContract::ID}.'competition:'.$competition->id);
                if (empty($competition->badge)) {
                    $competition = $this->workRepository->countForOneCompetition($competition);
                    Cache::put('unread-user:'.Auth::guard('api')->user()->{UserContract::ID}.'competition:'.$competition->{CompetitionContract::ID}, $competition->badge, 3600);
                }
            }
        }
        else{
            $competitions = $this->workRepository->countForCompetitions($competitions);
        }



        $competitions = $competitions
            ->groupBy('category_id');

        $competitions->transform(function ($category) {
            if($category->count() > 4) {
                $category = $category->random(4);
            }
            return $category;
        });

        return $competitions;
    }

    /**
     * @param      $data
     * @param null $user_id
     *
     * @return mixed
     */
    public function create($data, $user_id = null): Competition
    {
        //TODO Resize images

        $ad    = Storage::put('v2/' . $user_id . '/competitions', $data['ad']);
        $image = Storage::put('v2/' . $user_id . '/competitions', $data['image']);

        return Competition::create(
            [
                'name'        => $data['name'],
                'category_id' => $data['category_id'],
                'ad'          => $ad,
                'url'         => $data['url'] ? $data['url'] : '',
                'image'       => $image,
                'min_works'   => $data['min_works'],
                'type_id'     => $data['type_id'],
                'description' => $data['description'],
                'user_id'     => $user_id,
                'start_at'    => Carbon::createFromTimestamp($data['start_at'] / 1000),
                'finish_at'   => Carbon::createFromTimestamp($data['finish_at'] / 1000),
                'email'       => $data['email'],
            ]
        );
    }

    public function countForCategories()
    {
        $result = Competition
            ::query()
            ->select(CompetitionContract::CATEGORY_ID, DB::raw('count(*) as badge'))
            ->groupBy(CompetitionContract::CATEGORY_ID)
            ->get()
        ;

        return $result;
    }

    public function countForOneCategory($id)
    {
        $result = Competition
            ::query()
            ->select(CompetitionContract::CATEGORY_ID, DB::raw('count(*) as badge'))
            ->where(CompetitionContract::CATEGORY_ID, $id)
            ->groupBy(CompetitionContract::CATEGORY_ID)
            ->first()
            ->badge
        ;

        return $result;
    }
}
