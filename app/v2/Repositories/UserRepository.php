<?php


namespace App\v2\Repositories;


use App\v2\Contracts\UserContract;
use App\v2\Models\User;

class UserRepository extends BaseRepository
{
    /**
     * @var \App\v2\Models\Competition
     */
    protected User $model;

    public function __construct()
    {
        $this->model = new User();
    }

    public function showByEmail($email)
    {
        return User::query()->where(UserContract::EMAIL, $email)->first();
    }
}
