<?php


namespace App\v2\Repositories;


use App\Report;
use App\v2\Contracts\CategoryAdContract;
use App\v2\Contracts\CategoryContract;
use App\v2\Models\CategoryAd;
use Illuminate\Support\Facades\DB;

class ComplaintRepository extends BaseRepository
{
    /**
     * @var \App\Report
     */
    protected Report $model;

    public function __construct()
    {
        $this->model = new Report();
    }
}
