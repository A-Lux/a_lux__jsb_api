<?php


namespace App\v2\Repositories;

use App\Comment;

class CommentRepository extends BaseRepository
{
    /**
     * @var \App\Comment
     */
    protected Comment $model;

    public function __construct()
    {
        $this->model = new Comment();
    }
}
