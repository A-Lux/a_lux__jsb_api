<?php


namespace App\v2\Repositories;

use App\Like;

class LikeRepository extends BaseRepository
{
    /**
     * @var \App\Like
     */
    protected Like $model;

    public function __construct()
    {
        $this->model = new Like();
    }
}
