<?php


namespace App\v2\Repositories;


use App\Like;
use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\WorkContract;
use App\v2\Models\Competition;
use App\v2\Models\User;
use App\v2\Models\Work;
use Illuminate\Database\Eloquent\Collection;

class WorkRepository extends BaseRepository
{
    /**
     * @var \App\v2\Models\Competition
     */
    protected Work $model;

    public function __construct()
    {
        $this->model = new Work();
    }

    public function checkLiked($user_id, $id)
    {
        return Like
            ::query()
            ->where('work_id', $id)
            ->where('user_id', $user_id)
            ->exists()
            ;
    }

    public function countForOneCompetition(Competition $competition): Competition
    {
        $competition->badge = Work
            ::query()
            ->where(WorkContract::COMPETITION_ID, $competition->{CompetitionContract::ID})
            ->count()
        ;

        return $competition;
    }

    public function countForCompetitions(Collection $competitions): Collection
    {
        foreach ($competitions as $competition) {
            $competition->badge = Work
                ::query()
                ->where(WorkContract::COMPETITION_ID, $competition->{CompetitionContract::ID})
                ->count()
            ;
        }

        return $competitions;
    }
}
