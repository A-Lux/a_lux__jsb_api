<?php


namespace App\v2\Services;


use App\v2\Contracts\CategoryAdContract;
use App\v2\Contracts\CommentContract;
use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use App\v2\Repositories\AdRepository;
use App\v2\Repositories\CommentRepository;
use App\v2\Repositories\CompetitionRepository;
use App\v2\Repositories\UserRepository;
use App\v2\Repositories\WorkRepository;
use App\v2\Responses\ApiResponse;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class CommentService
{
    private CommentRepository $commentRepository;
    private UserRepository $userRepository;
    private WorkRepository $workRepository;
    private CompetitionRepository $competitionRepository;
    private \Aws\S3\S3Client $storage;

    public function __construct(CommentRepository $commentRepository, UserRepository $userRepository, WorkRepository $workRepository, CompetitionRepository $competitionRepository)
    {
        $this->commentRepository     = $commentRepository;
        $this->userRepository        = $userRepository;
        $this->workRepository        = $workRepository;
        $this->competitionRepository = $competitionRepository;
        $this->storage               = Storage
            ::disk('s3')
            ->getAdapter()
            ->getClient();

    }

    public function universal($filters = [])
    {
        $result = $this
            ->commentRepository
            ->paginateWithFilters($filters);

        $result->getCollection()
               ->transform(function ($comment) {
                   $comment->user                                           = $this->userRepository->show(
                       $comment->{CommentContract::USER_ID}
                   );
                   $comment->work                                           = $this->workRepository->show(
                       $comment->{CommentContract::WORK_ID}
                   );
                   $comment->work->competition                              = $this->competitionRepository->show(
                       $comment->work->{WorkContract::COMPETITION_ID}
                   );
                   $comment->work->competition->{CompetitionContract::USER} = $this->userRepository->show(
                       $comment->work->competition->{CompetitionContract::USER_ID}
                   );

                   if (isset($comment->user->{UserContract::AVATAR})) {
                       $comment->user->{UserContract::AVATAR} = $this->storage->getObjectUrl(
                           env('AWS_BUCKET'),
                           $comment->user->{UserContract::AVATAR}
                       );
                   }

                   if (isset($comment->work->{WorkContract::DATA})) {
                       $comment->work->{WorkContract::DATA} = $this->storage->getObjectUrl(
                           env('AWS_BUCKET'), $comment->work->{WorkContract::DATA}
                       );
                   }

                   if (isset($comment->work->competition->{CompetitionContract::IMAGE})) {
                       $comment->work->competition->{CompetitionContract::IMAGE} = $this->storage->getObjectUrl(
                           env('AWS_BUCKET'), $comment->work->competition->{CompetitionContract::IMAGE}
                       );
                   }
                   if (isset($comment->work->competition->{CompetitionContract::USER}->{UserContract::AVATAR})) {
                       $comment->work->competition->{CompetitionContract::USER}->{UserContract::AVATAR} = $this->storage->getObjectUrl(
                           env('AWS_BUCKET'),
                           $comment->work->competition->{CompetitionContract::USER}->{UserContract::AVATAR}
                       );
                   }

                   return $comment;
               })
        ;

        return ApiResponse::success($result, Response::HTTP_OK);
    }

}
