<?php


namespace App\v2\Services;


use App\v2\Contracts\CommentContract;
use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use App\v2\Repositories\CompetitionRepository;
use App\v2\Repositories\ComplaintRepository;
use App\v2\Repositories\UserRepository;
use App\v2\Repositories\WorkRepository;
use App\v2\Responses\ApiResponse;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class ComplaintService
{
    private ComplaintRepository $complaintRepository;
    private UserRepository $userRepository;
    private WorkRepository $workRepository;
    private CompetitionRepository $competitionRepository;
    private \Aws\S3\S3Client $storage;

    public function __construct(
        ComplaintRepository   $complaintRepository,
        UserRepository        $userRepository,
        WorkRepository        $workRepository,
        CompetitionRepository $competitionRepository
    ) {
        $this->complaintRepository   = $complaintRepository;
        $this->userRepository        = $userRepository;
        $this->workRepository        = $workRepository;
        $this->competitionRepository = $competitionRepository;
        $this->storage               = Storage::disk('s3')
                                              ->getAdapter()
                                              ->getClient();

    }

    public function universal($filters = [])
    {
        $result = $this
            ->complaintRepository
            ->paginateWithFilters($filters);

        $result->getCollection()
               ->transform(function ($complaint) {
                   $complaint->user                                           = $this->userRepository->show(
                       $complaint->{CommentContract::USER_ID}
                   );
                   $complaint->work                                           = $this->workRepository->show(
                       $complaint->{CommentContract::WORK_ID}
                   );
                   $complaint->work->competition                              = $this->competitionRepository->show(
                       $complaint->work->{WorkContract::COMPETITION_ID}
                   );
                   $complaint->work->competition->{CompetitionContract::USER} = $this->userRepository->show(
                       $complaint->work->competition->{CompetitionContract::USER_ID}
                   );

                   if (isset($complaint->user->{UserContract::AVATAR})) {
                       $complaint->user->{UserContract::AVATAR} = $this->storage->getObjectUrl(
                           env('AWS_BUCKET'),
                           $complaint->user->{UserContract::AVATAR}
                       );
                   }

                   if (isset($complaint->work->{WorkContract::DATA})) {
                       $complaint->work->{WorkContract::DATA} = $this->storage->getObjectUrl(
                           env('AWS_BUCKET'), $complaint->work->{WorkContract::DATA}
                       );
                   }

                   if (isset($complaint->work->competition->{CompetitionContract::IMAGE})) {
                       $complaint->work->competition->{CompetitionContract::IMAGE} = $this->storage->getObjectUrl(
                           env('AWS_BUCKET'), $complaint->work->competition->{CompetitionContract::IMAGE}
                       );
                   }
                   if (isset($complaint->work->competition->{CompetitionContract::USER}->{UserContract::AVATAR})) {
                       $complaint->work->competition->{CompetitionContract::USER}->{UserContract::AVATAR} = $this->storage->getObjectUrl(
                           env('AWS_BUCKET'),
                           $complaint->work->competition->{CompetitionContract::USER}->{UserContract::AVATAR}
                       );
                   }

                   return $complaint;
               })
        ;

        return ApiResponse::success($result, Response::HTTP_OK);
    }

}
