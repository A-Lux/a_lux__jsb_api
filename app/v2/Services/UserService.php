<?php


namespace App\v2\Services;

use App\Comment;
use App\v2\Contracts\CommentContract;
use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use App\v2\Models\Competition;
use App\v2\Models\User;
use App\v2\Models\Work;
use App\v2\Repositories\UserRepository;
use App\v2\Responses\ApiResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class UserService
{
    private UserRepository $userRepository;
    private \Aws\S3\S3Client $storage;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
        $this->storage        = Storage
            ::disk('s3')
            ->getAdapter()
            ->getClient();
    }

    public function paginateUniversal($filters = [])
    {
        $result = $this
            ->userRepository
            ->paginateWithFilters($filters);

        return ApiResponse::success($result, Response::HTTP_OK);
    }

    public function show($id)
    {
        $result = $this->userRepository->show($id);

        return ApiResponse::success($result, Response::HTTP_OK);
    }

    public function login($data)
    {
        if (Auth::attempt($data)) {
            $user  = $this->userRepository->showByEmail($data[UserContract::EMAIL]);
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;

            return ApiResponse::success(['token' => $token], Response::HTTP_OK);
        }

        return ApiResponse::failure(['token' => null], Response::HTTP_FORBIDDEN);
    }

    public function delete($id): array
    {
        Comment::query()->where(CommentContract::USER_ID, $id)->delete();
        Work::query()->where(WorkContract::USER_ID)->delete();
        Competition::query()->where(CompetitionContract::USER_ID, $id)->delete();
        User
            ::query()
            ->where(UserContract::ID, $id)
            ->delete()
        ;

        return ApiResponse::success([], Response::HTTP_NO_CONTENT);
    }

    public function update($id, $data): array
    {
        $this->userRepository->update($id, $data);

        return ApiResponse::success([], Response::HTTP_NO_CONTENT);
    }

    // //TODO Refactor
    // public function today()
    // {
    //     $result = [];
    //     $result['userList'] = Statistic
    //         ::query()
    //         ->select(
    //             'users.name',
    //             'users.avatar',
    //             'statistics.user_id',
    //             DB::raw('SUM(sum) as rating')
    //         )
    //         ->join('users', 'users.id', 'statistics.user_id')
    //         ->where('is_winner', 1)
    //         ->whereNotNull('users.name')
    //         ->groupBy('user_id', 'name', 'avatar')
    //         ->inRandomOrder()
    //         ->get();
    //     $result['activity'] = User::query()->count() * 10; //Like::count() + User::count() + Work::count() + Comment::count() + Work::sum('share_amount') + Competition::sum('share') + QuestionSet::sum('share');

    //     return response($result, 200);
    // }
}
