<?php

namespace App\v2\Services;

use App\v2\Contracts\LocationContract;
use App\v2\Repositories\LocationRepository;
use App\v2\Responses\ApiResponse;
use Stevebauman\Location\Facades\Location;
use Symfony\Component\HttpFoundation\Response;

class LocationService
{
    private LocationRepository $locationRepository;

    public function __construct(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }
    public function getLocationByIP() : array
    {
        return ApiResponse::success(Location::get(), Response::HTTP_OK);
    }

    /**
     * @param string $query
     *
     * @return array
     */
    public function searchLocations(string $query = ''): array
    {
        // TODO Optimize
        $locations = $this->locationRepository->get([[LocationContract::CITY_NAME, 'LIKE', '%' . $query . '%']], [LocationContract::CITY_NAME, 'ASC']);

        return ApiResponse::success($locations, Response::HTTP_OK);
    }
}
