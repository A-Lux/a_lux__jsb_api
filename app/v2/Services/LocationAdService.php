<?php

namespace App\v2\Services;

use App\v2\Contracts\LocationContract;
use App\v2\Repositories\LocationAdRepository;
use App\v2\Responses\ApiResponse;
use Stevebauman\Location\Facades\Location;
use Symfony\Component\HttpFoundation\Response;

class LocationAdService
{
    private LocationAdRepository $adRepository;

    public function __construct(LocationAdRepository $locationAdRepository)
    {
        $this->adRepository = $locationAdRepository;
    }

    public function getAdsByIP()
    {
        $location = Location::get();
        dd($location);
    }
}
