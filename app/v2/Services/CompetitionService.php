<?php


namespace App\v2\Services;


use App\Jobs\FinishCompetition;
use App\v2\Contracts\CategoryAdContract;
use App\v2\Contracts\CategoryContract;
use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\CompetitionTypeContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use App\v2\Models\Popular;
use App\v2\Models\Work;
use App\v2\Repositories\AdRepository;
use App\v2\Repositories\CompetitionRepository;
use App\v2\Repositories\UserRepository;
use App\v2\Repositories\WorkRepository;
use App\v2\Responses\ApiResponse;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class CompetitionService
{
    private CompetitionRepository $competitionRepository;
    private UserRepository $userRepository;
    private \Aws\S3\S3Client $storage;
    protected AdRepository $adRepository;
    protected WorkRepository $workRepository;

    public function __construct(
        CompetitionRepository $competitionRepository,
        UserRepository        $userRepository,
        AdRepository          $adRepository,
        WorkRepository        $workRepository
    ) {
        $this->competitionRepository = $competitionRepository;
        $this->userRepository        = $userRepository;
        $this->storage               = Storage::disk('s3')
                                              ->getAdapter()
                                              ->getClient();
        $this->adRepository          = $adRepository;
        $this->workRepository        = $workRepository;
    }

    public function main()
    {
        $competitions = $this->competitionRepository->getGroupedByCategoriesForMainPage();
        $new          = $this->competitionRepository->get(
            [
                [
                    CompetitionContract::IS_CONFIRMED, '=', true,
                ],
            ],
            [CompetitionContract::CREATED_AT, 'desc'],
            [
                DB::raw(
                    '(Select count(' . WorkContract::ID . ') from ' . WorkContract::TABLE . ' where ' . WorkContract::COMPETITION_ID . ' = ' . CompetitionContract::TABLE . '.' . CompetitionContract::ID . ' AND ' . WorkContract::VERIFIED . ' = 1) as works_amount'
                ),
                ...CompetitionContract::SMALL,
            ],
            4
        );

        if (Auth::guard('api')
                ->check()) {
            foreach ($new as $competition) {
                $competition->badge = Cache::get(
                    'unread-user:' . Auth::guard('api')
                                         ->user()->{UserContract::ID} . 'competition:' . $competition->id
                );
                if (is_null($competition->badge)) {
                    $competition = $this->workRepository->countForOneCompetition($competition);
                    Cache::put(
                        'unread-user:' . Auth::guard('api')
                                             ->user(
                                             )->{UserContract::ID} . 'competition:' . $competition->{CompetitionContract::ID},
                        $competition->badge, 3600
                    );
                }
            }
        } else {
            $new = $this->workRepository->countForCompetitions($new);
        }

        $populars = $this->competitionRepository->getPopulars();
        $ads      = $this->adRepository->main();
        $competitions->prepend($new);
        $competitions->prepend($populars);
        $formed = [];
        $i      = 0;
        foreach ($competitions as $category) {
            $id    = null;
            $name  = '';
            $image = '';
            $ad    = null;
            switch ($i) {
                case 0:
                    $id   = 'Popular';
                    $name = CategoryContract::popular[env('SERVER_LANG')];;
                    $image = 'https://ewr1.vultrobjects.com/general/categories/July2020/vPJA08zYIpO92GXUgN3I.png';
                    break;
                case 1:
                    $id   = 'New';
                    $name = CategoryContract::newConst[env('SERVER_LANG')];;
                    $image = 'https://ewr1.vultrobjects.com/general/new-3-3.png';
                    break;
                default:
                    $cat   = collect(CategoryContract::LIST)
                        ->where(
                            'id', $category
                            ->pluck(CompetitionContract::CATEGORY_ID)
                            ->first()
                        )
                        ->first();
                    $id    = $category->pluck(CompetitionContract::CATEGORY_ID)
                                      ->first();
                    $ad    = $ads->where(CategoryAdContract::CATEGORY_ID, $id)
                                 ->first();
                    $name  = $cat['name'][env('SERVER_LANG')];
                    $image = $cat['image'];
                    break;
            }


            $formed[] = [
                'id'       => $id,
                'ad'       => $ad,
                'category' => $name,
                'image'    => $image,
                'list'     => $category,
            ];

            ++$i;
        }

        foreach ($formed as $category) {
            if (!empty($category['ad'])) {
                $category['ad']->{CategoryAdContract::IMAGE} = $this->storage->getObjectUrl(
                    env('AWS_BUCKET'), $category['ad']->{CategoryAdContract::IMAGE}
                );
            }
            foreach ($category['list'] as $competition) {
                $competition->{CompetitionContract::IMAGE} = $this->storage->getObjectUrl(
                    env('AWS_BUCKET'), $competition->{CompetitionContract::IMAGE}
                );
            }
        }

        return ApiResponse::success($formed, Response::HTTP_OK);
    }

    public function types()
    {
        $types = CompetitionTypeContract::LIST;

        return ApiResponse::success($types, Response::HTTP_OK);
    }

    public function paginateUniversal($filters = [])
    {
        $result = $this
            ->competitionRepository
            ->paginateWithFilters($filters);

        $collection = $result->getCollection();

        $checkUsers = $collection->filter(function ($value, $key) {
            return isset($value->{CompetitionContract::USER_ID});
        })
                                 ->isNotEmpty();

        $users = collect([]);
        if ($checkUsers) {
            $users = $this->userRepository->get(
                [
                    [UserContract::ID, 'IN', $collection->pluck(CompetitionContract::USER_ID)
                                                        ->toArray()],
                ],
                [UserContract::ID, 'DESC']
            );
        }

        $result
            ->getCollection()
            ->transform(
                function ($item) use ($users, $checkUsers) {
                    if (isset($item->{CompetitionContract::IMAGE})) {
                        $item->{CompetitionContract::IMAGE} = $this->storage->getObjectUrl(
                            env('AWS_BUCKET'), $item->{CompetitionContract::IMAGE}
                        );
                    }

                    if (isset($item->{CompetitionContract::AD})) {
                        $item->{CompetitionContract::AD} = $this->storage->getObjectUrl(
                            env('AWS_BUCKET'), $item->{CompetitionContract::AD}
                        );
                    }

                    if (isset($item->{CompetitionContract::CATEGORY_ID})) {
                        $item->{CompetitionContract::CATEGORY} = collect(CategoryContract::LIST)
                            ->where(CategoryContract::ID, $item->{CompetitionContract::CATEGORY_ID})
                            ->first();
                    }

                    if ($checkUsers) {
                        $item->{CompetitionContract::USER}                         = $users->where(
                            UserContract::ID, $item->{CompetitionContract::USER_ID}
                        )
                                                                                           ->first();
                        $item->{CompetitionContract::USER}->{UserContract::AVATAR} = $this->storage->getObjectUrl(
                            env('AWS_BUCKET'), $item->{CompetitionContract::USER}->{UserContract::AVATAR}
                        );
                    }

                    $item = $this->workRepository->countForOneCompetition($item);

                    return $item;
                }
            )
        ;

        return ApiResponse::success($result, Response::HTTP_OK);
    }

    public function show($id)
    {
        $result = $this
            ->competitionRepository
            ->show($id, CompetitionContract::FULL);

        //TODO Add try/Catch and commit
        $this
            ->competitionRepository
            ->update($id, [CompetitionContract::VIEWS => $result->{CompetitionContract::VIEWS} + 1])
        ;

        if ($result && $result->{CompetitionContract::USER_ID}) {
            $result->user                         = $this->userRepository->show(
                $result->{CompetitionContract::USER_ID},
                [
                    UserContract::ID,
                    UserContract::AVATAR,
                    UserContract::NAME,
                    UserContract::COUNTRY_NAME,
                    UserContract::CITY_NAME,
                ]
            );
            $result->user->{UserContract::AVATAR} = $this->storage->getObjectUrl(
                env('AWS_BUCKET'), $result->user->{UserContract::AVATAR}
            );
            $result->{CompetitionContract::AD}    = $this->storage->getObjectUrl(
                env('AWS_BUCKET'), $result->{CompetitionContract::AD}
            );

            //TODO Refactor
            $result->work_amount = Work
                ::query()
                ->where(WorkContract::COMPETITION_ID, $id)
                ->count();

            $result->{CompetitionContract::START_AT}  = Carbon::make($result->{CompetitionContract::START_AT})
                                                              ->toISOString();
            $result->{CompetitionContract::FINISH_AT} = Carbon::make($result->{CompetitionContract::FINISH_AT})
                                                              ->toISOString();

            $result->finish    = $result->{CompetitionContract::FINISH_AT} < Carbon::now();

            if(Auth::guard('api')->check()) {
                $result->completed = Work
                    ::query()
                    ->where(WorkContract::COMPETITION_ID, $result->{CompetitionContract::ID})
                    ->where(WorkContract::USER_ID, Auth::guard('api')
                                                       ->user()->id
                    )
                    ->exists();
            }
        }

        return ApiResponse::success($result, Response::HTTP_OK);
    }

    public function create($data, $user_id = null)
    {
        $competition = $this->competitionRepository->create($data, $user_id);

        FinishCompetition
            ::dispatch($competition->id)
            ->delay(Carbon::createFromTimestamp($competition->finish_at))
        ;

        return ApiResponse::success(
            [
                'competition' => $competition
            ], Response::HTTP_OK
        );
    }

    public function update($id, $data)
    {
        $this->competitionRepository->update($id, $data);

        return ApiResponse::success($data, Response::HTTP_OK);
    }

    public function addToPopular($id)
    {
        Popular::query()->create(
            [
                'competition_id' => $id
            ]
        );
    }

    public function deleteFromPopular($id)
    {
        Popular::query()->where('competition_id', $id)->delete();
    }
}
