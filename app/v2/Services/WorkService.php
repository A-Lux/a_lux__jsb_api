<?php


namespace App\v2\Services;


use App\Comment;
use App\Like;
use App\Statistic;
use App\v2\Contracts\CategoryContract;
use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use App\v2\Models\Work;
use App\v2\Repositories\CompetitionRepository;
use App\v2\Repositories\UserRepository;
use App\v2\Repositories\WorkRepository;
use App\v2\Responses\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class WorkService
{
    private CompetitionRepository $competitionRepository;
    private WorkRepository $workRepository;
    private UserRepository $userRepository;
    private \Aws\S3\S3Client $storage;

    public function __construct(WorkRepository $workRepository, UserRepository $userRepository, CompetitionRepository $competitionRepository)
    {
        $this->competitionRepository = $competitionRepository;
        $this->workRepository = $workRepository;
        $this->userRepository = $userRepository;
        $this->storage        = Storage::disk('s3')
                                       ->getAdapter()
                                       ->getClient();
    }

    public function universal($filters): array
    {
        $result = $this
            ->workRepository
            ->paginateWithFilters($filters);

        $competitions = $this->competitionRepository->get(
            [[CompetitionContract::ID, 'IN', $result->getCollection()->pluck('competition_id')->toArray()]],
            [CompetitionContract::ID, 'DESC']
        );

        $result
            ->getCollection()
            ->transform(
                function ($item) use ($competitions) {
                    if (!empty($item->{WorkContract::DATA})) {
                        $item->{WorkContract::DATA} = $this->storage->getObjectUrl(
                            env('AWS_BUCKET'), $item->{WorkContract::DATA}
                        );
                    }
                    $item->competition_type = $competitions->where(CompetitionContract::ID, $item->{WorkContract::COMPETITION_ID})->first()->{CompetitionContract::TYPE_ID};
                    //TODO Refactor
                    $item->likes_amount = Like
                        ::query()
                        ->where('work_id', $item->id)
                        ->count();
                    $item->comments_amount = Comment
                        ::query()
                        ->where('work_id', $item->id)
                        ->count();
                    $item->user = $this
                        ->userRepository->show(
                            $item->{WorkContract::USER_ID},
                            [
                                UserContract::ID,
                                UserContract::AVATAR,
                                UserContract::NAME,
                                UserContract::COUNTRY_NAME,
                                UserContract::CITY_NAME,
                            ]
                        );

                    $item->user->{UserContract::AVATAR} = env(
                            'STORAGE_URL'
                        ) . '/' . $item->user->{UserContract::AVATAR};


                    if (Auth::guard('api')
                            ->check()) {
                        $item->is_liked = $this->workRepository->checkLiked(
                            Auth::guard('api')
                                ->user()->{UserContract::ID}, $item->id
                        );
                    } else {
                        $item->is_liked = false;
                    }

                    return $item;
                }
            )
        ;

        return ApiResponse::success($result, Response::HTTP_OK);
    }

    public function create($data, $competition_id): array
    {
        $link = '';
        if (!empty($data['file'])) {
            $link = Storage::put('v2/users/' . request()->user()->id . '/competitions/works', $data['file']);
        }
        $premoderation = DB
            ::table('settings')
            ->where('key', 'admin.work_premoderation')
            ->first();
        $work          = [
            'competition_id' => $competition_id,
            'user_id'        => Auth
                ::guard('api')
                ->user()
                ->id,
            'description'    => $data['description'],
            'data'           => $link,
            'verified'       => 1//$premoderation ? ($premoderation->value === 1 ? 0 : 1) : 1,
        ];

        $competition = $this->competitionRepository->show($competition_id);

        if($competition->finish_at < Carbon::now()) {
            return ApiResponse::failure('', Response::HTTP_FORBIDDEN);
        }

        $works = $this->workRepository->get(
            [
                [WorkContract::COMPETITION_ID, '=', $competition_id],
                [WorkContract::USER_ID, '=', request()->user()->id]
            ],
            [WorkContract::ID, 'DESC']
        );

        if($works->isNotEmpty()) {
            return ApiResponse::failure('', Response::HTTP_FORBIDDEN);
        }

        Work
            ::query()
            ->create($work)
        ;

        Statistic
            ::query()
            ->create(
                [
                    'competition_id' => $competition_id,
                    'user_id'        => Auth
                        ::guard('api')
                        ->user()
                        ->id,
                    'sum'            => 0,
                    'is_winner'      => false,
                ]
            )
        ;

        if ($premoderation === 1) {
            return ApiResponse::success([], Response::HTTP_OK);
        }

        return ApiResponse::success([], Response::HTTP_CREATED);
    }

    public function like($work_id)
    {

    }
}
