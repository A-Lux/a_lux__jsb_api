<?php


namespace App\v2\Services;


use App\v2\Contracts\CategoryAdContract;
use App\v2\Repositories\AdRepository;
use App\v2\Responses\ApiResponse;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Storage;

class AdService
{
    private AdRepository $adRepository;
    private \Aws\S3\S3Client $storage;

    public function __construct(AdRepository $adRepository)
    {
        $this->adRepository = $adRepository;
        $this->storage      = Storage
            ::disk('s3')
            ->getAdapter()
            ->getClient()
        ;
    }

    public function getByCategory($id): array
    {
        $result = $this->adRepository->get(
            [
                [
                    CategoryAdContract::CATEGORY_ID, '=', $id,
                    CategoryAdContract::IS_ON_MAIN_SCREEN, '<>', false
                ],
            ],
            [CategoryAdContract::ID, 'desc'],
            [
                CategoryAdContract::ID,
                CategoryAdContract::CATEGORY_ID,
                CategoryAdContract::IMAGE,
                CategoryAdContract::LINK
            ]
        );

        $result
            ->transform(
                function ($item) {
                    $item->{CategoryAdContract::IMAGE} = $this->storage->getObjectUrl(
                        env('AWS_BUCKET'), $item->{CategoryAdContract::IMAGE}
                    );

                    return $item;
                });

        return ApiResponse::success($result, Response::HTTP_OK);
    }
}
