<?php


namespace App\v2\Services;


use App\v2\Contracts\CategoryContract;
use App\v2\Contracts\UserContract;
use App\v2\Repositories\CompetitionRepository;
use App\v2\Responses\ApiResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class CategoryService
{
    private CompetitionRepository $competitionRepository;

    public function __construct(CompetitionRepository $competitionRepository)
    {
        $this->competitionRepository = $competitionRepository;
    }

    public function get()
    {
        $result = collect(CategoryContract::LIST);
        $result = $result->map(
            function ($category) {
                $category['name'] = $category['name'][env('SERVER_LANG')];

                return $category;
            }
        );

        return ApiResponse::success($result, Response::HTTP_OK);
    }

    public function getBadges()
    {
        $categories = collect(CategoryContract::LIST)->pluck('id');
        $result = [];

        if(Auth::guard('api')->check()){
            foreach ($categories as $category) {
                $categoryReadAmount = Cache::get('unread-user:'.Auth::guard('api')->user()->{UserContract::ID}.'category:'.$category);
                if (is_null($categoryReadAmount)) {
                    $categoryReadAmount = $this->competitionRepository->countForOneCategory($category);
                    Cache::put('unread-user:'.Auth::guard('api')->user()->{UserContract::ID}.'category:'.$category, $categoryReadAmount, 3600);
                }
                $result[] = [
                    'category_id' => $category,
                    'badge' => $categoryReadAmount
                ];
            }
        }
        else{
            $result = $this->competitionRepository->countForCategories();
        }

        return ApiResponse::success($result, Response::HTTP_OK);
    }
}
