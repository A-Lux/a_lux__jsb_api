<?php

namespace App\v2\Contracts;

interface PurchasesContract
{
    public const LIST = [
        'ADD_TO_POPULAR'          => 'popular_category',
        'INCREASE_CONTEST_AMOUNT' => 'add_contest_1',
        'ALLOW_CONTEST_LINK'      => 'contest_link',
        'SHOW_ON_MAIN_SCREEN'     => 'visible_part_category',
    ];
}
