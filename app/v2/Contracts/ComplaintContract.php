<?php


namespace App\v2\Contracts;


interface ComplaintContract
{
    public const TABLE = 'reports';

    public const ID             = 'id';
    public const USER_ID        = 'user_id';
    public const WORK_ID        = 'work_id';
    public const REPORT_TYPE_ID = 'report_type_id';
    public const TEXT           = 'text';
    public const CREATED_AT     = 'created_at';
    public const UPDATED_AT     = 'updated_at';

    public const REPORT_TYPES = [
        1 => 'Marketing spamming, fraud',
        2 => 'Discrimination, violence, sex',
        3 => 'Dangerous items, tobacco and alcohol',
        4 => 'Other'
    ];

    public const FILLABLE = [
        self::USER_ID,
        self::WORK_ID,
        self::USER_ID,
        self::REPORT_TYPE_ID,
        self::TEXT,
    ];
}
