<?php


namespace App\v2\Contracts;


interface LocationAdMediaContract
{
    public const TABLE = 'location_ads_medias';

    public const ID         = 'uuid';
    public const AD_ID      = 'ad_id';
    public const FILE_URL   = 'file_url';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'upddated_at';

    public const FILLABLE = [
        self::AD_ID,
        self::FILE_URL,
    ];
}
