<?php


namespace App\v2\Contracts;


interface CategoryAdContract
{
    public const TABLE = 'category_ad';

    public const ID                = 'id';
    public const IMAGE             = 'img';
    public const CATEGORY_ID       = 'category_id';
    public const LINK              = 'url';
    public const IS_ON_MAIN_SCREEN = 'is_on_main_screen';

    public const FILLABLE = [
        self::IMAGE,
        self::CATEGORY_ID,
        self::LINK,
    ];
}
