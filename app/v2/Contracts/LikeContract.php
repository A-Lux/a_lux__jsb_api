<?php


namespace App\v2\Contracts;


interface LikeContract
{
    public const TABLE = 'likes';

    public const ID = 'id';
    public const USER_ID = 'user_id';
    public const WORK_ID = 'work_id';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';

    public const FILLABLE = [
        self::USER_ID,
        self::WORK_ID,
        self::USER_ID,
    ];
}
