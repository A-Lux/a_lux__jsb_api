<?php


namespace App\v2\Contracts;


interface RoleContract
{
    public const TYPE = 'name';
    public const NAME = 'display_name';
}
