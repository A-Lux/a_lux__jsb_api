<?php

namespace App\v2\Contracts;

interface LocationContract
{
    public  const ID = 'id';
    public  const GEONAME_ID = 'geoname_id';
    public  const LOCALE_CODE = 'locale_code';
    public  const CONTINENT_CODE = 'continent_code';
    public  const CONTINENT_NAME = 'continent_name';
    public  const COUNTRY_ISO_CODE = 'country_iso_code';
    public  const COUNTRY_NAME = 'country_name';
    public  const SUBDIVISION_1_ISO_CODE = 'subdivision_1_iso_code';
    public  const SUBDIVISION_1_NAME = 'subdivision_1_name';
    public  const SUBDIVISION_2_ISO_CODE = 'subdivision_2_iso_code';
    public  const SUBDIVISION_2_NAME = 'subdivision_2_name';
    public  const CITY_NAME = 'city_name';
    public  const METRO_CODE = 'metro_code';
    public  const TIME_ZONE = 'time_zone';
    public  const IS_IN_EUROPEAN_UNION = 'is_in_european_union';
}
