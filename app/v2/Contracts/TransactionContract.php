<?php


namespace App\v2\Contracts;


interface TransactionContract
{
    public const TABLE = 'transactions';

    public const ID = 'id';
    public const USER_ID = 'user_id';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';

    public const FILLABLE = [
        self::USER_ID,
    ];
}
