<?php


namespace App\v2\Contracts;


interface CompetitionTypeContract
{
    public const LIST =
        [
            [
                'id' => 1,
                'name' => 'Photo',
                'value' => 'images'
            ],
            [
                'id' => 2,
                'name' => 'Text',
                'value' => 'plainText'
            ],
            [
                'id' => 3,
                'name' => 'Video',
                'value' => 'video'
            ],
        ];
}
