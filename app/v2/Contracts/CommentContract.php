<?php


namespace App\v2\Contracts;


interface CommentContract
{
    public const TABLE = 'comments';

    public const ID  = 'id';
    public const WORK_ID  = 'work_id';
    public const USER_ID  = 'user_id';
    public const TEXT     = 'text';
    public const VERIFIED = 'verified';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    public const FILLABLE = [
        self::WORK_ID,
        self::USER_ID,
        self::TEXT,
        self::VERIFIED,
    ];
}
