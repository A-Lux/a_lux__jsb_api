<?php


namespace App\v2\Contracts;


interface WorkContract
{
    public const TABLE = 'works';

    public const ID             = 'id';
    public const COMPETITION_ID = 'competition_id';
    public const USER_ID        = 'user_id';
    public const DESCRIPTION    = 'description';
    public const DATA           = 'data';
    public const SHARE_AMOUNT   = 'share_amount';
    public const VERIFIED       = 'verified';
    public const HIGHLIGHT      = 'highlight_id';
    public const CREATED_AT     = 'created_at';

    public const MOBILE_FIELDS = [
        self::ID,
        self::COMPETITION_ID,
        self::USER_ID,
        self::DESCRIPTION,
        self::DATA,
        self::CREATED_AT,
    ];

    public const FILLABLE = [
        self::COMPETITION_ID,
        self::USER_ID,
        self::DESCRIPTION,
        self::DATA,
        self::VERIFIED,
    ];
}
