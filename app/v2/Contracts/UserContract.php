<?php


namespace App\v2\Contracts;


interface UserContract
{
    public const TABLE = 'users';

    public const ID                     = 'id';
    public const AVATAR                 = 'avatar';
    public const NAME                   = 'name';
    public const EMAIL                  = 'email';
    public const COUNTRY_NAME           = 'country_name';
    public const COUNTRY_ID             = 'country_id';
    public const CITY_NAME              = 'city_name';
    public const CITY_ID                = 'city_id';
    public const ROLE                   = 'role_id';
    public const PASSWORD               = 'password';
    public const ABOUT                  = 'about';
    public const SETTINGS               = 'settings';
    public const DIFFICULTY             = 'difficulty';
    public const IS_BLOCKED             = 'is_blocked';
    public const FCM_TOKEN              = 'fcm_token';
    public const FACEBOOK               = 'facebook';
    public const INSTAGRAM              = 'instagram';
    public const TIKTOK                 = 'tiktok';
    public const YOUTUBE                = 'youtube';
    public const CREATED_AT             = 'created_at';
    public const UPDATED_AT             = 'updated_at';
    public const EMAIL_VERIFIED_AT      = 'email_verified_at';
    public const ALLOWED_CONTEST_AMOUNT = 'allowed_contest_amount';

    public const ROLES = [
        'Admin' => 1,
        'User'  => 2,
    ];
}
