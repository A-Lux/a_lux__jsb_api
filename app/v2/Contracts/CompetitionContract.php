<?php


namespace App\v2\Contracts;


interface CompetitionContract
{
    public const TABLE = 'competitions';

    public const ID           = 'id';
    public const CATEGORY_ID  = 'category_id';
    public const CATEGORY     = 'category';
    public const USER_ID      = 'user_id';
    public const USER         = 'user';
    public const IS_CONFIRMED = 'confirmed';
    public const IMAGE        = 'image';
    public const AD           = 'ad';
    public const COMPANY_ID   = 'company_id';
    public const NAME         = 'name';
    public const TYPE_ID      = 'type_id';
    public const DESCRIPTION  = 'description';
    public const SUM          = 'sum';
    public const SHARE        = 'share';
    public const URL          = 'url';
    public const MIN_WORKS    = 'min_works';
    public const START_AT     = 'start_at';
    public const FINISH_AT    = 'finish_at';
    public const EMAIL        = 'email';
    public const CREATED_AT   = 'created_at';
    public const IS_READ      = 'read';
    public const VIEWS        = 'views';
    public const WORK_AMOUNT  = 'work_amount';
    public const IS_DENIED    = 'is_denied';

    public const FILLABLE = [
        self::CATEGORY_ID,
        self::USER_ID,
        self::IS_CONFIRMED,
        self::IMAGE,
        self::AD,
        self::COMPANY_ID,
        self::NAME,
        self::TYPE_ID,
        self::DESCRIPTION,
        self::SUM,
        self::SHARE,
        self::URL,
        self::MIN_WORKS,
        self::START_AT,
        self::FINISH_AT,
        self::EMAIL,
        self::IS_READ,
        self::VIEWS,
        'main',
    ];

    public const SMALL = [
        self::ID,
        self::IMAGE,
        self::NAME,
        self::SUM,
        self::CATEGORY_ID,
        self::VIEWS,
    ];

    public const FULL = [
        self::ID,
        self::AD,
        self::NAME,
        self::SUM,
        self::URL,
        self::MIN_WORKS,
        self::TYPE_ID,
        self::CATEGORY_ID,
        self::USER_ID,
        self::MIN_WORKS,
        self::START_AT,
        self::FINISH_AT,
        self::DESCRIPTION,
        self::VIEWS,
    ];
}
