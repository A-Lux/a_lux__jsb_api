<?php


namespace App\v2\Contracts;


interface LocationAdContract
{
    public const TABLE = 'location_ads';

    public const ID             = 'uuid';
    public const LOCATION_ID    = 'location_id';
    public const TYPE           = 'type';
    public const IS_FOR_COUNTRY = 'is_for_country';
    public const CREATED_AT     = 'created_at';
    public const UPDATED_AT     = 'upddated_at';

    public const FILLABLE = [
        self::LOCATION_ID,
        self::TYPE,
        self::IS_FOR_COUNTRY,
    ];

    public const TYPES = [
      'IMAGE' => 1,
      'VIDEO' => 2,
      'TEXT' => 3
    ];
}
