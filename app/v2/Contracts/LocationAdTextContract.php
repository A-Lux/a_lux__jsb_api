<?php


namespace App\v2\Contracts;


interface LocationAdTextContract
{
    public const TABLE = 'location_ads_medias';

    public const ID         = 'uuid';
    public const AD_ID      = 'ad_id';
    public const TEXT       = 'text';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'upddated_at';

    public const FILLABLE = [
        self::AD_ID,
        self::TEXT,
    ];
}
