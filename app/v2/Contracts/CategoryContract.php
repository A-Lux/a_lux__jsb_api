<?php


namespace App\v2\Contracts;


interface CategoryContract
{
    public const popular = [
        'en' => 'Popular',
        'ru' => 'Популярные',
        'br' => 'Popular',
        'fr' => 'Popular',
        'es' => 'Popular',
    ];
    public const newConst = [
        'en' => 'New contests',
        'ru' => 'Новые конкурсы',
        'br' => 'Novos concursos',
        'fr' => 'Nouveaux concours',
        'es' => 'Nuevos concursos',
    ];

    public const ID = 'id';

    public const LIST =
        [
            [
                'id'       => 1,
                'name'     => [
                    'en' => 'Beauty',
                    'ru' => 'Красота',
                    'br' => 'Beleza',
                    'fr' => 'Beauté',
                    'es' => 'Belleza',
                ],
                'image'    => 'https://ewr1.vultrobjects.com/general/categories/July2020/IqNBfx6DXlGBczI3Yv30.png'
            ],
            [
                'id'       => 2,
                'name'     => [
                    'en' => 'Humour',
                    'ru' => 'Юмор',
                    'br' => 'Humor',
                    'fr' => 'Humour',
                    'es' => 'Humor',
                ],
                'image'    => 'https://ewr1.vultrobjects.com/general/categories/July2020/vPJA08zYIpO92GXUgN3I.png'
            ],
            [
                'id'       => 3,
                'name'     => [
                    'en' => 'Leisure',
                    'ru' => 'Отдых',
                    'br' => 'Lazer',
                    'fr' => 'Loisirs',
                    'es' => 'Ocio',
                ],
                'image'    => 'https://ewr1.vultrobjects.com/general/categories/July2020/zEkAImN1yfijUFc1yY6l.png'
            ],
            [
                'id'       => 4,
                'name'     => [
                    'en' => 'Fauna',
                    'ru' => 'Фауна',
                    'br' => 'Animais',
                    'fr' => 'Faune',
                    'es' => 'Animales',
                ],
                'image'    => 'https://ewr1.vultrobjects.com/general/categories/July2020/xZpaQhwYMtxGXuGvOLaR.png'
            ],
            [
                'id'       => 5,
                'name'     => [
                    'en' => 'Creative',
                    'ru' => 'Креатив',
                    'br' => 'Criatividade',
                    'fr' => 'Créativité',
                    'es' => 'Creatividad',
                ],
                'image'    => 'https://ewr1.vultrobjects.com/general/categories/July2020/SwlYWj7YKo7jXYN6612K.png'
            ],
            [
                'id'       => 6,
                'name'     => [
                    'en' => 'Food',
                    'ru' => 'Еда',
                    'br' => 'Comida',
                    'fr' => 'Nourriture',
                    'es' => 'Comida',
                ],
                'image'    => 'https://ewr1.vultrobjects.com/general/categories/July2020/fCV6cy3EzZuhENaCxQn0.png'
            ],
            [
                'id'       => 7,
                'name'     => [
                    'en' => 'Talents',
                    'ru' => 'Таланты',
                    'br' => 'Talentos',
                    'fr' => 'Talents',
                    'es' => 'Talentos',
                ],
                'image'    => 'https://ewr1.vultrobjects.com/general/categories/July2020/uEkBfpNveo9vaIS0FLNt.png'
            ],
            [
                'id'       => 8,
                'name'     => [
                    'en' => 'Weird',
                    'ru' => 'Странное',
                    'br' => 'Estranho',
                    'fr' => 'Bizarre',
                    'es' => 'Extraño',
                ],
                'image'    => 'https://ewr1.vultrobjects.com/general/categories/July2020/BmyeI6mE1z9Zmtec5AMP.png'
            ],
            [
                'id'       => 9,
                'name'     => [
                    'en' => 'Sports',
                    'ru' => 'Спорт',
                    'br' => 'Esportes',
                    'fr' => 'Sports',
                    'es' => 'Deporte',
                ],
                'image'    => 'https://ewr1.vultrobjects.com/general/categories/July2020/TSPLPlyX154j2gpix3fq.png'
            ],
            [
                'id'       => 10,
                'name'     => [
                    'en' => 'Other',
                    'ru' => 'Другое',
                    'br' => 'Outro',
                    'fr' => 'Autre',
                    'es' => 'Otro',
                ],
                'image'    => 'https://ewr1.vultrobjects.com/general/categories/July2020/qfPTpMALBBFipmtXl96B.png'
            ],
        ];
}
