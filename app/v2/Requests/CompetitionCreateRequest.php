<?php

namespace App\v2\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompetitionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => ['required', 'max:255'],
            'category_id' => ['required'],
            'ad'          => ['required', 'file'],
            'image'       => ['required', 'file'],
            'min_works'   => ['required', 'numeric', 'min:30'],
            'type_id'     => ['required'],
            'url'         => ['string', 'nullable'],
            'description' => ['required'],
            'start_at'    => ['required'],
            'finish_at'   => ['required'],
            'email'       => ['required', 'email'],
        ];
    }
}
