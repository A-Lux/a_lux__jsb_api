<?php


namespace App\v2\QueryFilters\Competition;


use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class SmallCards
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        $fields = [
            DB::raw(
                '(Select count(' . WorkContract::ID . ') from ' . WorkContract::TABLE . ' where ' . WorkContract::COMPETITION_ID . ' = ' . CompetitionContract::TABLE . '.' . CompetitionContract::ID . ' AND ' . WorkContract::VERIFIED . ' = 1) as works_amount'
            ),
        ];
        $fields = array_merge($fields, CompetitionContract::SMALL);
        $query->select($fields);

        return $next($query);
    }
}
