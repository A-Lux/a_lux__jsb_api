<?php


namespace App\v2\QueryFilters\Competition;


use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class CategoryID
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        if (request()->has(CompetitionContract::CATEGORY_ID)) {
            $query->where(function ($q) {
                foreach (request()->{CompetitionContract::CATEGORY_ID} as $id) {
                    $q->orWhere(CompetitionContract::CATEGORY_ID, $id);
                }
            });
            if(Auth::guard('api')->check()) {
                foreach (request()->{CompetitionContract::CATEGORY_ID} as $id) {
                    Cache::put('unread-user:'.Auth::guard('api')->user()->{UserContract::ID}.'category:'.$id, 0, 3600);
                }
            }
        }

        return $next($query);
    }
}
