<?php


namespace App\v2\QueryFilters\Competition;


use App\v2\Contracts\CompetitionContract;

class UserID
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        if (request()->has(CompetitionContract::USER_ID)) {
            $query->where(function ($q) {
                foreach (request()->{CompetitionContract::USER_ID} as $id) {
                    $q->orWhere(CompetitionContract::USER_ID, $id);
                }
            });
        }

        return $next($query);
    }
}
