<?php


namespace App\v2\QueryFilters\Competition;


use App\v2\Contracts\CompetitionContract;

class Confirmed
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        if (request()->has(CompetitionContract::IS_CONFIRMED)) {
            //TODO Remove on update 3.1.3
            $status = request()->get(CompetitionContract::IS_CONFIRMED);
            if ($status == "true") {
                $status = 1;
            }
            elseif ($status == "false") {
                $status = 0;
            }

            $query->where(CompetitionContract::IS_CONFIRMED, $status);
        } else {
            $query->where(CompetitionContract::IS_CONFIRMED, true);
        }

        return $next($query);
    }
}
