<?php


namespace App\v2\QueryFilters\Competition;


use App\v2\Contracts\CompetitionContract;
use Carbon\Carbon;

class Finished
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        if (request()->has('finished')) {
            if (request()->finished) {
                $query->where(CompetitionContract::FINISH_AT, '>', Carbon::now());
            }
            else {
                $query->where(CompetitionContract::FINISH_AT, '<', Carbon::now());
            }
        }

        return $next($query);
    }
}
