<?php


namespace App\v2\QueryFilters;


use App\v2\Contracts\CompetitionContract;

class Winners
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        if (request()->has('order_by_field') && request()->has('order_by_value')) {
            $query->orderBy(request()->order_by_field, request()->order_by_value);
        }

        return $next($query);
    }
}
