<?php


namespace App\v2\QueryFilters\Work;


use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class Likes
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        $query->orderByRaw('(Select count(likes.id) from likes where likes.work_id = works.id) DESC'); 
        return $next($query);
    }
}
