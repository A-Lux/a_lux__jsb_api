<?php


namespace App\v2\QueryFilters\Work;


use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class CompetitionID
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        if (request()->has(WorkContract::COMPETITION_ID)) {
            $query->where(WorkContract::COMPETITION_ID, request()->{WorkContract::COMPETITION_ID});
            //TODO Make smth, that's not good that admin also triggers it
            if(Auth::guard('api')->check()) {
                Cache::put('unread-user:'.Auth::guard('api')->user()->{UserContract::ID}.'competition:'.request()->{WorkContract::COMPETITION_ID}, 0, 3600);
            }
        }

        return $next($query);
    }
}
