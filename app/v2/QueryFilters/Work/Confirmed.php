<?php


namespace App\v2\QueryFilters\Work;


use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\WorkContract;

class Confirmed
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        if(request()->has(WorkContract::VERIFIED)) {
            $query->where(CompetitionContract::IS_CONFIRMED, true);
        }
        return $next($query);
    }
}
