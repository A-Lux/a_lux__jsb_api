<?php


namespace App\v2\QueryFilters\Work;


use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\WorkContract;
use Illuminate\Support\Facades\DB;

class MobileFields
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        $fields = WorkContract::MOBILE_FIELDS;
        if(request()->has(WorkContract::HIGHLIGHT)) {
            $fields[] = DB::raw('IF (id = ' . request()->get(WorkContract::HIGHLIGHT) . ', "1", "0") as ' . WorkContract::HIGHLIGHT);
        }
        $query->select(
            $fields
        );

        return $next($query);
    }
}
