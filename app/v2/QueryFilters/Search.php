<?php


namespace App\v2\QueryFilters;

class Search
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        if(request()->has('search_field') && request()->has('search_value')) {
            $query->where(request()->search_field, 'LIKE', '%' . request()->search_value . '%');
        }

        return $next($query);
    }
}
