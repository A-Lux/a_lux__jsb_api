<?php


namespace App\v2\QueryFilters;


use App\v2\Contracts\CompetitionContract;

class OrderByRandom
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        if (request()->has('random')) {
            $query->inRandomOrder();
        }

        return $next($query);
    }
}
