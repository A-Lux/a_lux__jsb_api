<?php

namespace App\v2\Mobile;

use App\Http\Controllers\Controller;
use App\v2\Contracts\UserContract;
use App\v2\Models\User;
use App\v2\Services\CompetitionService;
use App\v2\Services\UserService;
use Illuminate\Http\Request;

class RevenuecatController extends Controller
{
    protected CompetitionService $competitionService;
    protected UserService $userService;

    public function __construct(CompetitionService $competitionService, UserService $userService)
    {
        $this->competitionService = $competitionService;
        $this->userService        = $userService;
    }

    public function handle(Request $request)
    {
        $type = $request->all()['event']['product_id'];
        if ($type === 'popular_category') {
            $competitionID = $request->all()['event']['subscriber_attributes']['competition']['value'];
            $this->competitionService->addToPopular($competitionID);

            return response([], 200);
        }
        if ($type === 'visible_part_category') {
            $competitionID = $request->all()['event']['subscriber_attributes']['competition']['value'];
            $this->competitionService->update($competitionID, ['main' => true]);

            return response([], 200);
        }
        if ($type === 'add_contest_1') {
            $this->userService->update($request->all()['event']['app_user_id'], [
                UserContract::ALLOWED_CONTEST_AMOUNT => User::query()->find($request->all()['event']['app_user_id'])->{UserContract::ALLOWED_CONTEST_AMOUNT} + 1
            ]);

            return response([], 200);
        }
    }
}
