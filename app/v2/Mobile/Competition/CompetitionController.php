<?php

namespace App\v2\Mobile\Competition;

use App\Http\Controllers\Controller;
use App\v2\Models\Popular;
use App\v2\QueryFilters\Competition\CategoryID;
use App\v2\QueryFilters\Competition\Confirmed;
use App\v2\QueryFilters\Competition\Denied;
use App\v2\QueryFilters\Competition\Finished;
use App\v2\QueryFilters\Competition\SmallCards;
use App\v2\QueryFilters\Competition\UserID;
use App\v2\QueryFilters\OrderBy;
use App\v2\QueryFilters\OrderByRandom;
use App\v2\Requests\CompetitionCreateRequest;
use App\v2\Services\CompetitionService;
use Illuminate\Http\Request;
use App\v2\Models\Competition;
use Illuminate\Support\Facades\DB;

class CompetitionController extends Controller
{
    private CompetitionService $competitionService;

    public function __construct(CompetitionService $competitionService)
    {
        $this->competitionService = $competitionService;
    }

    public function main()
    {
        $result = $this->competitionService->main();

        return response(
            $result['message'], $result['code']
        );
    }

    public function types()
    {
        $result = $this->competitionService->types();

        return response(
            $result['message'], $result['code']
        );
    }

    public function create(CompetitionCreateRequest $request)
    {
        $result = $this->competitionService->create($request->validated(), $request->user()->id);

        return response(
            $result['message'], $result['code']
        );
    }

    public function universal(Request $request)
    {
        $result = $this->competitionService->paginateUniversal(
            [
                CategoryID::class,
                UserID::class,
                Denied::class,
                OrderBy::class,
                OrderByRandom::class,
                Finished::class,
                Confirmed::class,
                SmallCards::class
            ]
        );

        return response(
            $result['message'], $result['code']
        );
    }

    public function show(Request $request, $id)
    {
        $result = $this->competitionService->show($id);

        return response(
            $result['message'], $result['code']
        );
    }
}
