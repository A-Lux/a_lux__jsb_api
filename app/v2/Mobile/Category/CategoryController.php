<?php

namespace App\v2\Mobile\Category;

use App\Http\Controllers\Controller;
use App\v2\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private CategoryService $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function get()
    {
        $result = $this->categoryService->get();

        return response(
            $result['message'], $result['code']
        );
    }

    public function badges()
    {
        $result = $this->categoryService->getBadges();

        return response(
            $result['message'], $result['code']
        );
    }
}
