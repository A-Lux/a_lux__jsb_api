<?php

namespace App\v2\Mobile\Ad;

use App\Http\Controllers\Controller;
use App\v2\Services\AdService;
use Illuminate\Http\Request;

class AdController extends Controller
{
    private AdService $adService;

    public function __construct(AdService $adService)
    {
        $this->adService = $adService;
    }

    public function getByCategory($id) {
        $result = $this->adService->getByCategory($id);

        return response($result['message'], $result['code']);
    }

    public function getByIP()
    {

    }
}
