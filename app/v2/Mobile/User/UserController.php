<?php

namespace App\v2\Mobile\User;

use App\Http\Controllers\Controller;
use App\v2\Models\Popular;
use App\v2\QueryFilters\Competition\CategoryID;
use App\v2\QueryFilters\Competition\Confirmed;
use App\v2\QueryFilters\Competition\Finished;
use App\v2\QueryFilters\Competition\SmallCards;
use App\v2\QueryFilters\Competition\UserID;
use App\v2\QueryFilters\OrderBy;
use App\v2\QueryFilters\OrderByRandom;
use App\v2\Requests\CompetitionCreateRequest;
use App\v2\Services\UserService;
use Illuminate\Http\Request;
use App\v2\Models\Competition;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function universal(Request $request)
    {
        $result = $this->userService->paginateUniversal(
            [
                OrderBy::class,
                OrderByRandom::class,
            ]
        );

        return response(
            $result['message'], $result['code']
        );
    }

    public function show($id)
    {
        $result = $this->userService->show($id);
        return response(
            $result['message'], $result['code']
        );
    }
}
