<?php

namespace App\v2\Mobile\Work;

use App\Http\Controllers\Controller;
use App\Http\Requests\WorkCreateRequest;
use App\Like;
use App\v2\Models\Popular;
use App\v2\Models\Work;
use App\v2\QueryFilters\Competition\CategoryID;
use App\v2\QueryFilters\Competition\Confirmed;
use App\v2\QueryFilters\Competition\SmallCards;
use App\v2\QueryFilters\Competition\UserID;
use App\v2\QueryFilters\OrderBy;
use App\v2\QueryFilters\OrderByRandom;
use App\v2\QueryFilters\Work\CompetitionID;
use App\v2\QueryFilters\Work\Highlight;
use App\v2\QueryFilters\Work\Likes;
use App\v2\QueryFilters\Work\MobileFields;
use App\v2\Requests\CompetitionCreateRequest;
use App\v2\Services\CompetitionService;
use App\v2\Services\WorkService;
use Illuminate\Http\Request;
use App\v2\Models\Competition;
use Illuminate\Support\Facades\DB;

class WorkController extends Controller
{
    private WorkService $workService;

    public function __construct(WorkService $workService)
    {
        $this->workService = $workService;
    }

    public function create(WorkCreateRequest $request, $competition_id)
    {
        $result = $this->workService->create($request->validated(), $competition_id);

        return response(
            $result['message'], $result['code']
        );
    }

    public function universal(Request $request)
    {
        $result = $this->workService->universal(
            [
                CompetitionID::class,
                UserID::class,
                Highlight::class,
                MobileFields::class,
                Likes::class,
            ]
        );

        return response(
            $result['message'], $result['code']
        );
    }
}
