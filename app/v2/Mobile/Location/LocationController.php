<?php

namespace App\v2\Mobile\Location;

use App\Http\Controllers\Controller;
use App\v2\Services\LocationService;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    private LocationService $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    public function getCurrentLocationByIP () {
        $result = $this->locationService->getLocationByIP();

        return response($result['message'], $result['code']);
    }
}
