<?php

namespace App\v2\Models;

use App\v2\Contracts\CategoryAdContract;
use Illuminate\Database\Eloquent\Model;

class CategoryAd extends Model
{
    protected $table = CategoryAdContract::TABLE;

    protected $fillable = CategoryAdContract::FILLABLE;
}
