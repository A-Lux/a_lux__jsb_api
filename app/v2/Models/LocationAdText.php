<?php

namespace App\v2\Models;

use App\Traits\WithUUID;
use App\v2\Contracts\LocationAdTextContract;
use Illuminate\Database\Eloquent\Model;

class LocationAdText extends Model
{
    use WithUUID;

    protected $primaryKey = LocationAdTextContract::ID;

    protected $fillable = LocationAdTextContract::FILLABLE;
}
