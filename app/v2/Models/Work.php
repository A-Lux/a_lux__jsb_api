<?php

namespace App\v2\Models;

use App\v2\Contracts\WorkContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Work extends Model
{
    use SoftDeletes;

    protected $fillable = WorkContract::FILLABLE;
}
