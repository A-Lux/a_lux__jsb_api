<?php

namespace App\v2\Models;

use App\v2\Contracts\CompetitionContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Competition extends Model
{
    use SoftDeletes;

    protected $fillable = CompetitionContract::FILLABLE;
}
