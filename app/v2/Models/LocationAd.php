<?php

namespace App\v2\Models;

use App\Traits\WithUUID;
use Illuminate\Database\Eloquent\Model;
use App\v2\Contracts\LocationAdContract;

class LocationAd extends Model
{
    use WithUUID;

    protected $primaryKey = LocationAdContract::ID;

    protected $fillable = LocationAdContract::FILLABLE;
}
