<?php

namespace App\v2\Models;

use App\Traits\WithUUID;
use App\v2\Contracts\LocationAdMediaContract;
use Illuminate\Database\Eloquent\Model;

class LocationAdMedia extends Model
{
    use WithUUID;

    protected $primaryKey = LocationAdMediaContract::ID;

    protected $fillable = LocationAdMediaContract::FILLABLE;
}
