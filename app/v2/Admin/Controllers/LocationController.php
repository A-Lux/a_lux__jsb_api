<?php

namespace App\v2\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\v2\Services\LocationService;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    private LocationService $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    public function search(Request $request)
    {
        $result = $this->locationService->searchLocations($request->search ?? '');

        return response($result['message'], $result['code']);
    }
}
