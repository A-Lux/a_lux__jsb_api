<?php

namespace App\v2\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\v2\Services\CommentService;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    private CommentService $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    public function universal(Request $request)
    {
        $result = $this->commentService->universal([]);

        return response(
            $result['message'], $result['code']
        );
    }
}
