<?php

namespace App\v2\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginRequest;
use App\Http\Requests\LoginRequest;
use App\v2\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function login(AdminLoginRequest $request)
    {
        $result = $this->userService->login($request->validated());

        return response(
            $result['message'], $result['code']
        );
    }

    public function profile()
    {
        $result = $this->userService->show(1001);

        return response(
            $result['message'], $result['code']
        );
    }

    public function destroy($id) {
        $result = $this->userService->delete($id);

        return response(
            $result['message'], $result['code']
        );
    }
}
