<?php

namespace App\v2\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\v2\Services\ComplaintService;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    private ComplaintService $complaintService;

    public function __construct(ComplaintService $complaintService)
    {
        $this->complaintService = $complaintService;
    }

    public function universal(Request $request)
    {
        $result = $this->complaintService->universal([]);

        return response(
            $result['message'], $result['code']
        );
    }
//changeStatus
}
