<?php

namespace App\v2\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompetitionUpdateRequest;
use App\v2\Models\Competition;
use App\v2\QueryFilters\Competition\CategoryID;
use App\v2\QueryFilters\Competition\Confirmed;
use App\v2\QueryFilters\Competition\Finished;
use App\v2\QueryFilters\Search;
use App\v2\QueryFilters\Competition\UserID;
use App\v2\QueryFilters\OrderBy;
use App\v2\QueryFilters\OrderByRandom;
use App\v2\Services\CompetitionService;
use Illuminate\Http\Request;

class CompetitionController extends Controller
{
    private CompetitionService $competitionService;

    public function __construct(CompetitionService $competitionService)
    {
        $this->competitionService = $competitionService;
    }

    public function universal(Request $request)
    {
        $result = $this->competitionService->paginateUniversal(
            [
                Search::class,
                CategoryID::class,
                UserID::class,
                OrderBy::class,
                OrderByRandom::class,
            ]
        );

        return response(
            $result['message'], $result['code']
        );
    }

    public function show(Request $request, $id)
    {
        $result = $this->competitionService->show($id);

        return response(
            $result['message'], $result['code']
        );
    }

    public function update(CompetitionUpdateRequest $request, Competition $competition) {
        $result = $this->competitionService->update($competition->id, $request->validated());

        return response(
            $result['message'], $result['code']
        );
    }
}
