<?php

namespace App\v2\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\v2\QueryFilters\Work\Confirmed;
use App\v2\QueryFilters\OrderBy;
use App\v2\QueryFilters\Search;
use App\v2\QueryFilters\Work\CompetitionID;
use App\v2\QueryFilters\Work\UserID;
use App\v2\Services\WorkService;
use Illuminate\Http\Request;

class WorkController extends Controller
{
    private WorkService $workService;

    public function __construct(WorkService $workService)
    {
        $this->workService = $workService;
    }

    public function universal(Request $request)
    {
        $result = $this->workService->universal(
            [
                CompetitionID::class,
                UserID::class,
                Confirmed::class,
                Search::class,
                OrderBy::class
            ]
        );

        return response(
            $result['message'], $result['code']
        );
    }
}
