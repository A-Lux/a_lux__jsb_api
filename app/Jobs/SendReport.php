<?php

namespace App\Jobs;

use App\User;
use App\UserCode;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $number = rand(1000, 9999);

        while (
            UserCode::whereRaw('DATEDIFF(created_at, now()) < 1')->where('code', $number)->exists()
        ) {
            $number = rand(1000, 9999);
        }

        $code = UserCode::create([
            'user_id' => $this->user->id,
            'code' => $number
        ]);

        Mail::send('mail.auth.password.recovery', ['code' => $code->code], function ($message) {
            $message->to($this->user->email, $this->user->name);
            $message->subject('Password recovery');
            $message->priority(3);
        });
    }
}
