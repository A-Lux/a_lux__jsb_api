<?php

namespace App\Jobs;

use App\Competition;
use App\Statistic;
use App\Traits\Push;
use App\Transaction;
use App\User;
use App\v2\Contracts\CompetitionContract;
use App\v2\Contracts\LikeContract;
use App\v2\Contracts\WorkContract;
use App\v2\Repositories\CompetitionRepository;
use App\v2\Repositories\WorkRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class FinishCompetition implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Push;

    protected int $competition_id;
    protected WorkRepository $workRepository;
    protected CompetitionRepository $competitionRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $competition_id)
    {
        $this->competition_id        = $competition_id;
        $this->workRepository        = new WorkRepository();
        $this->competitionRepository = new CompetitionRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $works       = $this->workRepository
            ->get(
                [
                    [WorkContract::COMPETITION_ID, '=', $this->competition_id],
                ],
                ['like_count', 'DESC'],
                [WorkContract::ID, DB::raw(
                    '(SELECT COUNT(' . LikeContract::ID . ') from ' . LikeContract::TABLE . ' where ' . LikeContract::WORK_ID . ' in (works.id)) as like_count'
                )]
            );
        $winner      = $works
            ->first();
        $competition = $this->competitionRepository->show($this->competition_id);

        if ($works->count() >= $competition->min_works) {
            Statistic
                ::where('user_id', $winner->user_id)
                ->where('competition_id', $winner->competition_id)
                ->update(
                    [
                        'is_winner' => 1,
                        'sum'       => $competition->sum,
                    ]
                )
            ;
            Transaction::create(
                [
                    'approved' => 1,
                    'sum'      => $competition->sum,
                    'type'     => 'win',
                    'user_id'  => $winner->user_id,
                ]
            );
            $this->sendPush(
                User::find($winner->user_id)->fcm_token,
                'You Win!',
                'Your work in competition ' . $competition->name . ' has won!. ' . $competition->sum . ' points of rating were added to your account'
            );
        }
    }
}
