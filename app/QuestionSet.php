<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Storage;
use App\Traits\ImageResise;
use Illuminate\Support\Facades\Redis;

/**
 * App\QuestionSet
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $company_id
 * @property int|null $time_amount
 * @property string $image
 * @property string $sum
 * @property string $name
 * @property int|null $difficulty_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $url
 * @property int|null $share_amount
 * @property-read \App\Company|null $company
 * @property-read \App\Difficulty|null $difficulty
 * @property-read mixed $img
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $questions
 * @property-read int|null $questions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Statistic[] $statistics
 * @property-read int|null $statistics_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereDifficultyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereShareAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereTimeAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuestionSet whereUrl($value)
 * @mixin \Eloquent
 */
class QuestionSet extends Model
{
    use SoftDeletes, ImageResise;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'company_id', 'image', 'sum', 'name', 'difficulty_id', 'time_amount', 'ad', 'url'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'time', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * Get the Questions for the QuestionSet.
     */
    public function questions()
    {
        return $this->hasMany(\App\Question::class);
    }


    /**
     * Get the Statistics for the QuestionSet.
     */
    public function statistics()
    {
        return $this->hasMany(\App\Statistic::class);
    }


    /**
     * Get the Company for the QuestionSet.
     */
    public function company()
    {
        return $this->belongsTo(\App\Company::class);
    }


    /**
     * Get the Difficulty for the QuestionSet.
     */
    public function difficulty()
    {
        return $this->belongsTo(\App\Difficulty::class);
    }
}
