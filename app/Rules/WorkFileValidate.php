<?php

namespace App\Rules;

use finfo;
use getID3;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Log;

class WorkFileValidate implements Rule
{
    public $text;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  \Illuminate\Http\UploadedFile  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
//        $finfo = new finfo(FILEINFO_MIME);

        $type = $value->getMimeType(); //(explode(' ', $finfo->file())[0]);

        if($type == 'image/png' || $type == 'image/jpeg'){
            return true;
        }

        if($type == 'video/quicktime' || $type == 'video/mp4' || $type = 'application/octet-stream'){
            $getID3 = new getID3;

            $video_file = $getID3->analyze($value);

            // Get the duration in seconds, e.g.: 277 (seconds)
            $duration_seconds = $video_file['playtime_seconds'];
            if($duration_seconds <= 11){
                return true;
            }
            $this->text = 'Видео слишком длинное';
            return false;
        }

        $this->text = 'Формат файла не поддерживается';
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->text;
    }
}
