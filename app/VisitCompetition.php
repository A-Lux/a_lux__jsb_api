<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitCompetition extends Model
{
    protected $fillable = [
        'user_id',
        'competition_id',
        'visited_at',
    ];
}
