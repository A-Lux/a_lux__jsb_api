<?php

use Illuminate\Support\Facades\Route;
use App\v2\Admin\Controllers\CompetitionController;
use App\v2\Admin\Controllers\WorkController;
use App\v2\Admin\Controllers\UserController;
use App\v2\Admin\Controllers\CategorieController;
use App\v2\Admin\Controllers\AdController;
use App\v2\Admin\Controllers\LocationController;
use App\v2\Admin\Controllers\CommentController;
use App\v2\Admin\Controllers\ComplaintController;
use App\v2\Admin\Controllers\TransactionController;

Route::post('/login', [UserController::class, 'login']);

Route::group(
    ['middleware' => 'auth:api'], function () {

    Route::get('/profile', [UserController::class, 'profile']);

    Route::get('/competitions', [CompetitionController::class, 'universal']);
    Route::get('/competitions/{id}', [CompetitionController::class, 'show'])
         ->where('id', '[0-9]+')
    ;
    Route::post('/competitions/{competition}', [CompetitionController::class, 'update'])
         ->where('id', '[0-9]+')
    ;

    Route::get('/works', [WorkController::class, 'universal']);

    Route::get('/comments', [CommentController::class, 'universal']);
    Route::get('/comments/{id}', [CommentController::class, 'show'])
         ->where('id', '[0-9]+')
    ;

    Route::get('/complaints', [ComplaintController::class, 'universal']);
    Route::post('/complaints/{id}', [ComplaintController::class, 'changeStatus'])
         ->where('id', '[0-9]+')
    ;

    Route::get('/feedback', []);

//
//Route::get('/users', '');
//
//Route::get('/categories', '');
//
//Route::get('/ads', '');
//Route::get('/ads/{id}', '')
//     ->where('id', '');
//
//Route::get('/locations', '');
//Route::get('/location-ads', '');
//

//;
//

//
//Route::get('/feedback/{id}', '')
//     ->where('id', '')
//;
//
}
);
