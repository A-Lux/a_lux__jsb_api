<?php

use App\Http\Controllers\Api\LikeAPIController;
use App\v2\Mobile\Ad\AdController;
use App\v2\Mobile\Category\CategoryController;
use App\v2\Mobile\Competition\CompetitionController;
use App\v2\Mobile\Location\LocationController;
use App\v2\Mobile\RevenuecatController;
use App\v2\Mobile\User\UserController;
use App\v2\Mobile\Work\WorkController;
use Illuminate\Support\Facades\Route;

Route::get('/main', [CompetitionController::class, 'main']);
Route::get('/competitions', [CompetitionController::class, 'universal']);
Route::get('/competitions/types', [CompetitionController::class, 'types']);
Route::get('/competitions/{id}', [CompetitionController::class, 'show'])
     ->where('id', '[0-9]+')
;

Route::get('/works', [WorkController::class, 'universal']);

Route::get('/users', [UserController::class, 'universal']);

Route::get('/likes', [LikeAPIController::class, 'index']);

Route::get('/categories', [CategoryController::class, 'get']);
Route::get('/categories/badges', [CategoryController::class, 'badges']);

Route::get('/ads/{id}', [AdController::class, 'getByCategory'])
    ->where('id', '[0-9]+');

//TODO Check if admin is needed
Route::get('/locations', [\App\v2\Admin\Controllers\LocationController::class, 'search']);
Route::get('/iploc', [LocationController::class, 'getCurrentLocationByIP']);

Route::group(
    ['middleware' => 'auth:api'], function () {
    Route::post('/competitions', [CompetitionController::class, 'create']);
    Route::post('/works/{competition_id}', [WorkController::class, 'create']);
    Route::delete('/users/{id}', [\App\v2\Admin\Controllers\UserController::class, 'destroy']);
}
);

Route::post('webhook/cloudconvert', '\CloudConvert\Laravel\CloudConvertWebhooksController');
Route::post('revenuecat', [RevenuecatController::class, 'handle']);


