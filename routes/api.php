<?php

use App\Http\Controllers\Api\WorkAPIController;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
* Snippet for a quick route reference
*/

Route::group(['prefix' => 'admin', 'middleware' => 'web'], function () {
    Voyager::routes();
    Route::get('/lang/{key}', 'LanguageController@editLang');
});

Route::get('/', function (Router $router) {
    return print_r(collect($router->getRoutes()->getRoutesByMethod()["GET"])->map(function ($value, $key) {
        return url($key);
    })->values(), true);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'Auth\LoginController@login')->name('login');
    Route::post('/register', 'Auth\RegisterController@register');
    Route::get('/verify', 'Auth\RegisterController@verify');
    Route::get('/forgot', 'Auth\ForgotPasswordController@sendCode');
    Route::get('/code', 'Auth\ForgotPasswordController@checkCode');
    Route::get('/check', 'Auth\LoginController@check');
});

Route::get('/random/user', 'Api\UserAPIController@random');

Route::get('/keys/{key}', 'LanguageController@get');
Route::get('/keys', 'LanguageController@getLangs');

Route::group(['prefix' => 'share'], function () {

    Route::get('/questions', function (Request $request) { // TODO Fix to /Share
        return view('share.questions');
    });

    Route::get('/rating', function (Request $request) {
        return view('share.rating');
    });

    Route::get('/main', function (Request $request) {
        return view('share.main');
    });

    Route::get('/category/{category}', function (Request $request, $id) {
        return view('share.category', [
            'category' => \App\Category::find($id)
        ]);
    });

    Route::get('/competition/{competition}', function (Request $request, $id) {
        return view('share.competition', [
            'competition' => \App\Competition::find($id)
        ]);
    });

    Route::get('/competition/{competition}/works', function (Request $request, $id) {
        return view('share.competitionWorks', [
            'competition' => \App\Competition::find($id)
        ]);
    });

    Route::get('/work/{work}', function (Request $request, $id) {
        return view('share.work', [
            'competition' => \App\Competition::find($id)
        ]);
    });


    Route::get('/questions/{set}', function (Request $request, $id) {
        return view('share.questionSet', [
            'set' => \App\QuestionSet::find($id)
        ]);
    });
});

Route::get('/competition/{competition}', function (Request $request, $id) {
    return view('share.competition', [
        'competition' => \App\Competition::find($id)
    ]);
});

Route::get('/main', function (Request $request, $id) {
    return view('share.main');
});

Route::get('/category', function (Request $request, $id) {
    return view('share.questionSet', [
        'set' => \App\QuestionSet::find($id)
    ]);
});

Route::get('/category', function (Request $request, $id) {
    return view('share.questionSet', [
        'set' => \App\QuestionSet::find($id)
    ]);
});

Route::group(['prefix' => 'users'], function () {
    Route::post('/language/add', 'LanguageController@add');
    Route::get('/keys/{key}', 'LanguageController@get');
    Route::get('/keys', 'LanguageController@getLangs');
    Route::get('/get/countries', 'Api\UserAPIController@getCountries');
    Route::get('/get/cities/{id}', 'Api\UserAPIController@getCities');
    Route::get('/ofert', 'OfertController@site');
    Route::get('/terms', 'AgreementController@site');
    Route::get('/about', 'AboutUsController@site');
    Route::get('/rules', 'HelpController@site');
    Route::post('/works/{work}/add/likes', 'Api\WorkAPIController@likeBulk');
    Route::get('/about', 'AboutUsController@site');

    Route::get('/guest', 'Api\UserAPIController@guest');
    Route::post('/works/{work}/add/likes', 'Api\WorkAPIController@likeBulk');
    Route::get('/main/list', 'Api\CategoryAPIController@main');
    Route::get('/categories', 'Api\CategoryAPIController@index');
    Route::get('/ads/list', 'Api\AdAPIController@index');
    Route::get('/category/{category}/list', 'Api\CategoryAPIController@show');
    Route::get('/category/{category}/list/random', 'Api\CategoryAPIController@showRandom');
    Route::get('/competition/{id}', 'Api\CompetitionAPIController@show');
    Route::get('/competition/{id}/works', 'Api\WorkAPIController@index');
    Route::get('/works/{id}/comments', 'Api\CommentAPIController@show');

    Route::post('/question/start/{id}', 'Api\QuestionSetAPIController@start');
    Route::get('/questions', 'Api\QuestionSetAPIController@index');
    Route::get('/questions/{questionSet}', 'Api\QuestionSetAPIController@show');
    Route::get('/rating', 'Api\StatisticAPIController@rating');

    Route::get('/today/list', 'Api\StatisticAPIController@today');
    Route::post('/works/{work}/share', 'Api\WorkAPIController@share');
    Route::get('/work/{work}', 'Api\WorkAPIController@show');
    Route::get('/report/types', 'Api\ReportTypeAPIController@index');
    Route::get('/faqs', 'AboutUsController@faqs');
    Route::post('/feedback', 'FeedbackController@store');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/competitions/create', 'Api\CompetitionAPIController@create');
        Route::get('/competitions/types', 'Api\CompetitionAPIController@types');
        Route::post('/logout', 'Auth\LoginController@logout');
        Route::get('/user/info', 'Api\UserAPIController@show');
        Route::post('/update/token', 'Api\UserAPIController@updateFirebaseToken');
        Route::post('/profile/edit', 'Api\UserAPIController@update');
        Route::resource('categories', 'Api\CategoryAPIController', [
            'only' => ['show', 'store', 'update', 'destroy']
        ]);
        Route::get('/blacklist', 'Api\UserAPIController@blacklist');
        Route::get('/add/blacklist', 'Api\UserAPIController@addToBlackList');
        Route::delete('/blacklist/{user}', 'Api\UserAPIController@deleteFromBlackList');
        Route::post('/favorites', function (Request $request) {
            return response(200);
        });

        Route::post('/works/{id}/comments', 'Api\CommentAPIController@store');
        Route::delete('/works/{work}', 'Api\WorkAPIController@destroy');
        Route::put('/works/{work}', 'Api\WorkAPIController@restore');

        Route::post('/competition/{competition}/compete', 'Api\WorkAPIController@store');
        Route::post('/works/{work}/report', 'Api\WorkAPIController@report');
        Route::post('/works/{work}/like', [WorkAPIController::class, 'like']);
        Route::post('/competitions/{competition}/share', 'Api\CompetitionAPIController@share');

        Route::post('/questions/finish/{questionSet}', 'Api\QuestionSetAPIController@finish');
        Route::post('/questions/site/finish/{questionSet}', 'Api\QuestionSetAPIController@finishSite');
        Route::post('/questions/{question}/share/yes', 'Api\QuestionSetAPIController@shareYes');
        Route::post('/questions/{question}/share/no', 'Api\QuestionSetAPIController@shareNo');
        Route::get('/statistics', 'Api\UserAPIController@statistics');
        Route::post('/update/token', 'Api\UserAPIController@updateFirebaseToken');
        Route::post('/pay', 'Api\UserAPIController@pay');
        Route::get('/currency', 'Api\UserAPIController@currency');

        Route::get('/user/details', 'Api\UserAPIController@details');
        Route::get('/user/rating', 'Api\UserAPIController@getRating');
        Route::get('/user/history', 'Api\UserAPIController@history');
        Route::get('/payment-systems', 'PaymentSystemController@index');
    });
    Route::get('/user/{user}/info', 'Api\UserAPIController@show');

    Route::get('/user/{user}/history', 'Api\UserAPIController@history');
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/emails', 'UserController@emails');
    Route::post('/mass/questions', 'Api\QuestionSetAPIController@import');
    Route::post('/mass/questions/delete', 'Api\QuestionSetAPIController@massDelete');
    Route::get('/overview/create', 'OverviewController@store');
    Route::get('/test/file/view', function () {
        return view('home');
    });
    Route::post('test/file', function (Request $request) {
        dd(Storage::put('/test', $request->file('test')));
    });
    Route::post('/premoderation/toggle', 'OverviewController@index');
    Route::get('/ban', 'Api\UserAPIController@ban');
    Route::get('/competition/{id}/approve', 'Api\CompetitionAPIController@approve');
    Route::get('/competition/{id}/set/finish_date', 'Api\CompetitionAPIController@setFinishDate');
    Route::get('/work/{work}/approve', 'Api\WorkAPIController@approve');
    Route::get('/transaction/{id}/approve', 'Admin\TransactionController@approve');
    Route::post('works/{work}/winner', 'Api\WorkAPIController@win');
    Route::post('winner/test', 'Api\WorkAPIController@winTest');
});
