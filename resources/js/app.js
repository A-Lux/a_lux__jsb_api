/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('platform');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data() {
        return {
            profileInfo: {
                name: "",
                email: "",
                password: "",
                password_confirm: ""
            },
            config: {
                headers: {
                    Authorization: 'Bearer ' + document.getElementById('token').value,
                }
            }
        }
    },
    methods: {
        send(e) {
            e.preventDefault();
            axios.post('http://localhost:8000/api/users/profile/edit', this.profileInfo, this.config)
        }
    },
    created() {
        // alert(platform.os.family);
        // if (platform.os.family == 'Android') {
        //     window.location.replace("intent://comment?access_token=" + document.getElementById('token').value + "#Intent;scheme=myapp;package=com.jsbproject;end");
        // }
        if (platform.os.family == 'iOS') {
            window.location.replace("myapp://comment?access_token=" + document.getElementById('token').value);
        }
        else {
            window.location.replace("intent://comment?access_token=" + document.getElementById('token').value + "#Intent;scheme=myapp;package=com.jsbproject;end");
        }
    }
});
