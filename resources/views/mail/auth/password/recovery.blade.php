<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
 <head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta name="x-apple-disable-message-reformatting">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="telephone=no" name="format-detection">
  <title>{{ setting('title') }}</title>
 </head>
 <body>
            {!! setting('codemail.top') !!}
            <p class="MsoNoSpacing" style="text-align: center;"><span lang="EN-US" style="mso-ansi-language: EN-US;">{{ $code }}</span></p>
            {!! setting('codemail.footer') !!}
            {!! setting('codemail.company') !!}
</html>
