@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" @submit="send" id="form" action="">
                        <input id="token" hidden value="{{ $token }}" />
                        <input name="name" v-model="profileInfo.name" type="text" value="" />
                        <input disabled type="email" value="{{ $email }}" />
                        <input name="password" v-model="profileInfo.password" type="password" value="" />
                        <input name="password_confirm" v-model="profileInfo.password_confirm" type="password" value="" />
                        <button type="submit">Отправить</button>
                    </form>
                    <a href="myapp://comment?access_token={{ $token }}" class="btn">Открыть в iOs</a>
                    <a href="intent://comment?access_token={{ $token }}#Intent;scheme=myapp;package=com.jsbproject;end" class="btn">Android</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
