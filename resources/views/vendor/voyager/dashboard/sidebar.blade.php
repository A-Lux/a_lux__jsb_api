@php
$menu = menu('admin', '_json');
foreach ($menu as $item) {
    if(Str::contains($item->route, 'work')) {
        $item->title = $item->title . ' (' . \App\Work::countRead() . ')';
    }
    if($item->children->count() > 0) {
        $item->children->map(function ($child) {
            if(Str::contains($child->route, 'work')) {
                $child->title = $child->title . ' (' . \App\Work::countRead() . ')';
            }
        });
    }
    if(Str::contains($item->route, 'competitions')) {
        $item->title = $item->title . ' (' . \App\Competition::countRead() . ')';
    }
    if($item->children->count() > 0) {
        $item->children->map(function ($child) {
            if(Str::contains($child->route, 'competitions')) {
                $child->title = $child->title . ' (' . \App\Competition::countRead() . ')';
            }
        });
    }
    if(Str::contains($item->route, 'reports')) {
        $item->title = $item->title . ' (' . \App\Report::countRead() . ')';
    }
    if($item->children->count() > 0) {
        $item->children->map(function ($child) {
            if(Str::contains($child->route, 'reports')) {
                $child->title = $child->title . ' (' . \App\Report::countRead() . ')';
            }
        });
    }
    if(Str::contains($item->route, 'transactions')) {
        $item->title = $item->title . ' (' . \App\Transaction::countRead() . ')';
    }
    if(Str::contains($item->route, 'requests')) {
        $item->title = $item->title . ' (' . \App\Feedback::countRead() . ')';
    }
    if($item->children->count() > 0) {
        $item->children->map(function ($child) {
            if(Str::contains($child->route, 'requests')) {
                $child->title = $child->title . ' (' . \App\Feedback::countRead() . ')';
            }
        });
    }
    if(Str::contains($item->route, 'comments')) {
        $item->title = $item->title . ' (' . \App\Comment::countRead() . ')';
    }
    if($item->children->count() > 0) {
        $item->children->map(function ($child) {
            if(Str::contains($child->route, 'comments')) {
                $child->title = $child->title . ' (' . \App\Comment::countRead() . ')';
            }
        });
    }
}
@endphp

<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('voyager.dashboard') }}">
                    <div class="logo-icon-container">
                        <?php $admin_logo_img = Voyager::setting('admin.icon_image', ''); ?>
                        @if($admin_logo_img == '')
                            <img src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
                        @else
                            <img src="{{ Voyager::image($admin_logo_img) }}" alt="Logo Icon">
                        @endif
                    </div>
                    <div class="title">{{Voyager::setting('admin.title', 'VOYAGER')}}</div>
                </a>
            </div><!-- .navbar-header -->

            <div class="panel widget center bgimage"
                 style="background-image:url({{ Voyager::image( Voyager::setting('admin.bg_image'), voyager_asset('images/bg.jpg') ) }}); background-size: cover; background-position: 0px;">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <img src="{{ $user_avatar }}" class="avatar" alt="{{ Auth::user()->name }} avatar">
                    <h4>{{ ucwords(Auth::user()->name) }}</h4>
                    <p>{{ Auth::user()->email }}</p>

                    <a href="{{ route('voyager.profile') }}" class="btn btn-primary">{{ __('voyager::generic.profile') }}</a>
                    <div style="clear:both"></div>
                </div>
            </div>

        </div>
        <div id="adminmenu">
            <admin-menu :items="{{ $menu }}"></admin-menu>
        </div>
    </nav>
</div>
