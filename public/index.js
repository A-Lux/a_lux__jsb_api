export const icHome = require("./home.png");
export const icChat = require("./icChat.png");
export const icStatic = require("./icStatic.png");

export const icUser = require("./icUser.png");
export const icFAQ = require("./icFAQ.png");
export const icShare = require("./icShare.png");

export const label = require("./label.png");

export const icArt = require("./icArt.png");
export const icBall = require("./icBall.png");
export const icBeauty = require("./icBeauty.png");
export const icDesign = require("./icDesign.png");
export const icFoot = require("./icFoot.png");
export const icImage = require("./icImage.png");
export const icMusic = require("./icMusic.png");
export const icOther = require("./icOther.png");
export const icStory = require("./icStory.png");
export const icVideo = require("./icVideo.png");
export const icFire = require("./icFire.png");
export const icBack = require("./icBack.png");
export const icLike = require("./icLike.png");
export const userCount = require("./userCount.png");
export const icComment = require("./icComment.png");
export const icSettings = require("./icSettings.png");
export const icMore = require("./icMore.png");
export const icAdd = require("./icAdd.png");

export const icMan = require("./icMan.jpg");

export const icNotes = require("./icNotes.png");
export const icClock = require("./icClock.png");

export const icWin = require("./icWin.png");
export const icLose = require("./icLose.png");
export const icQuestion = require("./icQuestion.png");
export const icDown = require("./icDown.png");

export const icVk = require("./icVk.png");
export const icGoogle = require("./icGoogle.png");
export const icFacebook = require("./icFacebook.png");
export const icRefresh = require("./icRefresh.png");

export const icDownload = require("./icDownload.png");
export const icLikeFull = require("./icLikeFull.png");

export const icPlay = require("./icPlay.png");
export const icPause = require("./icPause.png");

export const icSend = require("./icSend.png");
export const icAddImage = require("./icAddImage.png");
