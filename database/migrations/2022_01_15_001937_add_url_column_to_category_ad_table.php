<?php

use App\v2\Contracts\CategoryAdContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUrlColumnToCategoryAdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn(CategoryAdContract::TABLE, CategoryAdContract::LINK)) {
            Schema::table(CategoryAdContract::TABLE, function (Blueprint $table) {
                $table->string(CategoryAdContract::LINK)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_ad', function (Blueprint $table) {
            //
        });
    }
}
