<?php

use App\v2\Contracts\CommentContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CommentContract::TABLE, function (Blueprint $table) {
            $table->bigIncrements(CommentContract::ID);
            $table->softDeletes();
            $table->unsignedBigInteger(CommentContract::USER_ID)->nullable()->index('comments_user_id_foreign');
            $table->unsignedBigInteger(CommentContract::WORK_ID)->nullable()->index('comments_work_id_foreign');
            $table->text(CommentContract::TEXT);
            $table->string(CommentContract::VERIFIED);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
