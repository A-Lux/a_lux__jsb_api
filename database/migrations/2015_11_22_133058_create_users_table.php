<?php

use App\v2\Contracts\UserContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->softDeletes();
            $table->string('name')->nullable();
            $table->string('email')->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('avatar')->default('users/default.jpeg');
            $table->text('about')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('city_name')->nullable();
            $table->string('country_name')->nullable();
            $table->string('password')->nullable();
            $table->string('remember_token')->nullable();
            $table->integer('difficulty')->nullable();
            $table->timestamps();
            $table->text('fcm_token')->nullable();
            $table->tinyInteger('is_blocked')->default(0);
            $table->string('comment')->nullable();
            $table->string(UserContract::FACEBOOK)->nullable();
            $table->string(UserContract::INSTAGRAM)->nullable();
            $table->string(UserContract::TIKTOK)->nullable();
            $table->string(UserContract::YOUTUBE)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
