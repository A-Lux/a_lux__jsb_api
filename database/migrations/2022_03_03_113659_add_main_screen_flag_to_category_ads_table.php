<?php

use App\v2\Contracts\CategoryAdContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMainScreenFlagToCategoryAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(CategoryAdContract::TABLE, function (Blueprint $table) {
            $table->boolean(CategoryAdContract::IS_ON_MAIN_SCREEN)->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_ads', function (Blueprint $table) {
            //
        });
    }
}
