<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationAdsTable extends Migration
{ //TODO Change to contracts
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_ads', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->unsignedBigInteger('location_id');
            $table->foreign('location_id')->references('id')->on('locations');;
            $table->integer('type')->default(2);
            $table->boolean('is_for_country')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_ads');
    }
}
