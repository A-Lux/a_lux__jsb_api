<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeUrlNotRequiredInCategoryAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(\App\v2\Contracts\CategoryAdContract::TABLE, function (Blueprint $table) {
            $table->string(\App\v2\Contracts\CategoryAdContract::LINK)->change()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_ads', function (Blueprint $table) {
            //
        });
    }
}
