<?php

use App\v2\Contracts\LocationContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements(LocationContract::ID);
            $table->string(LocationContract::GEONAME_ID);
            $table->string(LocationContract::LOCALE_CODE);
            $table->string(LocationContract::CONTINENT_CODE);
            $table->string(LocationContract::CONTINENT_NAME);
            $table->string(LocationContract::COUNTRY_ISO_CODE);
            $table->string(LocationContract::COUNTRY_NAME);
            $table->string(LocationContract::SUBDIVISION_1_ISO_CODE);
            $table->string(LocationContract::SUBDIVISION_1_NAME);
            $table->string(LocationContract::SUBDIVISION_2_ISO_CODE);
            $table->string(LocationContract::SUBDIVISION_2_NAME);
            $table->string(LocationContract::CITY_NAME);
            $table->string(LocationContract::METRO_CODE);
            $table->string(LocationContract::TIME_ZONE);
            $table->string(LocationContract::IS_IN_EUROPEAN_UNION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}

