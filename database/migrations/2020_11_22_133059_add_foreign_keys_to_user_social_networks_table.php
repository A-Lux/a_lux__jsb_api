<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToUserSocialNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_social_networks', function (Blueprint $table) {
            $table->foreign('social_network_id', 'user_social_network_social_network_id_foreign')->references('id')->on('social_networks')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('user_id', 'user_social_network_user_id_foreign')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_social_networks', function (Blueprint $table) {
            $table->dropForeign('user_social_network_social_network_id_foreign');
            $table->dropForeign('user_social_network_user_id_foreign');
        });
    }
}
