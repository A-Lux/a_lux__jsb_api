<?php

use App\v2\Contracts\CategoryAdContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryAdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            CategoryAdContract::TABLE, function (Blueprint $table) {
            $table->bigIncrements(CategoryAdContract::ID);
            $table->string(CategoryAdContract::IMAGE);
            $table->unsignedBigInteger(CategoryAdContract::CATEGORY_ID);
            $table->string(CategoryAdContract::LINK);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_ad');
    }
}
