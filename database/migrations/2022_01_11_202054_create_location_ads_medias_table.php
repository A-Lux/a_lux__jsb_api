<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationAdsMediasTable extends Migration
{ // TODO Change to contracts
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_ad_media', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('ad_id');
            $table->foreign('ad_id')->references('uuid')->on('location_ads');;
            $table->string('file_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_ads_medias');
    }
}
