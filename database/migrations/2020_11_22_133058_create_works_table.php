<?php

use App\v2\Contracts\WorkContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(WorkContract::TABLE, function (Blueprint $table) {
            $table->bigIncrements(WorkContract::ID);
            $table->unsignedBigInteger(WorkContract::USER_ID)->nullable()->index('works_user_id_foreign');
            $table->unsignedBigInteger(WorkContract::COMPETITION_ID)->index('works_competition_id_foreign');
            $table->longText(WorkContract::DESCRIPTION);
            $table->string(WorkContract::DATA);
            $table->integer(WorkContract::SHARE_AMOUNT)->default(0);
            $table->string('thumbnail')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->boolean(WorkContract::VERIFIED)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
