<?php

use App\v2\Contracts\TransactionContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TransactionContract::TABLE, function (Blueprint $table) {
            $table->integer('id', true);
            $table->bigInteger('user_id');
            $table->integer('sum');
            $table->string('type');
            $table->tinyInteger('approved')->nullable()->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->string('payment_type', 45)->nullable();
            $table->string('number', 45)->nullable();
            $table->string('email')->nullable();
            $table->string('phone', 45)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
