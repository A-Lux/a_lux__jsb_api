<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_sets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->softDeletes();
            $table->unsignedBigInteger('company_id')->nullable()->index('question_sets_company_id_foreign');
            $table->integer('time_amount')->nullable();
            $table->string('image');
            $table->string('ad')->nullable();
            $table->string('url')->nullable();
            $table->string('sum');
            $table->string('name');
            $table->unsignedBigInteger('difficulty_id')->nullable()->index('question_sets_difficulty_id_foreign');
            $table->timestamps();
            $table->integer('share')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_sets');
    }
}
