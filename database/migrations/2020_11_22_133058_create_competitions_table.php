<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->softDeletes();
            $table->unsignedBigInteger('company_id')->nullable()->index('competitions_company_id_foreign');
            $table->unsignedBigInteger('category_id');
            $table->string('name');
            $table->string('image');
            $table->timestamp('start_at')->nullable();
            $table->timestamp('finish_at')->nullable();
            $table->unsignedBigInteger('type_id');
            $table->text('description');
            $table->string('sum')->default('0');
            $table->timestamps();
            $table->string('url')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->string('ad')->nullable();
            $table->integer('share')->nullable()->default(0);
            $table->integer('min_works')->default(30);
            $table->tinyInteger('confirmed')->default(0);
            $table->bigInteger('user_id')->default(1);
            $table->integer('checked')->nullable();
            $table->string('email', 45)->nullable();
            $table->boolean('read')->default(0);
            $table->bigInteger('views')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competitions');
    }
}
