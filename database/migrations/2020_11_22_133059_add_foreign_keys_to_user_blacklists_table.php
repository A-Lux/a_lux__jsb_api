<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToUserBlacklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_blacklists', function (Blueprint $table) {
            $table->foreign('blacklist_id', 'user_blacklist_blacklist_id_foreign')->references('id')->on('blacklists')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('user_id', 'user_blacklist_user_id_foreign')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_blacklists', function (Blueprint $table) {
            $table->dropForeign('user_blacklist_blacklist_id_foreign');
            $table->dropForeign('user_blacklist_user_id_foreign');
        });
    }
}
