<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOverviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overviews', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('users')->nullable();
            $table->bigInteger('new_users')->nullable();
            $table->bigInteger('total_question_complete')->nullable();
            $table->bigInteger('today_question_complete')->nullable();
            $table->bigInteger('total_question_winner')->nullable();
            $table->bigInteger('total_works')->nullable();
            $table->bigInteger('total_likes')->nullable();
            $table->bigInteger('total_share');
            $table->bigInteger('total_comments')->nullable();
            $table->timestamps();
            $table->bigInteger('today_works')->nullable();
            $table->bigInteger('total_competition')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overviews');
    }
}
