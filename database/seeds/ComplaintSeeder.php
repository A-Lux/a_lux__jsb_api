<?php

use AlecRabbit\Spinner\SnakeSpinner;
use App\v2\Contracts\ComplaintContract;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ComplaintSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $competitions = collect([]);
        $spinner      = new SnakeSpinner();

        echo "Creating competitions to insert\n";
        $spinner->begin();

        for ($i = 0; $i < 10000; $i++) {
            $competitions->push(
                [
                    ComplaintContract::USER_ID => random_int(1, 999),
                    ComplaintContract::TEXT    => $faker->sentence(),
                    ComplaintContract::WORK_ID => random_int(1, 999),
                    ComplaintContract::REPORT_TYPE_ID => array_rand(ComplaintContract::REPORT_TYPES)
                ]
            );
            $spinner->spin();
        }

        $spinner->end('Data created');

        echo "\nInserting\n";

        $chunks = $competitions->chunk(1000);
        $this
            ->command
            ->getOutput()
            ->progressStart($competitions->count())
        ;
        foreach ($chunks as $chunk) {
            \App\Report
                ::query()
                ->insert($chunk->toArray())
            ;
            $this
                ->command
                ->getOutput()
                ->progressAdvance(1000)
            ;
        }

        $this->command
            ->getOutput()
            ->progressFinish()
        ;
    }
}
