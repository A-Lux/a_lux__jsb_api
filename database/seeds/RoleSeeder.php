<?php

use App\v2\Contracts\RoleContract;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                RoleContract::TYPE => 'superadmin',
                RoleContract::NAME => 'Super Admin',
            ],
            [
                RoleContract::TYPE => 'user',
                RoleContract::NAME => 'User',
            ],
            [
                RoleContract::TYPE => 'admin',
                RoleContract::NAME => 'Admin',
            ],
            [
                RoleContract::TYPE => 'manager',
                RoleContract::NAME => 'Manager',
            ],
        ];

        \TCG\Voyager\Models\Role
            ::query()
            ->insert($roles)
        ;

    }
}
