<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = '
[
{"id":"4","key":"site.google_analytics_tracking_id","display_name":"Google Analytics Tracking ID","value":null,"details":"","type":"text","order":"4","group":"Site"},
{"id":"5","key":"admin.bg_image","display_name":"Admin Background Image","value":"","details":"","type":"image","order":"5","group":"Admin"},
{"id":"6","key":"admin.title","display_name":"Admin Title","value":"iPrize","details":"","type":"text","order":"1","group":"Admin"},
{"id":"7","key":"admin.description","display_name":"Admin Description","value":"GLOBAL (London)","details":"","type":"text","order":"2","group":"Admin"},
{"id":"8","key":"admin.loader","display_name":"Admin Loader","value":"","details":"","type":"image","order":"3","group":"Admin"},
{"id":"9","key":"admin.icon_image","display_name":"Admin Icon Image","value":"","details":"","type":"image","order":"4","group":"Admin"},
{"id":"10","key":"admin.google_analytics_client_id","display_name":"Google Analytics Client ID (used for admin dashboard)","value":null,"details":"","type":"text","order":"1","group":"Admin"},
{"id":"13","key":"admin.premoderation","display_name":"Premoderation","value":"0","details":null,"type":"checkbox","order":"6","group":"Admin"},
{"id":"14","key":"admin.rate","display_name":"Rate. 1000 Points is equal to:","value":"5","details":null,"type":"text","order":"7","group":"Admin"},
{"id":"15","key":"admin.currency","display_name":"Currency","value":"US$","details":null,"type":"text","order":"8","group":"Admin"},
{"id":"18","key":"codemail.title","display_name":"Title","value":"Welcome to iPrize!","details":null,"type":"text","order":"9","group":"Codemail"},
{"id":"19","key":"codemail.top","display_name":"Top text","value":"<p class=\"msonospacingmailrucssattributepostfix\" style=\"text-align: center; background: white;\" align=\"center\"><span lang=\"EN-US\" style=\"font-size: 11.5pt; font-family: \'Arial\',\'sans - serif\'; color: #333333; mso-ansi-language: EN-US;\">Welcome to iPrize!<\/span><\/p>\r\n<p class=\"msonospacingmailrucssattributepostfix\" style=\"text-align: center; background: white;\" align=\"center\"><span lang=\"EN-US\" style=\"font-size: 11.5pt; font-family: \'Arial\',\'sans - serif\'; color: #333333; mso-ansi-language: EN-US;\">You are receiving this email as this address was used to register on the iPrize app (or website).<\/span><\/p>\r\n<p class=\"msonospacingmailrucssattributepostfix\" style=\"text-align: center; background: white;\" align=\"center\"><span lang=\"EN-US\" style=\"font-size: 11.5pt; font-family: \'Arial\',\'sans - serif\'; color: #333333; mso-ansi-language: EN-US;\">If you did not register in a specified app (or on the website), simply ignore and delete this email.<\/span><\/p>\r\n<p class=\"msonospacingmailrucssattributepostfix\" style=\"text-align: center; background: white;\" align=\"center\"><span lang=\"EN-US\" style=\"font-size: 11.5pt; font-family: \'Arial\',\'sans - serif\'; color: #333333; mso-ansi-language: EN-US;\">To continue registering with iPrize, enter the unique verification code in the text box on page.<\/span><\/p>","details":null,"type":"rich_text_box","order":"10","group":"Codemail"},
{"id":"20","key":"codemail.footer","display_name":"Footer text","value":"<p style=\"text-align: center; background: white;\" align=\"center\"><span lang=\"EN-US\" style=\"font-size: 11.5pt; font-family: \'Arial\',\'sans - serif\'; color: #333333; mso-ansi-language: EN-US;\">This is an automatic letter. If you have any questions, you can get help at&nbsp;<\/span><a href=\"http:\/\/www.iprize.io\/\" target=\"_blank\" rel=\"noopener\"><span lang=\"EN-US\" style=\"font-size: 11.5pt; font-family: \'Arial\',\'sans - serif\'; color: #005bd1; mso-ansi-language: EN-US;\">www.iprize.io<\/span><\/a><\/p>\r\n<p style=\"text-align: center; background: white;\" align=\"center\"><span style=\"font-size: 11.5pt; font-family: \'Arial\',\'sans - serif\'; color: #333333;\">&copy; 2020 iPrize Limited<\/span><\/p>","details":null,"type":"rich_text_box","order":"11","group":"Codemail"},
{"id":"22","key":"admin.question_page","display_name":"Question sets on page","value":"5","details":"{\r\n    \"validation\": {\r\n        \"rule\": \"required|numeric\"\r\n    }\r\n}","type":"text","order":"13","group":"Admin"},
{"id":"23","key":"share.main-text","display_name":"Main text","value":"Create your own contest","details":null,"type":"text_area","order":"15","group":"Share"},
{"id":"24","key":"share.main-title","display_name":"Main title","value":"iPrize","details":null,"type":"text","order":"14","group":"Share"},
{"id":"25","key":"share.main-logo","display_name":"Main logo","value":"settings\/May2020\/NAHqO4FsgQMVkuJ7E6JE.jpg","details":null,"type":"image","order":"16","group":"Share"},
{"id":"37","key":"share.rating-text","display_name":"Rating text","value":"Rating","details":null,"type":"text_area","order":"15","group":"Share"},
{"id":"38","key":"share.rating-title","display_name":"Rating title","value":"iPrize","details":null,"type":"text","order":"14","group":"Share"},
{"id":"39","key":"share.rating-logo","display_name":"Rating logo","value":"settings\/May2020\/O9HuFCtRCSW7H5WegXHA.png","details":null,"type":"image","order":"16","group":"Share"},
{"id":"40","key":"share.category-text","display_name":"Category text","value":"Create your own contest","details":null,"type":"text_area","order":"15","group":"Share"},
{"id":"41","key":"share.questions-text","display_name":"Questions text","value":"Create your own contest","details":null,"type":"text_area","order":"15","group":"Share"},
{"id":"42","key":"share.questions-title","display_name":"Questions title","value":"iPrize","details":null,"type":"text","order":"14","group":"Share"},
{"id":"43","key":"share.questions-logo","display_name":"Questions logo","value":"settings\/May2020\/7LPfEm0xEl4GTq6BvIQD.png","details":null,"type":"image","order":"16","group":"Share"},
{"id":"44","key":"share-pages.top-text","display_name":"Top text","value":"Download iPrize","details":null,"type":"text","order":"17","group":"Share pages"},
{"id":"45","key":"share-pages.google","display_name":"Google Play Icon","value":"settings\/June2020\/TC87MFTuxxOGNL2eh8Xx.png","details":null,"type":"image","order":"18","group":"Share pages"},
{"id":"46","key":"share-pages.apple","display_name":"App Store Icon","value":"settings\/June2020\/74wgKGXWuAc0WXQNlFk5.png","details":null,"type":"image","order":"19","group":"Share pages"},
{"id":"47","key":"share-pages.bottom-text","display_name":"Bottom text","value":"If you’ve already installed the app","details":null,"type":"text","order":"20","group":"Share pages"},
{"id":"48","key":"share-pages.button-text","display_name":"Button text","value":"PRESS HERE","details":null,"type":"text","order":"21","group":"Share pages"},
{"id":"49","key":"bonuses.like","display_name":"Like","value":"1","details":null,"type":"text","order":"22","group":"bonuses"},
{"id":"50","key":"bonuses.register","display_name":"Register","value":"100","details":null,"type":"text","order":"23","group":"bonuses"},
{"id":"51","key":"bonuses.competition","display_name":"Create competition","value":"100","details":null,"type":"text","order":"24","group":"bonuses"},
{"id":"52","key":"bonuses.share","display_name":"Share","value":"10","details":null,"type":"text","order":"25","group":"bonuses"},
{"id":"53","key":"site.popular","display_name":"Popular","value":"Popular","details":null,"type":"text","order":"26","group":"Site"},
{"id":"54","key":"admin.work_premoderation","display_name":"Work Premoderation","value":"0","details":null,"type":"checkbox","order":"27","group":"Admin"}
]
        ';

        $settings = json_decode($json, true);
        \Illuminate\Support\Facades\DB::table('settings')->insert($settings);
    }
}
