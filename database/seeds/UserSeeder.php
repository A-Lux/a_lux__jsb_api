<?php

use App\v2\Contracts\UserContract;
use App\v2\Models\User;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use AlecRabbit\Spinner\SnakeSpinner;

class UserSeeder extends Seeder
{

    public function run(Faker $faker)
    {
        $users = collect([]);

        $spinner = new SnakeSpinner();

        echo "Creating users to insert\n";
        $spinner->begin();

        for ($i = 0; $i < 1000; $i++) {
            $users->push(
                [
                    UserContract::AVATAR       => 'v2/users/defaults.png',
                    UserContract::NAME         => $faker->name(),
                    UserContract::EMAIL        => 'test' . $i . '@test.com',
                    UserContract::ROLE         => 2,
                    UserContract::COUNTRY_NAME => $faker->country,
                    UserContract::COUNTRY_ID   => 10,
                    UserContract::CITY_NAME    => $faker->city,
                    UserContract::CITY_ID      => 10,
                    UserContract::PASSWORD     => Hash::make('password'),
                    UserContract::ABOUT        => $faker->sentence(),
                    UserContract::DIFFICULTY   => 1,
                    UserContract::IS_BLOCKED   => random_int(0, 100) < 0.75,
                    'created_at'               => \Carbon\Carbon::now(),
                ]
            );
            $spinner->spin();
        }

        $users->push(
            [
                UserContract::AVATAR       => 'v2/users/defaults.png',
                UserContract::NAME         => 'Shinrai Hikaro',
                UserContract::EMAIL        => 'japananimetime@gmail.com',
                UserContract::ROLE         => 1,
                UserContract::COUNTRY_NAME => 'Japan',
                UserContract::COUNTRY_ID   => 10,
                UserContract::CITY_NAME    => 'Tokio',
                UserContract::CITY_ID      => 10,
                UserContract::PASSWORD     => Hash::make('password'),
                UserContract::ABOUT        => 'Just some developer',
                UserContract::DIFFICULTY   => 1,
                UserContract::IS_BLOCKED   => random_int(0, 100) < 0.75,
                UserContract::CREATED_AT   => \Carbon\Carbon::now(),
            ]
        );
        $users->push(
            [
                UserContract::AVATAR       => 'v2/users/defaults.png',
                UserContract::NAME         => 'Yuri',
                UserContract::EMAIL        => 'y.pashov@gmail.com',
                UserContract::ROLE         => 2,
                UserContract::COUNTRY_NAME => 'Japan',
                UserContract::COUNTRY_ID   => 10,
                UserContract::CITY_NAME    => 'Almaty',
                UserContract::CITY_ID      => 10,
                UserContract::PASSWORD     => Hash::make('password'),
                UserContract::ABOUT        => 'Just some developer',
                UserContract::DIFFICULTY   => 1,
                UserContract::IS_BLOCKED   => 0,
                UserContract::CREATED_AT   => \Carbon\Carbon::now(),
            ]
        );

        $spinner->end('Data created');

        echo "\nInserting\n";

        $chunks = $users->chunk(200);
        $this
            ->command
            ->getOutput()
            ->progressStart(1000)
        ;

        foreach ($chunks as $chunk) {
            User
                ::query()
                ->insert(
                    $chunk->toArray()
                )
            ;
            $this
                ->command
                ->getOutput()
                ->progressAdvance(200)
            ;

        }

        $this
            ->command
            ->getOutput()
            ->progressFinish()
        ;
    }
}
