<?php

use AlecRabbit\Spinner\SnakeSpinner;
use App\v2\Contracts\LocationAdContract;
use App\v2\Contracts\LocationAdMediaContract;
use App\v2\Contracts\LocationAdTextContract;
use App\v2\Models\LocationAd;
use App\v2\Models\LocationAdMedia;
use App\v2\Models\LocationAdText;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class LocationAdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ads    = collect([]);
        $videos = collect([]);
        $texts  = collect([]);
        $images = collect([]);

        $spinner = new SnakeSpinner();

        echo "Creating ads to insert\n";
        $spinner->begin();

        for ($i = 0; $i < 100000; $i++) {
            $type = \Illuminate\Support\Arr::random(LocationAdContract::TYPES);
            $ads->push(
                [
                    LocationAdContract::ID             => Str::uuid()
                                                             ->toString(),
                    LocationAdContract::LOCATION_ID    => random_int(1, 130000),
                    LocationAdContract::TYPE           => $type,
                    LocationAdContract::IS_FOR_COUNTRY => random_int(0, 4) > 1 ? 1 : 0,
                    LocationAdContract::CREATED_AT     => \Carbon\Carbon::now(),
                ]
            );
            if ($type === LocationAdContract::TYPES['IMAGE']) {
                $images->push(
                    [
                        LocationAdMediaContract::FILE_URL => 'v2/competitions/1/competitions/hSEDai1AQQu6MAgwyb4fYhFcnGvdQi6HQiAVD9dQ.jpg',
                    ]
                );
            }
            if ($type === LocationAdContract::TYPES['VIDEO']) {
                $videos->push(
                    [
                        LocationAdMediaContract::FILE_URL => 'https://www.w3schools.com/html/mov_bbb.mp4',
                    ]
                );
            }
            $spinner->spin();
        }

        $spinner->end('Data created');

        echo "\nInserting\n";
        $chunks = $ads->chunk(1000);
        $this
            ->command
            ->getOutput()
            ->progressStart(100000)
        ;
        foreach ($chunks as $chunk) {
            LocationAd
                ::query()
                ->insert($chunk->toArray())
            ;
            $this
                ->command
                ->getOutput()
                ->progressAdvance(1000)
            ;
        }

        $this->command
            ->getOutput()
            ->progressFinish()
        ;

        echo "\nGetting ads to insert \n";
        $this
            ->command
            ->getOutput()
            ->progressStart(3)
        ;
        $imageAds = LocationAd
            ::query()
            ->where(LocationAdContract::TYPE, LocationAdContract::TYPES['IMAGE'])
            ->get();
        $this
            ->command
            ->getOutput()
            ->progressAdvance(1)
        ;
        $videoAds = LocationAd
            ::query()
            ->where(LocationAdContract::TYPE, LocationAdContract::TYPES['VIDEO'])
            ->get();
        $this
            ->command
            ->getOutput()
            ->progressAdvance(1)
        ;
        $textAds = LocationAd
            ::query()
            ->where(LocationAdContract::TYPE, LocationAdContract::TYPES['TEXT'])
            ->get();
        $this
            ->command
            ->getOutput()
            ->progressAdvance(1)
        ;

        $this->command
            ->getOutput()
            ->progressFinish()
        ;

        echo "\nProcessing images for ads\n";

        $i = collect([]);
        foreach ($imageAds as $ad) {
            $i->push(
                [
                    LocationAdMediaContract::ID        => Str::uuid()
                                                            ->toString(),
                    LocationAdMediaContract::AD_ID    => $ad->{LocationAdContract::ID},
                    LocationAdMediaContract::FILE_URL => 'v2/competitions/1/competitions/hSEDai1AQQu6MAgwyb4fYhFcnGvdQi6HQiAVD9dQ.jpg',
                ]
            );
        }

        echo "\nInserting images for ads\n";

        $this
            ->command
            ->getOutput()
            ->progressStart($images->count())
        ;

        $i_c = $i->chunk(1000);

        foreach ($i_c as $d) {
            LocationAdMedia
                ::query()
                ->insert(
                    $d->toArray()
                )
            ;
        }

        $this
            ->command
            ->getOutput()
            ->progressAdvance(1)
        ;


        $this->command
            ->getOutput()
            ->progressFinish()
        ;

        echo "\nProcessing videos for ads\n";

        $v = collect([]);
        foreach ($imageAds as $ad) {
            $v->push(
                [
                    LocationAdMediaContract::ID        => Str::uuid()
                                                            ->toString(),
                    LocationAdMediaContract::AD_ID    => $ad->{LocationAdContract::ID},
                    LocationAdMediaContract::FILE_URL => 'v2/competitions/1/competitions/hSEDai1AQQu6MAgwyb4fYhFcnGvdQi6HQiAVD9dQ.jpg',
                ]
            );
        }

        echo "\nInserting images for ads\n";

        $this
            ->command
            ->getOutput()
            ->progressStart($images->count())
        ;

        $v_c = $v->chunk(1000);

        foreach ($v_c as $d) {
            LocationAdMedia
                ::query()
                ->insert(
                    $d->toArray()
                )
            ;
        }

        $this
            ->command
            ->getOutput()
            ->progressAdvance(1)
        ;


        $this->command
            ->getOutput()
            ->progressFinish()
        ;

        echo "\nProcessing text for ads\n";

        $t = collect([]);
        foreach ($imageAds as $ad) {
            $t->push(
                [
                    LocationAdTextContract::ID        => Str::uuid()
                                                            ->toString(),
                    LocationAdTextContract::AD_ID    => $ad->{LocationAdContract::ID},
                    LocationAdTextContract::TEXT => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
                ]
            );
        }

        echo "\nInserting images for ads\n";

        $this
            ->command
            ->getOutput()
            ->progressStart($images->count())
        ;

        $t_c = $t->chunk(1000);

        foreach ($t_c as $d) {
            LocationAdText
                ::query()
                ->insert(
                    $d->toArray()
                )
            ;
        }

        $this
            ->command
            ->getOutput()
            ->progressAdvance(1)
        ;


        $this->command
            ->getOutput()
            ->progressFinish()
        ;
    }
}
