<?php

use AlecRabbit\Spinner\SnakeSpinner;
use App\Comment;
use App\v2\Contracts\CommentContract;
use App\v2\Contracts\UserContract;
use App\v2\Contracts\WorkContract;
use App\v2\Models\Work;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $comments = collect([]);
        $work_ids = Work::query()
                        ->select(WorkContract::ID)
                        ->get();
        $user_ids = \App\v2\Models\User::query()
                                       ->select(UserContract::ID)
                                       ->get();

        $spinner = new SnakeSpinner();

        echo "Creating comments to insert\n";
        $spinner->begin();

        //TODO Change to 10000000, but in chunks cause of ram shortage
        for ($i = 0; $i < 10000; $i++) {
            $comments->push(
                [
                    CommentContract::WORK_ID    => $work_ids->random()->{WorkContract::ID},
                    CommentContract::USER_ID    => $user_ids->random()->{UserContract::ID},
                    CommentContract::TEXT       => $faker->sentence(),
                    CommentContract::VERIFIED   => random_int(1, 100) > 75 ? 1 : 0,
                    CommentContract::CREATED_AT => \Carbon\Carbon::now(),
                ]
            );
            $spinner->spin();
        }

        $spinner->end('Data created');

        echo "\nInserting\n";

        $chunks = $comments->chunk(1000);

        $this
            ->command
            ->getOutput()
            ->progressStart($comments->count())
        ;
        foreach ($chunks as $chunk) {
            Comment
                ::query()
                ->insert($chunk->toArray())
            ;
            $this
                ->command
                ->getOutput()
                ->progressAdvance(1000)
            ;
        }

        $this->command
            ->getOutput()
            ->progressFinish()
        ;
    }
}
