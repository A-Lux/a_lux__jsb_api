<?php

use App\v2\Contracts\LocationContract;
use App\v2\Models\Location;
use Illuminate\Database\Seeder;
use AlecRabbit\Spinner\SnakeSpinner;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fh      = fopen(database_path() . "/maxmind/locations.csv", "r");
        $csvData = [];
        while (($row = fgetcsv($fh, 0, ",")) !== false) {
            $csvData[] = $row;
        }

        $locations = collect([]);
        $spinner   = new SnakeSpinner();

        echo "Reading locations to insert\n";
        $spinner->begin();

        for ($i = 1; $i < count($csvData); $i++) {
            $locations->push(
                [
                    LocationContract::GEONAME_ID             => $csvData[$i][0],
                    LocationContract::LOCALE_CODE            => $csvData[$i][1],
                    LocationContract::CONTINENT_CODE         => $csvData[$i][2],
                    LocationContract::CONTINENT_NAME         => $csvData[$i][3],
                    LocationContract::COUNTRY_ISO_CODE       => $csvData[$i][4],
                    LocationContract::COUNTRY_NAME           => $csvData[$i][5],
                    LocationContract::SUBDIVISION_1_ISO_CODE => $csvData[$i][6],
                    LocationContract::SUBDIVISION_1_NAME     => $csvData[$i][7],
                    LocationContract::SUBDIVISION_2_ISO_CODE => $csvData[$i][8],
                    LocationContract::SUBDIVISION_2_NAME     => $csvData[$i][9],
                    LocationContract::CITY_NAME              => $csvData[$i][10],
                    LocationContract::METRO_CODE             => $csvData[$i][11],
                    LocationContract::TIME_ZONE              => $csvData[$i][12],
                    LocationContract::IS_IN_EUROPEAN_UNION   => $csvData[$i][13],
                ]
            );
            $spinner->spin();
        }

        $spinner->end('Data created');

        echo "\nInserting\n";
        $chunks = $locations->chunk(1000);
        $this->command
            ->getOutput()
            ->progressStart($locations->count())
        ;

        foreach ($chunks as $chunk) {
            Location
                ::query()
                ->insert($chunk->toArray())
            ;
            $this
                ->command
                ->getOutput()
                ->progressAdvance(1000)
            ;
        }

        $this->command
            ->getOutput()
            ->progressFinish()
        ;
    }
}
