<?php

use App\v2\Contracts\CategoryAdContract;
use App\v2\Contracts\CategoryContract;
use App\v2\Contracts\CompetitionContract;
use App\v2\Models\CategoryAd;
use App\v2\Models\Competition;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;


class CategoryAdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        for ($i = 0; $i < 100; $i++) {
            CategoryAd
                ::query()
                ->create(
                    [
                        CategoryAdContract::CATEGORY_ID => random_int(1, 10),
                        CategoryAdContract::IMAGE => 'v2/competitions/1/competitions/hSEDai1AQQu6MAgwyb4fYhFcnGvdQi6HQiAVD9dQ.jpg',
                        CategoryAdContract::LINK => 'https://google.com'
                    ]
                )
            ;
        }
    }
}
