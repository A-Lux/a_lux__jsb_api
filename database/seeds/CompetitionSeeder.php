<?php

use App\v2\Contracts\CategoryContract;
use App\v2\Contracts\CompetitionContract;
use App\v2\Models\Competition;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use AlecRabbit\Spinner\SnakeSpinner;

class CompetitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {

        $competitions = collect([]);
        $spinner      = new SnakeSpinner();

        echo "Creating competitions to insert\n";
        $spinner->begin();

        for ($i = 0; $i < 10000; $i++) {
            $competitions->push(
                [
                    CompetitionContract::NAME         => $faker->name(),
                    CompetitionContract::COMPANY_ID   => null,
                    CompetitionContract::CATEGORY_ID  => collect(CategoryContract::LIST)->random()['id'],
                    CompetitionContract::IMAGE        => 'v2/competitions/1/competitions/hSEDai1AQQu6MAgwyb4fYhFcnGvdQi6HQiAVD9dQ.jpg',
                    CompetitionContract::AD           => 'v2/competitions/1/competitions/hSEDai1AQQu6MAgwyb4fYhFcnGvdQi6HQiAVD9dQ.jpg',
                    CompetitionContract::START_AT     => $faker->dateTimeBetween('-30 days'),
                    CompetitionContract::FINISH_AT    => $faker->dateTimeBetween('now', '+30 days'),
                    CompetitionContract::TYPE_ID      => 1,
                    CompetitionContract::DESCRIPTION  => $faker->sentence(),
                    CompetitionContract::SUM          => random_int(1000, 1000000),
                    CompetitionContract::IS_CONFIRMED => random_int(0, 100) < 0.75,
                    CompetitionContract::USER_ID      => random_int(0, 999),
                ]
            );
            $spinner->spin();
        }

        $spinner->end('Data created');

        echo "\nInserting\n";

        $chunks = $competitions->chunk(1000);
        $this
            ->command
            ->getOutput()
            ->progressStart($competitions->count())
        ;
        foreach ($chunks as $chunk) {
            Competition
                ::query()
                ->insert($chunk->toArray())
            ;
            $this
                ->command
                ->getOutput()
                ->progressAdvance(1000)
            ;
        }

        $this->command
            ->getOutput()
            ->progressFinish()
        ;
    }
}
