<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CompetitionSeeder::class);
        $this->call(WorkSeeder::class);
        $this->call(ComplaintSeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(CategoryAdSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(LocationAdSeeder::class);
        $this->call(SettingSeeder::class);
    }
}
