<?php

use App\v2\Contracts\WorkContract;
use App\v2\Models\Work;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use AlecRabbit\Spinner\SnakeSpinner;

class WorkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $works = collect([]);
        $spinner      = new SnakeSpinner();

        echo "Creating works to insert\n";
        $spinner->begin();

        for ($i = 0; $i < 100000; $i++) {
            $works->push(
                    [
                        WorkContract::COMPETITION_ID => random_int(1, 9999),
                        WorkContract::USER_ID   => random_int(1, 999),
                        WorkContract::DESCRIPTION   => $faker->sentence(),
                        WorkContract::DATA  => 'v2/users/defaults.png',
                        WorkContract::SHARE_AMOUNT  => random_int(0, 9999),
                        WorkContract::VERIFIED  => random_int(0, 100) < 0.75,
                    ]
                )
            ;
            $spinner->spin();
        }

        $spinner->end('Data created');

        echo "\nInserting\n";
        $this
            ->command
            ->getOutput()
            ->progressStart($works->count())
        ;
        $chunks = $works->chunk(1000);

        foreach ($chunks as $chunk) {
            Work
                ::query()
                ->insert($chunk->toArray())
            ;
            $this
                ->command
                ->getOutput()
                ->progressAdvance(1000)
            ;
        }

        $this->command
            ->getOutput()
            ->progressFinish()
        ;
    }
}
